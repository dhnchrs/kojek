$(document).ajaxStart(function(){
	$('#loading').show();
});

$(document).ajaxComplete(function(){
	$('#loading').hide();
});

$(".bootstrapMaterialDatePicker").bootstrapMaterialDatePicker({
	weekStart:0,
	time: false,
	format: 'DD/MM/YYYY'
});


$.ajax({
	url: base_url+"Inhealth/cari_rawat_inap_inhealth",
	type: "post",
	dataType: 'json',
	success: function(response) {
		$('#parent_tabel_rawat_inap_inhealth').empty();
		var tabel_rawat_inap_inhealth = "";
		tabel_rawat_inap_inhealth =
			"<table class='table table-bordered table-striped table-hover' id='tabel_rawat_inap_inhealth'>"+
				"<thead>"+
					"<th>#</th>"+
					"<th>Nomor MR</th>"+
					"<th>Nama</th>"+
					"<th>Ruang</th>"+
					"<th>Nomor Bed</th>"+
					"<th>Nomor SJP</th>"+
					"<th>Hak Kelas</th>"+
					"<th>Kelas Ditempati</th>"+
					"<th style='text-align: center;'>Aksi</th>"+
				"</thead>"+
				"<tbody>";
	
		var nomor_tabel_rawat_inap = 1;
		$.each(response, function (index, dataRawatInap) {
			if (dataRawatInap.v_mr_code != null) {
				var v_mr_code = dataRawatInap.v_mr_code;
			} else {
				var v_mr_code = '-';
			}
			if (dataRawatInap.v_patient_name != null) {
				var v_patient_name = dataRawatInap.v_patient_name;
			} else {
				var v_patient_name = '-';
			}
			if (dataRawatInap.v_ward_name != null) {
				var v_ward_name = dataRawatInap.v_ward_name;
			} else {
				var v_ward_name = '-';
			}
			if (dataRawatInap.v_bed_desc != null) {
				var v_bed_desc = dataRawatInap.v_bed_desc;
			} else {
				var v_bed_desc = '-';
			}
			if (dataRawatInap.no_sjp != null) {
				var no_sjp = dataRawatInap.no_sjp;
			} else {
				var no_sjp = '-';
			}
			if (dataRawatInap.kelas != null) {
				var kelas = dataRawatInap.kelas;
			} else {
				var kelas = '';
			}
			if (dataRawatInap.kelas_desc != null) {
				var kelas_desc = dataRawatInap.kelas_desc;
			} else {
				var kelas_desc = '';
			}
			if (dataRawatInap.v_tclass_desc != null) {
				var ditempati = dataRawatInap.v_tclass_desc;
			} else {
				var ditempati = '-';
			}
			var nosjp_lempar = '"'+dataRawatInap.no_sjp+'"';
			var idakomodasi_lempar = '"BED-'+dataRawatInap.n_bed_id+'"';
			var tglmasukrawat_lempar = '"'+dataRawatInap.tglmasukrawat+'"';
			tabel_rawat_inap_inhealth +=
				"<tr>" +
					"<td>" + nomor_tabel_rawat_inap + "</td>" +
					"<td>" + v_mr_code + "</td>" +
					"<td>" + v_patient_name + "</td>" +
					"<td>" + v_ward_name + "</td>" +
					"<td>" + v_bed_desc + "</td>" +
					"<td>" + no_sjp + "</td>" +
					"<td>" + kelas + " - " + kelas_desc + "</td>" +
					"<td>" + ditempati + "</td>" +
					"<td style='text-align: center;'><button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' onclick='modal_daftar_nota_valid("+dataRawatInap.n_reg_id+","+nosjp_lempar+","+idakomodasi_lempar+","+tglmasukrawat_lempar+")' title='Tampilkan Daftar Nota Valid'><i class='material-icons'>attach_money</i></button></td>" +
				"</tr>";
			nomor_tabel_rawat_inap++;
		});
		tabel_rawat_inap_inhealth +=
				"</tbody>"+
			"</table>";
		$('#parent_tabel_rawat_inap_inhealth').html(tabel_rawat_inap_inhealth);
		$('#tabel_rawat_inap_inhealth').DataTable();
	}
});
function get_detail_nota_valid(n_exam_id){
	//console.log(n_exam_id);
	var detail_nota_valid = "";
	$.ajax({
		url: base_url+"Inhealth/cari_detail_daftar_nota_valid",
		type: "post",
		data: {
			n_exam_id: n_exam_id
		},
		dataType: 'json',
		async: false,
		success: function(response) {
			if (response.detail_item_trx || response.detail_treatment_trx) {
				$.each(response.detail_item_trx, function (index, detail_item_trx) {
					detail_nota_valid += detail_item_trx.v_item_name+", ";
				});
				$.each(response.detail_treatment_trx, function (index, detail_treatment_trx) {
					detail_nota_valid += detail_treatment_trx.v_treatment_name+", ";
				});
			} else {
				detail_nota_valid = "-";
			}
		}
	});
	return detail_nota_valid.replace(/, +$/,'');
}
function modal_daftar_nota_valid(n_reg_id, nosjp_lempar, idakomodasi_lempar, tglmasukrawat_lempar){
	console.log(n_reg_id);
	var tabel_daftar_nota_valid = "";
	$.ajax({
		url: base_url+"Inhealth/cari_daftar_nota_valid",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		dataType: 'json',
		async: false,
		success: function(response) {
			$('#parent_tabel_daftar_nota_valid').empty();
			$('#button_daftar_nota_valid').empty();
			tabel_daftar_nota_valid +=
						"<table class='table table-bordered table-striped table-hover'>"+
							"<thead>"+
								"<th style='text-align: center;'>#</th>"+
								"<th style='text-align: center;'>Nomor Nota</th>"+
								"<th style='text-align: center;'>Total Nominal</th>"+
								"<th style='text-align: center;'>Detail</th>"+
							"</thead>"+
							"<tbody>";
			var nomor_daftar_nota_valid = 1;
			$.each(response, function (index, dataNotaValid) {
				if (dataNotaValid.n_status_inhealth != 0) {
					var disabled = 'disabled';
				} else {
					var disabled = '';
				}
				tabel_daftar_nota_valid +=
					"<tr>"+
						"<td style='text-align: center;'>\
							<div style='width: 10px'>\
							<div class='demo-checkbox'>\
                                <input type='checkbox' name='checkbox_examination' id='checkbox_examination"+nomor_daftar_nota_valid+"' "+disabled+" value='"+dataNotaValid.n_exam_id+"'/>\
                                <label for='checkbox_examination"+nomor_daftar_nota_valid+"'></label>\
                            </div>\
                            </div>\
						</td>" +
						"<td nowrap>" + dataNotaValid.v_note_no + "</td>" +
						"<td style='text-align: right;' nowrap>Rp. " + numeral(dataNotaValid.n_total_amount).format('0,0') + "</td>" +
						"<td>" + get_detail_nota_valid(dataNotaValid.n_exam_id) + "</td>" +
					"</tr>";
				nomor_daftar_nota_valid++;
			});
			var nosjp = '"'+nosjp_lempar+'"';
			var idakomodasi = '"'+idakomodasi_lempar+'"';
			var tglmasukrawat = '"'+tglmasukrawat_lempar+'"';
			button_daftar_nota_valid = "<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id='button_simpan_sep_rajal' onclick='simpan_tindakan_inhealth("+nosjp+", "+idakomodasi+", "+tglmasukrawat+")' style='margin-top: 5px;margin-left: -2px;'><i class='material-icons'>save</i> Kirim Bridging</button>";
			$('#parent_tabel_daftar_nota_valid').html(tabel_daftar_nota_valid)
			$('#button_daftar_nota_valid').html(button_daftar_nota_valid)
			$('#modal_daftar_nota_valid').modal('show');
		}
	});
}
function simpan_tindakan_inhealth(nosjp, idakomodasi, tglmasukrawat) {
	var checkbox_examination = [];
	$.each($("input[name='checkbox_examination']:checked"), function(){
		checkbox_examination.push($(this).val());
	});
	console.log(checkbox_examination);
	console.log(nosjp);
	console.log(idakomodasi);
	console.log(tglmasukrawat);
	$.ajax({
		url: base_url+"Inhealth/simpan_tindakan_inhealth",
		type: "post",
		data: {
			checkbox_examination: checkbox_examination,
			nosjp: nosjp,
			idakomodasi: idakomodasi,
			tglmasukrawat: tglmasukrawat
		},
		dataType: 'json',
		success: function(response) {
			console.log(response);
			swal({   
				title: "Sukses!",
				text: "Data anda berhasil disimpan ke bridging inhealth",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}