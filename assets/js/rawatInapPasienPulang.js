function modal_buat_pdf_pasien_pulang(n_reg_id,nosep,tglsep,sum_real_cost) {
	console.log(n_reg_id);
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatInapAll/cari_nilai_paket_buat_pdf_pasien_pulang",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			sum_real_cost: sum_real_cost
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			$('#nosep_buat_pdf_hidden').val('');
			$('#regid_buat_pdf_hidden').val('');
			$('#tglsep_buat_pdf_hidden').val('');
			$('#nosep_buat_pdf_hidden').val(nosep);
			$('#regid_buat_pdf_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_hidden').val(tglsep);
			$('#parent_buat_pdf').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;border-collapse: collapse;' border='1'>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<th style='padding: 5px;text-align:center;'>No.</th>" +
						"<th style='padding: 5px;text-align:center;'>Kelompok</th>" +
						"<th style='padding: 5px;text-align:center;'>Total</th>" +
					"</tr>";
			var nomor_nota_paket = 1;
			var total_nota_paket = 0;
			$.each(response_parse.nota_paket, function (index, dataNotaPaket) {
				if (dataNotaPaket.total != null) {
					total_nota_paket = dataNotaPaket.total;
				} else {
					total_nota_paket = 0;
				}
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;text-align:center;'>" + nomor_nota_paket + "</td>" +
						"<td style='padding: 5px'>" + dataNotaPaket.namakel + "</td>" +
						"<td style='padding: 5px' align='right'>" + numeral(total_nota_paket).format('0,0') + "</td>" +
					"</tr>";
				nomor_nota_paket++;
			});
			tabel_buat_pdf +=
				"</table>" +
				"<table align='right'>" +
					"<tr>" +
						"<td style='padding: 5px;'><b>Jumlah Biaya : Rp. " + numeral(response_parse.sum_real_cost).format('0,0') +"</b></td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td><br/><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
		}
	});
	$('#parent_buat_pdf').html(tabel_buat_pdf);
	if (nosep != 'null') {
		$('#judul_buat_pdf').html('(Nomor SEP: '+nosep+')');
	} else {
		$('#judul_buat_pdf').html('');
	}
	$('#modal_buat_pdf_pasien_pulang').modal('show');
}
function modal_buat_detail_pdf_pasien_pulang(n_reg_id,nosep,tglsep,sum_real_cost) {
	console.log(n_reg_id);
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatInapAll/cari_nilai_paket_buat_pdf_pasien_pulang",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			sum_real_cost: sum_real_cost
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			$('#nosep_buat_detail_pdf_hidden').val('');
			$('#regid_buat_detail_pdf_hidden').val('');
			$('#tglsep_buat_detail_pdf_hidden').val('');
			$('#nosep_buat_detail_pdf_hidden').val(nosep);
			$('#regid_buat_detail_pdf_hidden').val(n_reg_id);
			$('#tglsep_buat_detail_pdf_hidden').val(tglsep);
			$('#parent_buat_detail_pdf').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;'>" +
					"<tr>" +
						"<td style='padding: 5px;' colspan='5'>KAMAR KEPERAWATAN</td>" +
					"</tr>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;'>NOTA RUANGAN</td>" +
						"<td style='padding: 5px;'>TANGGAL MASUK</td>" +
						"<td style='padding: 5px;'>TANGGAL KELUAR</td>" +
						"<td style='padding: 5px;'>JUMLAH JAM</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			
			var total_nota_paket = 0;
			$.each(response_parse.kasur_detail, function (index, dataKasurDetail) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.nota + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.masuk).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.keluar).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.jam + "</td>" +
						"<td style='padding: 5px;text-align:right;'>" + numeral(dataKasurDetail.total).format('0,0') + "</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(response_parse.sum_fungsi_kasur_detail).format('0,0') +"</td>" +
					"</tr>" +
				"</table><br/>";
			tabel_buat_pdf +=
				"<table style='width:100%;'>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;' colspan='4'>KELOMPOK BIAYA</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			$.each(response_parse.cetak_byreg, function (index, dataCetakByReg) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;font-size: 18px;' colspan='5'>" + dataCetakByReg.kelompok + "</td>" +
					"</tr>";
				$.ajax({
					url: base_url+"RawatInap/get_cetak_byreg_kojek_by_kelompok",
					type: "post",
					data: {
						n_reg_id: n_reg_id,
						kelompok: dataCetakByReg.kelompok
					},
					datatype: 'json',
					async: false,
					success: function(response_by_kelompok) {
						var response_by_kelompok_parse = JSON.parse(response_by_kelompok);
						$.each(response_by_kelompok_parse, function (index, dataByKelompok) {
							tabel_buat_pdf +=
								"<tr>" +
									"<td>&emsp;&emsp;</td>" +
									"<td style='padding: 5px;'>" + moment(dataByKelompok.tgl).format('DD/MM/YYYY') + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.ket + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.qty + "</td>" +
									"<td style='padding: 5px;text-align:right;'>" + numeral(dataByKelompok.subttl).format('0,0') + "</td>" +
								"</tr>";
						});
					}
				});
				tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(dataCetakByReg.sum_total).format('0,0') +"</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
				"</table>" +
				"<table align='right'>" +
					"<tr>" +
						"<td style='padding: 5px;'><b>Jumlah Biaya : Rp. " + numeral(response_parse.sum_real_cost).format('0,0') +"</b></td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td><br/><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			$('#parent_buat_detail_pdf').html(tabel_buat_pdf);
			if (nosep != 'null') {
				$('#judul_buat_detail_pdf').html('(Nomor SEP: '+nosep+')');
			} else {
				$('#judul_buat_detail_pdf').html('');
			}
			$('#modal_buat_detail_pdf').modal('show');
		}
	});
}
$('#button_cari_rawat_inap_pulang').click(function(){
	var tanggal_pulang = moment($("#tanggal_pulang").val(), 'DD-MM-YYYY').format('YYYY-MM-DD');
	console.log(tanggal_pulang);
	cari_rawat_inap_pulang(tanggal_pulang);
});
function cari_rawat_inap_pulang(tglPulang) {
	if (!tglPulang) {
		swal({   
			title: "Gagal!",
			text: "Data anda gagal dicari, silahkan mengisi data dengan benar",
			timer: 1500,
			showConfirmButton: false,
			type: "error",
		});
	} else {
		$.ajax({
			url: base_url+"RawatInapAll/cari_rawat_inap_pulang",
			type: "post",
			data: {
				tgl_pulang: tglPulang
			},
			datatype: 'json',
			success: function(response) {
				var response_parse = JSON.parse(response);
				console.log(response_parse);
				$('#parent_tabel_rawat_inap').empty();
				$('#pemberitahuan_input').hide();
				var tabel_rawat_inap_awal =
						
						// "<input type='hidden' id='ward_name_hidden' value='"+ward_name+"'>"+
						"<table class='table table-bordered table-striped table-hover' id='tabel_rawat_inap'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Tanggal Masuk</th>"+
								"<th>LOS</th>"+
								"<th>Nomor Bed</th>"+
								"<th>Nomor MR</th>"+
								"<th>Nama Pasien</th>"+
								"<th>Nomor SEP</th>"+
								"<th>Koding Hak</th>"+
								"<th>Koding Ditempati</th>"+
								"<th>Real Cost</th>"+
								"<th style='text-align:center;'>Buat PDF</th>"+
								"<th>Tanggal Koding</th>"+
								"<th>Status Koding</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
				var nomor_tabel_rawat_inap = 1;
				var tabel_rawat_inap_isi = "";
				$.each(response_parse, function (index, dataRawatInap) {
						var n_Koding_hak;
						var n_codding_ditempati;
						var tanggal_koding;
						console.log(dataRawatInap);
						$.ajax({
							url: base_url+"RawatInap/get_last_koding",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_last_koding) {
								var response_last_koding_parse = JSON.parse(response_last_koding);
								if (response_last_koding_parse != null) {
									//KODING HAK
									if (response_last_koding_parse.n_codding_hak != null) {
										n_Koding_hak = numeral(response_last_koding_parse.n_codding_hak).format('0,0');
									} else {
										n_Koding_hak = '-';
									}
									//KODING HAK
									//KODING DITEMPATI
									if (response_last_koding_parse.n_codding_ditempati != null) {
										n_codding_ditempati = numeral(response_last_koding_parse.n_codding_ditempati).format('0,0');
									} else {
										n_codding_ditempati = '-';
									}
									//KODING DITEMPATI
									//TANGGAL KODING
									if (response_last_koding_parse.tanggal_koding != null) {
										tanggal_koding = moment(response_last_koding_parse.tanggal_koding).format('DD/MM/YYYY');
									} else {
										tanggal_koding = '-';
									}
									//TANGGAL KODING
								} else {
									n_Koding_hak = '-';
									n_codding_ditempati = '-';
									tanggal_koding = '-';
								}
							}
						});
					//LOS
					// var date1 = moment(dataRawatInap.date);
					// var date2 = moment(tglPulang);
					// var diff = date2.diff(date1, 'days');
					//LOS
					tabel_rawat_inap_isi +=
								"<tr>"+
									"<td>" + nomor_tabel_rawat_inap + "</td>" +
									"<td>" + moment(dataRawatInap.reg_date).format('DD/MM/YYYY') + "</td>" +
									"<td nowrap>" + dataRawatInap.los + " hari</td>" +
									"<td>" + dataRawatInap.v_bed_desc + "</td>" +
									"<td nowrap>" + (dataRawatInap.v_mr_code).replace(/\-/g,"") + "</td>" +
									"<td>" + dataRawatInap.v_patient_name + "</td>";
					if (dataRawatInap.v_nosepbpjs != null) {
						var no_sep = '"'+dataRawatInap.v_nosepbpjs+'"';
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						tabel_rawat_inap_isi += "<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Edit SEP' onclick='modal_edit_sep("+dataRawatInap.n_reg_id+","+no_sep+","+nama_pasien+")'>" + dataRawatInap.v_nosepbpjs + "</button></td>";
					} else {
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						tabel_rawat_inap_isi += "<td align='center'><button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Input SEP' onclick='modal_input_sep("+dataRawatInap.n_reg_id+","+nama_pasien+")'><i class='material-icons'>add_circle</i></button></td>";
					}
					tabel_rawat_inap_isi +=
									"<td>" + n_Koding_hak + "</td>" +
									"<td>" + n_codding_ditempati + "</td>" +
									"<td nowrap>" + numeral(dataRawatInap.sum_real_cost).format('0,0') + "</td>";

					tabel_rawat_inap_isi += "<td nowrap>"; //BUKA BUAT PDF
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' onclick='modal_buat_pdf_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>group_work</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
					}
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							/*var waktu_awal = "";
							var waktu_akhir = "";
							$.ajax({
								url: base_url+"RawatInap/get_waktu_awal_akhir",
								type: "post",
								data: {
									n_reg_id: dataRawatInap.n_reg_id
								},
								datatype: 'json',
								async: false,
								success: function(response_waktu_awal_akhir) {
									var response_waktu_awal_akhir_parse = JSON.parse(response_waktu_awal_akhir);
									waktu_awal = '"'+response_waktu_awal_akhir_parse.waktu_awal+'"';
									waktu_akhir = '"'+response_waktu_awal_akhir_parse.waktu_akhir+'"';
								}
							});*/
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' onclick='modal_buat_detail_pdf_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>note</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						$.ajax({
							url: base_url+"RawatInap/get_vnoteno_by_nregid",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_vnoteno) {
								var response_vnoteno_parse = JSON.parse(response_vnoteno);
								//console.log(response_vnoteno_parse);
								if (response_vnoteno_parse != null) {
									var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
									var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' onclick='modal_buat_pdf_lab_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>hourglass_empty</i></button>";
								} else {
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
								}
							}
						});
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						if (dataRawatInap.n_codding_status == 2) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' onclick='modal_buat_pdf_iuran("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>attach_money</i></button>";
						} else {
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
					}
					tabel_rawat_inap_isi += "</td>"; //TUTUP BUAT PDF

					tabel_rawat_inap_isi +=
									"<td>" + tanggal_koding + "</td>" +
									"<td>" + dataRawatInap.status + "</td>" +
									"<td nowrap>";

					if (dataRawatInap.v_nosepbpjs != null) {
						var v_nosepbpjs = '"'+dataRawatInap.v_nosepbpjs+'"';
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' onclick='minta_koding("+dataRawatInap.n_reg_id+","+v_nosepbpjs+")'><i class='material-icons'>code</i></button>";
					} else { 
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' disabled><i class='material-icons'>code</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						var tglsep = "";
						$.ajax({
							url: base_url+"RawatInap/get_tanggal_sep",
							type: "post",
							data: {
								v_nosepbpjs: dataRawatInap.v_nosepbpjs
							},
							datatype: 'json',
							async: false,
							success: function(response_tanggal_sep) {
								var response_tanggal_sep_parse = JSON.parse(response_tanggal_sep);
								tglsep = '"'+response_tanggal_sep_parse.tglsep+'"';
							}
						});
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' onclick='modal_upload_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>file_upload</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' disabled><i class='material-icons'>file_upload</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' onclick='modal_lihat_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>pageview</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' disabled><i class='material-icons'>pageview</i></button>";
					}
					tabel_rawat_inap_isi +=	
									"</td>" +
								"</tr>";
					nomor_tabel_rawat_inap++;
				});
				var tabel_rawat_inap_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_rawat_inap = tabel_rawat_inap_awal+tabel_rawat_inap_isi+tabel_rawat_inap_akhir;
				$('#parent_tabel_rawat_inap').html(hasil_tabel_rawat_inap);
				$('#tabel_rawat_inap').DataTable();
			}
		});
	}
	$('#ward_name').val($('#ward_name option:first').val()).trigger('change');
	$('.selectpicker').selectpicker('refresh');
}
function cari_rawat_inap_pasien_pulang(ward_name) {
	if (!ward_name) {
		swal({   
			title: "Gagal!",
			text: "Data anda gagal dicari, silahkan mengisi data dengan benar",
			timer: 1500,
			showConfirmButton: false,
			type: "error",
		});
	} else {
		$.ajax({
			url: base_url+"RawatInap/cari_rawat_inap",
			type: "post",
			data: {
				ward_name: ward_name
			},
			datatype: 'json',
			success: function(response) {
				var response_parse = JSON.parse(response);
				$('#parent_tabel_rawat_inap').empty();
				$('#pemberitahuan_input').hide();
				var tabel_rawat_inap_awal =
						// "<div class='header' align='center' style='margin-top:-20px;'>\
						// 	<span><h2>RUANG RAWAT INAP "+response_parse.nama_ward+"</h2></span>\
						// </div>" +
						// "<input type='hidden' id='ward_name_hidden' value='"+ward_name+"'>"+
						"<table class='table table-bordered table-striped table-hover' id='tabel_rawat_inap'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Tanggal Masuk</th>"+
								"<th>LOS</th>"+
								"<th>Nomor Bed</th>"+
								"<th>Nomor MR</th>"+
								"<th>Nama Pasien</th>"+
								"<th>Nomor SEP</th>"+
								"<th>Koding Hak</th>"+
								"<th>Koding Ditempati</th>"+
								"<th>Real Cost</th>"+
								"<th style='text-align:center;'>Buat PDF</th>"+
								"<th>Tanggal Koding</th>"+
								"<th>Status Koding</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
			
				var nomor_tabel_rawat_inap = 1;
				var tabel_rawat_inap_isi = "";
				$.each(response_parse.rawat_inap, function (index, dataRawatInap) {
						var n_Koding_hak;
						var n_codding_ditempati;
						var tanggal_koding;
						$.ajax({
							url: base_url+"RawatInap/get_last_koding",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_last_koding) {
								var response_last_koding_parse = JSON.parse(response_last_koding);
								if (response_last_koding_parse != null) {
									//KODING HAK
									if (response_last_koding_parse.n_codding_hak != null) {
										n_Koding_hak = numeral(response_last_koding_parse.n_codding_hak).format('0,0');
									} else {
										n_Koding_hak = '-';
									}
									//KODING HAK
									//KODING DITEMPATI
									if (response_last_koding_parse.n_codding_ditempati != null) {
										n_codding_ditempati = numeral(response_last_koding_parse.n_codding_ditempati).format('0,0');
									} else {
										n_codding_ditempati = '-';
									}
									//KODING DITEMPATI
									//TANGGAL KODING
									if (response_last_koding_parse.tanggal_koding != null) {
										tanggal_koding = moment(response_last_koding_parse.tanggal_koding).format('DD/MM/YYYY');
									} else {
										tanggal_koding = '-';
									}
									//TANGGAL KODING
								} else {
									n_Koding_hak = '-';
									n_codding_ditempati = '-';
									tanggal_koding = '-';
								}
							}
						});
					//LOS
					var date1 = moment(dataRawatInap.date);
					var date2 = moment();
					var diff = date2.diff(date1, 'days');
					//LOS
					tabel_rawat_inap_isi +=
								"<tr>"+
									"<td>" + nomor_tabel_rawat_inap + "</td>" +
									"<td>" + moment(dataRawatInap.date).format('DD/MM/YYYY') + "</td>" +
									"<td nowrap>" + diff + " hari</td>" +
									"<td>" + dataRawatInap.v_bed_desc + "</td>" +
									"<td nowrap>" + (dataRawatInap.v_mr_code).replace(/\-/g,"") + "</td>" +
									"<td>" + dataRawatInap.v_patient_name + "</td>";
					if (dataRawatInap.v_nosepbpjs != null) {
						var no_sep = '"'+dataRawatInap.v_nosepbpjs+'"';
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						tabel_rawat_inap_isi += "<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Edit SEP' onclick='modal_edit_sep("+dataRawatInap.n_reg_id+","+no_sep+","+nama_pasien+")'>" + dataRawatInap.v_nosepbpjs + "</button></td>";
					} else {
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						tabel_rawat_inap_isi += "<td align='center'><button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Input SEP' onclick='modal_input_sep("+dataRawatInap.n_reg_id+","+nama_pasien+")'><i class='material-icons'>add_circle</i></button></td>";
					}
					tabel_rawat_inap_isi +=
									"<td>" + n_Koding_hak + "</td>" +
									"<td>" + n_codding_ditempati + "</td>" +
									"<td nowrap>" + numeral(dataRawatInap.sum_real_cost).format('0,0') + "</td>";

					tabel_rawat_inap_isi += "<td nowrap>"; //BUKA BUAT PDF
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' onclick='modal_buat_pdf_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>group_work</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
					}
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							/*var waktu_awal = "";
							var waktu_akhir = "";
							$.ajax({
								url: base_url+"RawatInap/get_waktu_awal_akhir",
								type: "post",
								data: {
									n_reg_id: dataRawatInap.n_reg_id
								},
								datatype: 'json',
								async: false,
								success: function(response_waktu_awal_akhir) {
									var response_waktu_awal_akhir_parse = JSON.parse(response_waktu_awal_akhir);
									waktu_awal = '"'+response_waktu_awal_akhir_parse.waktu_awal+'"';
									waktu_akhir = '"'+response_waktu_awal_akhir_parse.waktu_akhir+'"';
								}
							});*/
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' onclick='modal_buat_detail_pdf_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>note</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						$.ajax({
							url: base_url+"RawatInap/get_vnoteno_by_nregid",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_vnoteno) {
								var response_vnoteno_parse = JSON.parse(response_vnoteno);
								//console.log(response_vnoteno_parse);
								if (response_vnoteno_parse != null) {
									var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
									var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' onclick='modal_buat_pdf_lab_pasien_pulang("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>hourglass_empty</i></button>";
								} else {
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
								}
							}
						});
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						if (dataRawatInap.n_codding_status == 2) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' onclick='modal_buat_pdf_iuran("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>attach_money</i></button>";
						} else {
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
					}
					tabel_rawat_inap_isi += "</td>"; //TUTUP BUAT PDF

					tabel_rawat_inap_isi +=
									"<td>" + tanggal_koding + "</td>" +
									"<td>" + dataRawatInap.status + "</td>" +
									"<td nowrap>";

					if (dataRawatInap.v_nosepbpjs != null) {
						var v_nosepbpjs = '"'+dataRawatInap.v_nosepbpjs+'"';
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' onclick='minta_koding("+dataRawatInap.n_reg_id+","+v_nosepbpjs+")'><i class='material-icons'>code</i></button>";
					} else { 
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' disabled><i class='material-icons'>code</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						var tglsep = "";
						$.ajax({
							url: base_url+"RawatInap/get_tanggal_sep",
							type: "post",
							data: {
								v_nosepbpjs: dataRawatInap.v_nosepbpjs
							},
							datatype: 'json',
							async: false,
							success: function(response_tanggal_sep) {
								var response_tanggal_sep_parse = JSON.parse(response_tanggal_sep);
								tglsep = '"'+response_tanggal_sep_parse.tglsep+'"';
							}
						});
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' onclick='modal_upload_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>file_upload</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' disabled><i class='material-icons'>file_upload</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' onclick='modal_lihat_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>pageview</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' disabled><i class='material-icons'>pageview</i></button>";
					}
					tabel_rawat_inap_isi +=	
									"</td>" +
								"</tr>";
					nomor_tabel_rawat_inap++;
				});
				var tabel_rawat_inap_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_rawat_inap = tabel_rawat_inap_awal+tabel_rawat_inap_isi+tabel_rawat_inap_akhir;
				$('#parent_tabel_rawat_inap').html(hasil_tabel_rawat_inap);
				$('#tabel_rawat_inap').DataTable();
			}
		});
	}
	$('#ward_name').val($('#ward_name option:first').val()).trigger('change');
	$('.selectpicker').selectpicker('refresh');
}
function modal_buat_detail_pdf_pasien_pulang(n_reg_id,nosep,tglsep,sum_real_cost) {
	console.log(n_reg_id);
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatInapAll/cari_detail_nilai_paket_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			sum_real_cost: sum_real_cost
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			$('#nosep_buat_detail_pdf_hidden').val('');
			$('#regid_buat_detail_pdf_hidden').val('');
			$('#tglsep_buat_detail_pdf_hidden').val('');
			$('#nosep_buat_detail_pdf_hidden').val(nosep);
			$('#regid_buat_detail_pdf_hidden').val(n_reg_id);
			$('#tglsep_buat_detail_pdf_hidden').val(tglsep);
			$('#parent_buat_detail_pdf').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;'>" +
					"<tr>" +
						"<td style='padding: 5px;' colspan='5'>KAMAR KEPERAWATAN</td>" +
					"</tr>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;'>NOTA RUANGAN</td>" +
						"<td style='padding: 5px;'>TANGGAL MASUK</td>" +
						"<td style='padding: 5px;'>TANGGAL KELUAR</td>" +
						"<td style='padding: 5px;'>JUMLAH JAM</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			
			var total_nota_paket = 0;
			$.each(response_parse.kasur_detail, function (index, dataKasurDetail) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.nota + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.masuk).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.keluar).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.jam + "</td>" +
						"<td style='padding: 5px;text-align:right;'>" + numeral(dataKasurDetail.total).format('0,0') + "</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(response_parse.sum_fungsi_kasur_detail).format('0,0') +"</td>" +
					"</tr>" +
				"</table><br/>";
			tabel_buat_pdf +=
				"<table style='width:100%;'>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;' colspan='4'>KELOMPOK BIAYA</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			$.each(response_parse.cetak_byreg, function (index, dataCetakByReg) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;font-size: 18px;' colspan='5'>" + dataCetakByReg.kelompok + "</td>" +
					"</tr>";
				$.ajax({
					url: base_url+"RawatInap/get_cetak_byreg_kojek_by_kelompok",
					type: "post",
					data: {
						n_reg_id: n_reg_id,
						kelompok: dataCetakByReg.kelompok
					},
					datatype: 'json',
					async: false,
					success: function(response_by_kelompok) {
						var response_by_kelompok_parse = JSON.parse(response_by_kelompok);
						$.each(response_by_kelompok_parse, function (index, dataByKelompok) {
							tabel_buat_pdf +=
								"<tr>" +
									"<td>&emsp;&emsp;</td>" +
									"<td style='padding: 5px;'>" + moment(dataByKelompok.tgl).format('DD/MM/YYYY') + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.ket + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.qty + "</td>" +
									"<td style='padding: 5px;text-align:right;'>" + numeral(dataByKelompok.subttl).format('0,0') + "</td>" +
								"</tr>";
						});
					}
				});
				tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(dataCetakByReg.sum_total).format('0,0') +"</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
				"</table>" +
				"<table align='right'>" +
					"<tr>" +
						"<td style='padding: 5px;'><b>Jumlah Biaya : Rp. " + numeral(response_parse.sum_real_cost).format('0,0') +"</b></td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td><br/><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			$('#parent_buat_detail_pdf').html(tabel_buat_pdf);
			if (nosep != 'null') {
				$('#judul_buat_detail_pdf').html('(Nomor SEP: '+nosep+')');
			} else {
				$('#judul_buat_detail_pdf').html('');
			}
			$('#modal_buat_detail_pdf').modal('show');
		}
	});
}

//LIS
function modal_buat_pdf_lab_pasien_pulang(n_reg_id,nosep,tglsep) {
	var tabel_buat_pdf_lab = "";
	var tabel_buat_pdf_lab_isi = "";
	var cek_rekap = "";
	$.ajax({
		url: base_url+"RawatInapAll/cari_lab_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_lab_hidden').val('');
			$('#regid_buat_pdf_lab_hidden').val('');
			$('#tglsep_buat_pdf_lab_hidden').val('');
			$('#nosep_buat_pdf_lab_hidden').val(nosep);
			$('#regid_buat_pdf_lab_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_lab_hidden').val(tglsep);
			$('#parent_buat_pdf_lab').empty();
			//console.log(response_parse);
			tabel_buat_pdf_lab +=
				"REKAP HASIL LABORAT<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;' id='tabel_rekap_hasil_lab'>";
				$.each(response_parse.list_vnoteno, function (index, dataVnoteno) {
					$.ajax({
						url: base_url+"RawatInap/cari_lab_buat_pdf_rekap",
						type: "post",
						data: {
							vnoteno: dataVnoteno.v_note_no
						},
						datatype: 'json',
						async: false,
						success: function(response_rekap) {
							var response_rekap_parse = JSON.parse(response_rekap);
							//console.log(response_rekap_parse);
							if (response_rekap_parse != null) {
								tabel_buat_pdf_lab +=
									"<tr style='border-bottom: 1px solid black;border-top: 1px solid black;border-collapse: collapse;'>" +
										"<td colspan='4' style='padding: 5px;'>" + dataVnoteno.v_note_no + "</td>" +
									"</tr>" +
									"<tr>" +
										"<td>Nama Tes</td>" +
										"<td>Hasil Tes</td>" +
										"<td>Nilai Normal</td>" +
										"<td>Nama Unit Tes</td>" +
									"</tr>";
								if (response_rekap_parse != null) {
									$.each(response_rekap_parse, function (index, dataRekap) {
										if (dataRekap.test_name != "") {
											var nama_tes = dataRekap.test_name;
										} else {
											var nama_tes = '-';
										}
										if (dataRekap.hasil != "") {
											var hasil_tes = dataRekap.hasil;
										} else {
											var hasil_tes = '-';
										}
										if (dataRekap.nilai_normal != "") {
											var nilai_normal = dataRekap.nilai_normal;
										} else {
											var nilai_normal = '-';
										}
										if (dataRekap.test_units_name != "") {
											var nama_unit_tes = dataRekap.test_units_name;
										} else {
											var nama_unit_tes = '-';
										}
										tabel_buat_pdf_lab_isi +=
											"<tr>" +
												"<td>"+nama_tes+"</td>" +
												"<td>"+hasil_tes+"</td>" +
												"<td>"+nilai_normal+"</td>" +
												"<td>"+nama_unit_tes+"</td>" +
											"</tr>";
									});
									cek_rekap += response_rekap_parse;
								}
							}
						}
					});
				tabel_buat_pdf_lab += tabel_buat_pdf_lab_isi;
			});
			tabel_buat_pdf_lab +=
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td colspan='5'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			if (cek_rekap != "") {
				$('#parent_buat_pdf_lab').html(tabel_buat_pdf_lab);
				if (nosep != 'null') {
					$('#judul_buat_pdf_lab').html('(Nomor SEP: '+nosep+')');
				} else {
					$('#judul_buat_pdf_lab').html('');
				}
				$('#modal_buat_pdf_lab').modal('show');
			} else {
				swal({   
					title: "Data hasil rekap laborat belum ada",
					type: "error",
				});
			}
		}
	});
	
}
//LIS

//DETAIL IURAN
function modal_buat_pdf_iuran(n_reg_id,nosep,tglsep) {
	var tabel_buat_pdf_lab = "";
	var tabel_buat_pdf_lab_isi = "";
	var cek_rekap = "";
	$.ajax({
		url: base_url+"RawatInapAll/cari_iuran_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_iuran_hidden').val('');
			$('#regid_buat_pdf_iuran_hidden').val('');
			$('#tglsep_buat_pdf_iuran_hidden').val('');
			$('#nosep_buat_pdf_iuran_hidden').val(nosep);
			$('#regid_buat_pdf_iuran_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_iuran_hidden').val(tglsep);
			$('#parent_buat_pdf_iuran').empty();
			//console.log(response_parse);
			tabel_buat_pdf_lab +=
				"PERINCIAN DETAIL IURAN<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
						"<td style='padding-right: 5px;' valign='top'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
					"</tr>" +
					/*"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
					"</tr>" +*/
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td><br/></td>" +
						"<td><br/></td>" +
						"<td><br/></td>" +
					"</tr>" +
				"</table>" +
				"<table style='width:40%;' id='tabel_detail_iuran'>" +
					"<tr>" +
						"<td>Nilai Koding Hak</td>" +
						"<td align='right'>" + response_parse.iuran.n_codding_hak + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td>Nilai Koding Ditempati</td>" +
						"<td align='right'>" + response_parse.iuran.n_codding_ditempati + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td>Iuran</td>" +
						"<td style='border-top: 1px solid black;border-collapse: collapse;' align='right'>" + response_parse.iuran.n_iur_max + "</td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td colspan='5'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			$('#parent_buat_pdf_iuran').html(tabel_buat_pdf_lab);
			if (nosep != 'null') {
				$('#judul_buat_pdf_iuran').html('(Nomor SEP: '+nosep+')');
			} else {
				$('#judul_buat_pdf_iuran').html('');
			}
			$('#modal_buat_pdf_iuran').modal('show');
		}
	});
}