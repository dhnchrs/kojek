$('#button_cari_rawat_jalan').click(function(){
	var tanggal_awal = $("#tanggal_awal").val();
	var tanggal_akhir = $("#tanggal_akhir").val();
	cari_rawat_jalan(tanggal_awal, tanggal_akhir);
});
function cari_rawat_jalan(tanggal_awal, tanggal_akhir) {
	if (!tanggal_awal && !tanggal_akhir) {
		swal({   
			title: "Gagal!",
			text: "Data anda gagal dicari, silahkan mengisi data dengan benar",
			timer: 1500,
			showConfirmButton: false,
			type: "error",
		});
	} else {
		$.ajax({
			url: base_url+"RawatJalan/cari_rawat_jalan",
			type: "post",
			data: {
				tanggal_awal: tanggal_awal,
				tanggal_akhir: tanggal_akhir
			},
			datatype: 'json',
			success: function(response) {
				var response_parse = JSON.parse(response);
				$('#parent_tabel_rawat_jalan').empty();
				$('#pemberitahuan_input').hide();
				var tabel_rawat_jalan_awal =
						"<input type='hidden' id='tanggal_awal_hidden' value='"+tanggal_awal+"'>"+
						"<input type='hidden' id='tanggal_akhir_hidden' value='"+tanggal_akhir+"'>"+
						"<table class='table table-bordered table-striped table-hover' id='tabel_rawat_jalan'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Tanggal SEP</th>"+
								"<th>Nomor MR</th>"+
								"<th>Poli</th>"+
								"<th>Nama</th>"+
								"<th>Diagnosa</th>"+
								"<th>Nomor SEP</th>"+
								"<th>Real Cost</th>"+
								"<th>Nota Paket</th>"+
								"<th>Laborat</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
			
				var nomor_tabel_rawat_jalan = 1;
				var tabel_rawat_jalan_isi = "";
				$.each(response_parse, function (index, dataRawatJalan) {
						var gabungan_poli_unit = "";
						$.ajax({
							url: base_url+"RawatJalan/cari_gabungan_poli_unit",
							type: "post",
							data: {
								mr_id: dataRawatJalan.n_mr_id,
								// tanggal_awal: tanggal_awal,
								// tanggal_akhir: tanggal_akhir,
								no_sep:dataRawatJalan.v_nosepbpjs
							},
							datatype: 'json',
							async: false,
							success: function(response_gabungan_poli) {
								var response_gabungan_poli_parse = JSON.parse(response_gabungan_poli);
								if (response_gabungan_poli_parse != null) {
									$.each(response_gabungan_poli_parse,function(index,dataPoli){
										if (dataPoli.v_reg_secondary_id != null && dataPoli.v_staff_name != null) {
											gabungan_poli_unit += "<b><i>" + dataPoli.v_reg_secondary_id + "</b></i> - " + dataPoli.v_staff_name + "<br>";
										} else {
											gabungan_poli_unit += "<b><i>" + dataPoli.v_reg_secondary_id + "</b></i>" + "<br>";
										}
									});
								}
							}
						});
					if (dataRawatJalan.tglsep != null) {
						var tglsep = moment(dataRawatJalan.tglsep).format('DD/MM/YYYY');
					} else {
						var tglsep = "-";
					}
					if (dataRawatJalan.diagnosaawal != null) {
						var diagnosaawal = dataRawatJalan.diagnosaawal;
					} else {
						var diagnosaawal = "-";
					}
					if (dataRawatJalan.v_nosepbpjs != null) {
						var v_nosepbpjs = '"'+dataRawatJalan.v_nosepbpjs+'"';
						var nama_pasien = '"'+(dataRawatJalan.v_patient_name).replace("'","")+'"';
						/*var tanggal_awal_lempar = '"'+tanggal_awal+'"';
						var tanggal_akhir_lempar = '"'+tanggal_akhir+'"';*/
						var v_nosepbpjs = "<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Input SEP' onclick='modal_input_sep_gabung(2,"+dataRawatJalan.n_mr_id+","+nama_pasien+","+v_nosepbpjs+")'>"+dataRawatJalan.v_nosepbpjs+"</button>";
					} else {
						var v_nosepbpjs = ' ';
						var nama_pasien = '"'+(dataRawatJalan.v_patient_name).replace(/'/g,"")+'"';
						/*var tanggal_awal_lempar = '"'+tanggal_awal+'"';
						var tanggal_akhir_lempar = '"'+tanggal_akhir+'"';*/
						var v_nosepbpjs = "<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Input SEP' onclick='modal_input_sep_gabung(1,"+dataRawatJalan.n_mr_id+","+nama_pasien+","+v_nosepbpjs+")'><i class='material-icons'>add_circle</i></button>";
					}
					if (dataRawatJalan.v_staff_name != null) {
						var v_staff_name = dataRawatJalan.v_staff_name;
					} else {
						var v_staff_name = "";
					}
					tabel_rawat_jalan_isi +=
								"<tr>"+
									"<td>" + nomor_tabel_rawat_jalan + "</td>" +
									"<td>" + tglsep + "</td>" +
									"<td>" + dataRawatJalan.n_mr_id + "</td>" +
									"<td>" + gabungan_poli_unit + "</td>" +
									"<td>" + dataRawatJalan.v_patient_name + "</td>" +
									"<td>" + diagnosaawal + "</td>" +
									"<td align='center'>" + v_nosepbpjs + "</td>" +
									"<td>" + numeral(dataRawatJalan.sum_real_cost).format('0,0') + "</td>";
					if (dataRawatJalan.sum_real_cost != null) {
						if (dataRawatJalan.v_nosepbpjs != null) {
							var tanggal_awal_lempar = '"'+tanggal_awal+'"';
							var tanggal_akhir_lempar = '"'+tanggal_akhir+'"';
							var tglsep_lempar = '"'+dataRawatJalan.tglsep+'"';
							var nosep_lempar = '"'+dataRawatJalan.v_nosepbpjs+'"';
							tabel_rawat_jalan_isi +=
									"<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Buat PDF' onclick='modal_buat_pdf_rajal("+dataRawatJalan.n_mr_id+","+tanggal_awal_lempar+","+tanggal_akhir_lempar+","+dataRawatJalan.sum_real_cost+","+nosep_lempar+","+tglsep_lempar+")'>"+numeral(dataRawatJalan.sum_real_cost).format('0,0')+"</button></td>";
						} else {
							tabel_rawat_jalan_isi +=
									"<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Upload PDF' disabled>"+numeral(dataRawatJalan.sum_real_cost).format('0,0')+"</button></td>";
						}
					} else {
						tabel_rawat_jalan_isi +=
									"<td>"+numeral(dataRawatJalan.sum_real_cost).format('0,0')+"</td>";
					}
					if (dataRawatJalan.v_nosepbpjs != null) {
						$.ajax({
							url: base_url+"RawatJalan/get_vnoteno_by_nmrid",
							type: "post",
							data: {
								n_mr_id: dataRawatJalan.n_mr_id,
								tanggal_awal: tanggal_awal,
								tanggal_akhir: tanggal_akhir
							},
							datatype: 'json',
							async: false,
							success: function(response_vnoteno) {
								var response_vnoteno_parse = JSON.parse(response_vnoteno);
								//console.log(response_vnoteno_parse);
								if (response_vnoteno_parse != null) {
									var tanggal_awal_lempar = '"'+tanggal_awal+'"';
									var tanggal_akhir_lempar = '"'+tanggal_akhir+'"';
									var tglsep_lempar = '"'+dataRawatJalan.tglsep+'"';
									var nosep_lempar = '"'+dataRawatJalan.v_nosepbpjs+'"';
									tabel_rawat_jalan_isi +=
										"<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Buat PDF' onclick='modal_buat_pdf_lab_rajal("+dataRawatJalan.n_mr_id+","+tanggal_awal_lempar+","+tanggal_akhir_lempar+","+nosep_lempar+","+tglsep_lempar+")'>LILIS</button></td>";
								} else {
									tabel_rawat_jalan_isi += '<td>-</td>';
								}
							}
						});
					} else {
						tabel_rawat_jalan_isi +=
									"<td><button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' title='Buat PDF' disabled>LILIS</button></td>";
					}
					tabel_rawat_jalan_isi += "<td nowrap>";
					if (dataRawatJalan.v_nosepbpjs != null) {
						var tglsep_lempar = '"'+dataRawatJalan.tglsep+'"';
						var nosep_lempar = '"'+dataRawatJalan.v_nosepbpjs+'"';
						tabel_rawat_jalan_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' onclick='modal_upload_file_rajal("+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>file_upload</i></button>";
					} else {
						tabel_rawat_jalan_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' disabled><i class='material-icons'>file_upload</i></button>";
					}
					if (dataRawatJalan.v_nosepbpjs != null) {
						tabel_rawat_jalan_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' onclick='modal_lihat_file_rajal("+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>pageview</i></button>";
					} else {
						tabel_rawat_jalan_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' disabled><i class='material-icons'>pageview</i></button>";
					}
					tabel_rawat_jalan_isi +=	
									"</td>" +
								"</tr>";
					nomor_tabel_rawat_jalan++;
				});
				var tabel_rawat_jalan_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_rawat_jalan = tabel_rawat_jalan_awal+tabel_rawat_jalan_isi+tabel_rawat_jalan_akhir;
				$('#parent_tabel_rawat_jalan').html(hasil_tabel_rawat_jalan);
				$('#tabel_rawat_jalan').DataTable();
			}
		});
	}
}
$("#konfirmasi_input").change(function() {
    if(this.checked) {
        $('#parent_text_input_sep').show();
    } else {
    	$('#parent_text_input_sep').hide();
    }
});
$('#input_nosep_gabungan').click(function(){
	input_sep();
});
$('#text_input_sep').click(function(){
	$('#input_nosep_gabungan').val($('#nosep').val());
	if ($('#input_nosep_gabungan').val() != ' '){
		if ($('#cek_edit_atau_simpan_sep').val() == 2) {
			$('#parent_sep_gabung').hide();
		} else {
			$('#parent_sep_gabung').show();
		}
	}
	$('#modal_input_sep').modal('hide');
});
function input_sep(){
	$("#nomor_rs").val('1124R005');
	$("#nomor_sep").val('');
	$("#konfirmasi_input").prop("checked", false);
	$('#nama_pasien_konfirmasi').val($('#nama_pasien_konfirmasi_input').val());
	$('#parent_text_input_sep').hide();
    $("#parent_keterangan_no_sep").hide();
	$('#modal_input_sep').modal('show');
}
$('#parent_sep_gabung').hide();
$("#cek_gabung_sep").change(function() {
    if(this.checked) {
        $('#parent_sep_gabung').show();
        $('#parent_button_edit_sep_rajal').hide();
        $('#parent_nosep_lama').hide();
        $('#input_nosep_gabungan').val($('#input_nosep_gabungan_lama').val());
    } else {
    	$('#parent_sep_gabung').hide();
    	$('#parent_button_edit_sep_rajal').show();
    	$('#parent_nosep_lama').show();
    	$('#input_nosep_gabungan').val(' ');
    }
});
function modal_input_sep_gabung(cek,n_mr_id,nama_pasien,nosep){
	//CEK jika 1 insert jika 2 update
	$('#cek_edit_atau_simpan_sep').val(cek);
	if (cek == 2) {
		$('#parent_sep_gabung').show();
		$('#parent_button_edit_sep_rajal').show();
		$('#parent_cek_gabung_sep').show();
		$('#parent_nosep_lama').show();
	} else {
		$('#parent_sep_gabung').hide();
		$('#parent_button_edit_sep_rajal').hide();
		$('#parent_cek_gabung_sep').hide();
		$('#parent_nosep_lama').hide();
	}
	$('#nama_pasien_konfirmasi_input').val(nama_pasien);
	var tabel_sep_gabung = "";
	$.ajax({
		url: base_url+"RawatJalan/get_modal_sep_gabung",
		type: "post",
		data: {
			n_mr_id: n_mr_id
			/*tanggal_awal: tanggal_awal,
			tanggal_akhir: tanggal_akhir*/
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#parent_sep_gabung').hide();
			$("#cek_gabung_sep").prop("checked", false);
			$('#parent_tabel_modal_sep_gabung').empty();
			$('#parent_button_sep_gabung').empty();
			//console.log(response_parse);
			tabel_sep_gabung +=
						"<table class='table table-bordered table-striped table-hover'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Tanggal Periksa</th>"+
								"<th>Nomor Register</th>"+
								"<th>Poli</th>"+
								"<th>Nomor SEP</th>"+
							"</thead>"+
							"<tbody>";
			var nomor_sep_gabung = 1;
			$.each(response_parse, function (index, dataSEPGabung) {
				//console.log(dataSEPGabung);
				var no_sep = "";
				var disabled = "";
				if (dataSEPGabung.v_nosepbpjs != null) {
					no_sep = dataSEPGabung.v_nosepbpjs;
				} else {
					no_sep = '-';
				}
				if (cek == 1) {
					if (dataSEPGabung.v_nosepbpjs != null) {
						disabled = 'disabled';
					} else {
						disabled = '';
					}
				}
				tabel_sep_gabung +=
					"<tr>"+
						"<td>\
							<div style='width: 10px'>\
							<div class='demo-checkbox'>\
                                <input type='checkbox' name='konfirmasi_input_checkbox' id='konfirmasi_input_checkbox"+nomor_sep_gabung+"' "+disabled+" value='"+dataSEPGabung.n_reg_id+"'/>\
                                <label for='konfirmasi_input_checkbox"+nomor_sep_gabung+"'></label>\
                            </div>\
                            </div>\
						</td>" +
						"<td>" + moment(dataSEPGabung.tgl_reg).format('DD/MM/YYYY') + "</td>" +
						"<td>" + dataSEPGabung.v_reg_secondary_id + "</td>" +
						"<td>" + dataSEPGabung.v_unit_name + "</td>" +
						"<td>" + no_sep + "</td>" +
					"</tr>";
				nomor_sep_gabung++;
				console.log(dataSEPGabung.n_reg_id);
			});
			if (cek == 1) {
				var button_sep_gabung = "<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id='button_simpan_sep_rajal' onclick='simpan_sep_rajal()' style='margin-top: 5px;margin-left: -2px;'><i class='material-icons'>save</i> Simpan</button>";
			} else {
				var button_sep_gabung = "<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' onclick='gabung_sep_rajal()' style='margin-top: 5px;margin-left: -2px;'><i class='material-icons'>edit</i> Gabung SEP</button>";
			}
			$('#parent_tabel_modal_sep_gabung').html(tabel_sep_gabung);
			$('#parent_button_sep_gabung').html(button_sep_gabung);
			$('#nosep_lama').val(nosep);
			$('#input_nosep_gabungan_lama').val(nosep);
			$('#input_nosep_gabungan').val(' ');
			$('#modal_input_sep_gabung').modal('show');
		}
	});
}
function simpan_sep_rajal() {
	var regid_checkbox = [];
	$.each($("input[name='konfirmasi_input_checkbox']:checked"), function(){
		regid_checkbox.push($(this).val());
	});
	$.ajax({
		url: base_url+"RawatJalan/simpan_sep_rajal",
		type: "post",
		data: {
			nosep: $("#nosep").val(),
			tglsep: $("#tglsep").val(),
			nokartu: $("#nokartu").val(),
			namapeserta: $("#namapeserta").val(),
			peserta: $("#peserta").val(),
			tgllahir: $("#tgllahir").val(),
			jnskelamin: $("#jnskelamin").val(),
			politujuan: $("#politujuan").val(),
			jnsrawat: $("#jnsrawat").val(),
			klsrawat: $("#klsrawat").val(),
			diagnosaawal: $("#diagnosaawal").val(),
			catatan: $("#catatan").val(),
			regid: regid_checkbox,
			idkelasstandart: $("#idkelasstandart").val(),
			no_rujukan: $("#no_rujukan").val()
		},
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			if (response_parse == 'sukses') {
				$('#modal_input_sep').modal('hide');
				$('#modal_input_sep_gabung').modal('hide');
				var tanggal_awal = $("#tanggal_awal_hidden").val();
				var tanggal_akhir = $("#tanggal_akhir_hidden").val();
				setTimeout(function(){ cari_rawat_jalan(tanggal_awal, tanggal_akhir); }, 1500);
				swal({   
					title: "Sukses!",
					text: "Data SEP anda berhasil disimpan",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			} else {
				swal({   
					title: "Gagal!",
					text: "Input SEP gagal, nomor SEP sudah ada",
					type: "error",
				});
			}
		}
	});
}
function edit_sep_rajal() {
	$.ajax({
		url: base_url+"RawatJalan/edit_sep_rajal",
		type: "post",
		data: {
			nosep_lama: $("#input_nosep_gabungan_lama").val(),
			nosep: $("#nosep").val(),
			tglsep: $("#tglsep").val(),
			nokartu: $("#nokartu").val(),
			namapeserta: $("#namapeserta").val(),
			peserta: $("#peserta").val(),
			tgllahir: $("#tgllahir").val(),
			jnskelamin: $("#jnskelamin").val(),
			politujuan: $("#politujuan").val(),
			jnsrawat: $("#jnsrawat").val(),
			klsrawat: $("#klsrawat").val(),
			diagnosaawal: $("#diagnosaawal").val(),
			catatan: $("#catatan").val(),
			//regid: kan carinya berdasar MR bukan REG ID,
			idkelasstandart: $("#idkelasstandart").val(),
			no_rujukan: $("#no_rujukan").val()
		},
		success: function(response) {
			//console.log(response);
			$('#modal_input_sep').modal('hide');
			$('#modal_input_sep_gabung').modal('hide');
			var tanggal_awal = $("#tanggal_awal_hidden").val();
			var tanggal_akhir = $("#tanggal_akhir_hidden").val();
			setTimeout(function(){ cari_rawat_jalan(tanggal_awal, tanggal_akhir); }, 1500);
			swal({   
				title: "Sukses!",
				text: "Data SEP anda berhasil diedit",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}
function gabung_sep_rajal() {
	var regid_checkbox = [];
	$.each($("input[name='konfirmasi_input_checkbox']:checked"), function(){
		regid_checkbox.push($(this).val());
	});
	$.ajax({
		url: base_url+"RawatJalan/gabung_sep_rajal",
		type: "post",
		data: {
			nosep: $("#nosep").val(),
			tglsep: $("#tglsep").val(),
			nokartu: $("#nokartu").val(),
			namapeserta: $("#namapeserta").val(),
			peserta: $("#peserta").val(),
			tgllahir: $("#tgllahir").val(),
			jnskelamin: $("#jnskelamin").val(),
			politujuan: $("#politujuan").val(),
			jnsrawat: $("#jnsrawat").val(),
			klsrawat: $("#klsrawat").val(),
			diagnosaawal: $("#diagnosaawal").val(),
			catatan: $("#catatan").val(),
			regid: regid_checkbox,
			idkelasstandart: $("#idkelasstandart").val(),
			no_rujukan: $("#no_rujukan").val()
		},
		success: function(response) {
			//console.log(response);
			$('#modal_input_sep').modal('hide');
			$('#modal_input_sep_gabung').modal('hide');
			var tanggal_awal = $("#tanggal_awal_hidden").val();
			var tanggal_akhir = $("#tanggal_akhir_hidden").val();
			setTimeout(function(){ cari_rawat_jalan(tanggal_awal, tanggal_akhir); }, 1500);
			swal({   
				title: "Sukses!",
				text: "Data SEP anda berhasil diedit",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}
//PDF
function modal_upload_file_rajal(nosep, tglsep) {
	$('#no_sep_hidden_rajal').val('');
	$('#tgl_sep_hidden_rajal').val('');
	$('#no_sep_hidden_rajal').val(nosep);
	$('#tgl_sep_hidden_rajal').val(tglsep);
	$('#judul_upload_no_sep_rajal').html('(Nomor SEP: '+nosep+')');
	$('#modal_upload_file_rajal').modal('show');
	myDropzoneRajal.removeAllFiles();
}
Dropzone.autoDiscover = false;
var myDropzoneRajal = new Dropzone("div#mydropzone_rajal", {
	url: base_url+"RawatJalan/upload_file",
	autoProcessQueue: false,
	paramName: "file_diupload",
	clickable: true,
	maxFilesize: 20, //in mb
	addRemoveLinks: false,
	acceptedFiles: '.pdf',
	init: function() {
		this.on("sending", function(file, xhr, formData) {
		  	//console.log("sending file");
		  	formData.append('no_sep', $('#no_sep_hidden_rajal').val());
		  	if ($('#tgl_sep_hidden_rajal').val() == "null" || $('#tgl_sep_hidden_rajal').val() == null) {
				$.ajax({
					url: base_url+"RawatJalan/cari_tglsep_by_nosep",
					type: "post",
					data: {
						nosep: $('#no_sep_hidden_rajal').val()
					},
					dataType: 'json',
					async: false,
					success: function(response) {
						tglsep = response;
					}
				});
				formData.append('tgl_sep', tglsep);
			} else {
				formData.append('tgl_sep', $('#tgl_sep_hidden_rajal').val());
			}
		});
		this.on("success", function(file, responseText) {
		  	//console.log('great success');
		});
		this.on("addedfile", function(file){
			//console.log('file added');
		});
		var submitButton = document.querySelector("#button_upload_file_rajal");
		myDropzoneRajal = this;
		submitButton.addEventListener("click", function() {
			myDropzoneRajal.processQueue();
		});
	}
});

function modal_lihat_file_rajal(nosep, tglsep) {
	$('#parent_lihat_file_rajal').empty();
	var hasil_lihat_file_rajal = "<iframe src='' type='application/pdf' style='margin-left:15px;width:100%;height:420px;''></iframe>";
	$('#parent_lihat_file_rajal').html(hasil_lihat_file_rajal);
	nama_file_rajal(nosep, tglsep);
}
function nama_file_rajal(nosep, tglsep) {
	$.ajax({
		url: base_url+"RawatJalan/nama_file",
		type: "post",
		data: {
			nosep: nosep
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			if (response_parse != 'gagal') {
				$('#parent_nama_file_rajal').empty();

				var tabel_nama_file_awal =
						"<table class='table table-bordered table-striped table-hover'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Nama File</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
				
				var nomor_file = 1;
				var tabel_nama_file_isi = "";
				var nosep_response = response_parse.nosep;
				var tglsep_response = (response_parse.tglsep).replace(/\-/g,'');
				$.each(response_parse.file, function (index, dataNamaFile) {
					var dataNamaFile_lempar = '"'+dataNamaFile+'"';
					var nosep_response_lempar = '"'+nosep_response+'"';
					var tglsep_response_lempar = '"'+tglsep_response+'"';
					tabel_nama_file_isi +=
								"<tr class='row_nama_file_rajal' namaFilePilihRajal='"+dataNamaFile+","+nosep_response+","+tglsep_response+"'>"+
									"<td>" + nomor_file + "</td>" +
									"<td>" + dataNamaFile + ".pdf</td>" +
									"<td style='text-align:center;'><button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Hapus PDF' onclick='modal_hapus_pdf_rajal("+dataNamaFile_lempar+","+nosep_response_lempar+","+tglsep_response_lempar+")'><i class='material-icons'>delete</i></button></td>" +
								"</tr>";
					nomor_file++;
				});
				var tabel_nama_file_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_nama_file = tabel_nama_file_awal+tabel_nama_file_isi+tabel_nama_file_akhir;

				$('#parent_nama_file_rajal').html(hasil_tabel_nama_file);
				$('#judul_lihat_no_sep_rajal').html('(Nomor SEP: '+nosep_response+')');
				$('#modal_lihat_file_rajal').modal('show');
			} else {
				swal({
					title: "Gagal!",
					text: "File belum ada, silahkan upload file terlebih dahulu",
					type: "error",
				});
			}
		}
	});
}

$(document).on("click", ".row_nama_file_rajal", function () {
    var nama_file = $(this).attr('namaFilePilihRajal');
    var nama_file_array = nama_file.split(',');
    var nama_file_pdf = nama_file_array[0];
    var no_sep = nama_file_array[1];
    var tgl_sep = nama_file_array[2];
    lihat_file_rajal(no_sep,tgl_sep,nama_file_pdf);

    $(this).addClass('pilih_row').siblings().removeClass('pilih_row');
});
function lihat_file_rajal(no_sep,tgl_sep,nama_file_pdf) {
	$('#parent_lihat_file_rajal').empty();
	var url = base_url+"RawatJalan/lihat_file/"+no_sep+"/"+tgl_sep+"/"+nama_file_pdf;
	var hasil_lihat_file_rajal = "<iframe src='"+url+"' type='application/pdf' style='margin-left:15px;width:100%;height:420px;''></iframe>";
	$('#parent_lihat_file_rajal').html(hasil_lihat_file_rajal);
}

function modal_buat_pdf_rajal(n_mr_id,tanggal_awal,tanggal_akhir,sum_real_cost,nosep,tglsep) {
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatJalan/cari_nilai_paket_buat_pdf",
		type: "post",
		data: {
			n_mr_id: n_mr_id,
			tanggal_awal: tanggal_awal,
			tanggal_akhir: tanggal_akhir,
			sum_real_cost: sum_real_cost,
			nosep:nosep
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_hidden').val('');
			$('#norm_buat_pdf_hidden').val('');
			$('#tglsep_buat_pdf_hidden').val('');
			$('#nosep_buat_pdf_hidden').val(nosep);
			$('#norm_buat_pdf_hidden').val(n_mr_id);
			$('#tglsep_buat_pdf_hidden').val(tglsep);
			$('#parent_buat_pdf_rajal').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT JALAN<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					/*"<tr>" +
						"<td style='padding-right: 40px;'>Nama Penjamin</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
					"</tr>" +*/
					/*"<tr>" +
						"<td style='padding-right: 40px;'>No. Register</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
					"</tr>" +*/
					"<tr>" +
						"<td style='padding-right: 40px;' nowrap>No. Rekam Medik</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 40px;' nowrap>Nama Pasien</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 40px;' valign='top' nowrap>Alamat</td>" +
						"<td style='padding-right: 5px;' valign='top'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
					"</tr>" +
				"</table><br/>" +
				"<table style='width:100%;'>" +
					"<tr style='border-bottom: 1px solid black;border-top: 1px solid black;border-collapse: collapse;'>" +
						"<td colspan='2' style='padding: 5px;'>KELOMPOK BIAYA</td>" +
						"<td align='center' style='padding: 5px;'>JUMLAH</td>" +
						"<td align='right' style='padding: 5px;'>SUBTOTAL</td>" +
					"</tr>";
			$.each(response_parse.tb_examination, function (index, dataExamination) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td colspan='4' style='padding: 5px;'>" + dataExamination.v_note_no + "</td>" +
					"</tr>";
				$.ajax({
					url: base_url+"RawatJalan/cari_buat_pdf_treatment_item",
					type: "post",
					data: {
						n_note_id: dataExamination.n_exam_id
					},
					datatype: 'json',
					async: false,
					success: function(response_treatment_item) {
						var response_treatment_item_parse = JSON.parse(response_treatment_item);
						$.each(response_treatment_item_parse, function (index, dataTreatmentItem) {
							//console.log(dataTreatmentItem);
								if (dataTreatmentItem.dokter != null) {
									nama_dokter_treatment = ' - '+dataTreatmentItem.dokter;
								} else {
									nama_dokter_treatment = '';
								}
							tabel_buat_pdf +=
									"<tr>" +
										"<td style='padding-left: 5px;padding-right: 40px;'>" + moment(dataTreatmentItem.tanggal).format('DD/MM/YYYY') + "</td>" +
										"<td>" + dataTreatmentItem.deskripsi + nama_dokter_treatment + "</td>" +
										"<td align='center'>" + numeral(dataTreatmentItem.qty).format('0,0.00') +"</td>" +
										"<td align='right'>" + numeral(dataTreatmentItem.rupiah).format('0,0') +"</td>" +
									"</tr>";
						});
						/*if (response_treatment_item_parse.tb_drug_ingredients != null) {
							$.each(response_treatment_item_parse.tb_drug_ingredients, function (index, dataDrug) {
								tabel_buat_pdf +=
									"<tr>" +
										"<td style='padding-left: 5px;padding-right: 40px;'>" + moment(dataDrug.date).format('DD/MM/YYYY') + "</td>" +
										"<td>" + dataDrug.v_item_composition + "</td>" +
										"<td align='center'>" + numeral(dataDrug.n_qty).format('0,0.00') +"</td>" +
										"<td align='right'>" + numeral(dataDrug.n_amount_trx).format('0,0') +"</td>" +
									"</tr>";
							});
						}
						if (response_treatment_item_parse.tb_item != null) {
							$.each(response_treatment_item_parse.tb_item, function (index, dataItem) {
								tabel_buat_pdf +=
									"<tr>" +
										"<td style='padding-left: 5px;padding-right: 40px;'>" + moment(dataItem.date).format('DD/MM/YYYY') + "</td>" +
										"<td>" + dataItem.v_item_name + "</td>" +
										"<td align='center'>" + numeral(dataItem.n_qty).format('0,0.00') +"</td>" +
										"<td align='right'>" + numeral(dataItem.n_amount_trx).format('0,0') +"</td>" +
									"</tr>";
							});
						}
						if (response_treatment_item_parse.tb_treatment != null) {
							$.each(response_treatment_item_parse.tb_treatment, function (index, dataTreatment) {
								var nama_dokter_treatment = "";
								if (dataTreatment.n_doctor_id != null) {
									$.ajax({
										url: base_url+"RawatJalan/get_nama_dokter_treatment",
										type: "post",
										data: {
											n_doctor_id: dataTreatment.n_doctor_id
										},
										datatype: 'json',
										async: false,
										success: function(response_nama_dokter) {
											var response_nama_dokter_parse = JSON.parse(response_nama_dokter);
											if (response_nama_dokter_parse.nama_dokter_treatment != null) {
												nama_dokter_treatment = " - " + response_nama_dokter_parse.nama_dokter_treatment;
											} else {
												nama_dokter_treatment = "";
											}
										}
									});
								}
								tabel_buat_pdf +=
									"<tr>" +
										"<td style='padding-left: 5px;padding-right: 40px;'>" + moment(dataTreatment.date).format('DD/MM/YYYY') + "</td>" +
										"<td>" + dataTreatment.v_treatment_name + nama_dokter_treatment + "</td>" +
										"<td align='center'>" + numeral(dataTreatment.n_qty).format('0,0.00') +"</td>" +
										"<td align='right'>" + numeral(dataTreatment.n_amount_trx).format('0,0') +"</td>" +
									"</tr>";
							});
						}*/
						tabel_buat_pdf +=
							"<tr>" +
								"<td></td>" +
								"<td></td>" +
								"<td></td>" +
								"<td style='border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(dataExamination.n_total_amount).format('0,0') +"</td>" +
							"</tr>";
					}
				});
			});
			tabel_buat_pdf +=
					"<tr style='border-top: 1px solid black;border-collapse: collapse;'>" +
						"<td colspan='3' align='right'>Jumlah Biaya :</td>" +
						"<td style='padding-top:5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>Rp. " + numeral(response_parse.total_amount).format('0,0') +"</td>" +
					"</tr>" +
					"<tr>" +
						"<td colspan='4'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
		}
	});
	$('#parent_buat_pdf_rajal').html(tabel_buat_pdf);
	if (nosep != 'null') {
		$('#judul_buat_pdf_rajal').html('(Nomor SEP: '+nosep+')');
	} else {
		$('#judul_buat_pdf_rajal').html('');
	}
	$('#modal_buat_pdf_rajal').modal('show');
}

function modal_buat_pdf_lab_rajal(n_mr_id,tanggal_awal,tanggal_akhir,nosep,tglsep) {
	var tabel_buat_pdf_lab = "";
	var tabel_buat_pdf_lab_isi = "";
	var cek_rekap = "";
	$.ajax({
		url: base_url+"RawatJalan/cari_lab_buat_pdf",
		type: "post",
		data: {
			n_mr_id: n_mr_id,
			tanggal_awal: tanggal_awal,
			tanggal_akhir: tanggal_akhir
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_lab_hidden').val('');
			$('#norm_buat_pdf_lab_hidden').val('');
			$('#tglsep_buat_pdf_lab_hidden').val('');
			$('#nosep_buat_pdf_lab_hidden').val(nosep);
			$('#norm_buat_pdf_lab_hidden').val(n_mr_id);
			$('#tglsep_buat_pdf_lab_hidden').val(tglsep);
			$('#parent_buat_pdf_lab_rajal').empty();
			//console.log(response_parse);
			tabel_buat_pdf_lab +=
				"REKAP HASIL LABORAT<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					/*"<tr>" +
						"<td style='padding-right: 40px;'>Nama Penjamin</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
					"</tr>" +*/
					/*"<tr>" +
						"<td style='padding-right: 40px;'>No. Register</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
					"</tr>" +*/
					"<tr>" +
						"<td style='padding-right: 40px;' nowrap>No. Rekam Medik</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 40px;' nowrap>Nama Pasien</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 40px;' valign='top' nowrap>Alamat</td>" +
						"<td style='padding-right: 5px;' valign='top'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
					"</tr>" +
				"</table><br/>" +
				"<table style='width:100%;' id='tabel_rekap_hasil_lab'>";
				$.each(response_parse.list_vnoteno, function (index, dataVnoteno) {
					$.ajax({
						url: base_url+"RawatJalan/cari_lab_buat_pdf_rekap",
						type: "post",
						data: {
							vnoteno: dataVnoteno.v_note_no
						},
						datatype: 'json',
						async: false,
						success: function(response_rekap) {
							var response_rekap_parse = JSON.parse(response_rekap);
							//console.log(response_rekap_parse);
							if (response_rekap_parse != null) {
								tabel_buat_pdf_lab +=
									"<tr style='border-bottom: 1px solid black;border-top: 1px solid black;border-collapse: collapse;'>" +
										"<td colspan='4' style='padding: 5px;'>" + dataVnoteno.v_note_no + "</td>" +
									"</tr>" +
									"<tr>" +
										"<td>Nama Tes</td>" +
										"<td>Hasil Tes</td>" +
										"<td>Nilai Normal</td>" +
										"<td>Nama Unit Tes</td>" +
									"</tr>";
								if (response_rekap_parse != null) {
									$.each(response_rekap_parse, function (index, dataRekap) {
										if (dataRekap.test_name != "") {
											var nama_tes = dataRekap.test_name;
										} else {
											var nama_tes = '-';
										}
										if (dataRekap.hasil != "") {
											var hasil_tes = dataRekap.hasil;
										} else {
											var hasil_tes = '-';
										}
										if (dataRekap.nilai_normal != "") {
											var nilai_normal = dataRekap.nilai_normal;
										} else {
											var nilai_normal = '-';
										}
										if (dataRekap.test_units_name != "") {
											var nama_unit_tes = dataRekap.test_units_name;
										} else {
											var nama_unit_tes = '-';
										}
										tabel_buat_pdf_lab_isi +=
											"<tr>" +
												"<td>"+nama_tes+"</td>" +
												"<td>"+hasil_tes+"</td>" +
												"<td>"+nilai_normal+"</td>" +
												"<td>"+nama_unit_tes+"</td>" +
											"</tr>";
									});
									cek_rekap += response_rekap_parse;
								}
							}
						}
					});
				tabel_buat_pdf_lab += tabel_buat_pdf_lab_isi;
			});
			tabel_buat_pdf_lab +=
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td colspan='5'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			if (cek_rekap != "") {
				$('#parent_buat_pdf_lab_rajal').html(tabel_buat_pdf_lab);
				//rowspan_jika_value_sama('tabel_rekap_hasil_lab');
				if (nosep != 'null') {
					$('#judul_buat_pdf_lab_rajal').html('(Nomor SEP: '+nosep+')');
				} else {
					$('#judul_buat_pdf_lab_rajal').html('');
				}
				$('#modal_buat_pdf_lab_rajal').modal('show');
			} else {
				swal({   
					title: "Data hasil rekap laborat belum ada",
					//text: "Data hasil rekap laborat belum ada",
					type: "error",
				});
			}
		}
	});
}

function rowspan_jika_value_sama(id_tabel){
	var span = 1;
	var prevTD = "";
	var prevTDVal = "";
	$("#"+id_tabel+" tr td:first-child").each(function() { //for each first td in every tr
		var $this = $(this);
		if ($this.text() == prevTDVal) { // check value of previous td text
			span++;
			if (prevTD != "") {
				prevTD.attr("rowspan", span); // add attribute to previous td
				$this.remove(); // remove current td
			}
		} else {
			prevTD     = $this; // store current td 
			prevTDVal  = $this.text();
			span       = 1;
		}
	});
}

$('#button_buat_pdf_rajal').click(function(){
	var nosep_buat_pdf = $('#nosep_buat_pdf_hidden').val();
	var norm_buat_pdf = $('#norm_buat_pdf_hidden').val();
	var cek_buat_pdf = 1; //CETAK BY REG
	var tglsep_buat_pdf = "";
	if ($('#tglsep_buat_pdf_hidden').val() == "null" || $('#tglsep_buat_pdf_hidden').val() == null) {
		$.ajax({
			url: base_url+"RawatJalan/cari_tglsep_by_nosep",
			type: "post",
			data: {
				nosep: nosep_buat_pdf
			},
			dataType: 'json',
			async: false,
			success: function(response) {
				tglsep_buat_pdf = response;
			}
		});
	} else {
		tglsep_buat_pdf = $('#tglsep_buat_pdf_hidden').val();
	}
	buat_pdf_rajal(nosep_buat_pdf, tglsep_buat_pdf, norm_buat_pdf, cek_buat_pdf);
});
$('#button_buat_pdf_lab_rajal').click(function(){
	var nosep_buat_pdf_lab = $('#nosep_buat_pdf_lab_hidden').val();
	var norm_buat_pdf_lab = $('#norm_buat_pdf_lab_hidden').val();
	var cek_buat_pdf_lab = 2; //REKAP HASIL LAB
	var tglsep_buat_pdf_lab = "";
	if ($('#tglsep_buat_pdf_lab_hidden').val() == "null" || $('#tglsep_buat_pdf_lab_hidden').val() == null) {
		$.ajax({
			url: base_url+"RawatJalan/cari_tglsep_by_nosep",
			type: "post",
			data: {
				nosep: nosep_buat_pdf_lab
			},
			dataType: 'json',
			async: false,
			success: function(response) {
				tglsep_buat_pdf_lab = response;
			}
		});
	} else {
		tglsep_buat_pdf_lab = $('#tglsep_buat_pdf_lab_hidden').val();
	}
	buat_pdf_rajal(nosep_buat_pdf_lab, tglsep_buat_pdf_lab, norm_buat_pdf_lab, cek_buat_pdf_lab);
});
function buat_pdf_rajal(nosep, tglsep, norm, cek) {
	if (cek == 1) {
		var html_buat_pdf = $('#parent_buat_pdf_rajal').html();
	} else {
		var html_buat_pdf = $('#parent_buat_pdf_lab_rajal').html();
	}
	//console.log(html_buat_pdf);
	$.ajax({
		url: base_url+"RawatJalan/buat_pdf",
		type: "post",
		data: {
			nosep: nosep,
			tglsep: tglsep,
			norm: norm,
			cek: cek,
			html_buat_pdf: html_buat_pdf
		},
		datatype: 'json',
		success: function(response) {
			swal({   
				title: "Sukses!",
				text: "PDF berhasil dibuat",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}

function modal_hapus_pdf_rajal(namafile, nosep, tglsep) {
	swal({
		title: "Apakah anda yakin ingin menghapus file ini?",
		text: "File yang akan anda hapus adalah '"+namafile+".pdf'",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, hapus!'
	}, function(){
		$.ajax({
			url: base_url+"RawatJalan/hapus_pdf",
			type: "post",
			data: {
				namafile: namafile,
				nosep: nosep,
				tglsep: tglsep
			},
			datatype: 'json',
			success: function(data) {
				if (data == "kosong") {
					$('#modal_lihat_file_rajal').modal('hide');
				} else {
					nama_file_rajal(nosep, tglsep);
				}
				swal({   
					title: "Sukses!",
					text: "PDF berhasil dihapus",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			}
		});
	});
}
//PDF