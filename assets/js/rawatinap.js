$(document).ajaxStart(function(){
	$('#loading').show();
});

$(document).ajaxComplete(function(){
	$('#loading').hide();
});

$(".bootstrapMaterialDatePicker").bootstrapMaterialDatePicker({
	weekStart:0,
	time: false,
	format: 'DD/MM/YYYY'
});

$('#button_cari_rawat_inap').click(function(){
	var ward_name = $("#ward_name").val();
	cari_rawat_inap(ward_name);
});


function cari_rawat_inap(ward_name) {
	if (!ward_name) {
		swal({   
			title: "Gagal!",
			text: "Data anda gagal dicari, silahkan mengisi data dengan benar",
			timer: 1500,
			showConfirmButton: false,
			type: "error",
		});
	} else {
		$.ajax({
			url: base_url+"RawatInap/cari_rawat_inap",
			type: "post",
			data: {
				ward_name: ward_name
			},
			datatype: 'json',
			success: function(response) {
				var response_parse = JSON.parse(response);
				$('#parent_tabel_rawat_inap').empty();
				$('#pemberitahuan_input').hide();
				var tabel_rawat_inap_awal =
						// "<div class='header' align='center' style='margin-top:-20px;'>\
						// 	<span><h2>RUANG RAWAT INAP "+response_parse.nama_ward+"</h2></span>\
						// </div>" +
						"<input type='hidden' id='ward_name_hidden' value='"+ward_name+"'>"+
						"<table class='table table-bordered table-striped table-hover' id='tabel_rawat_inap'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Tanggal Masuk</th>"+
								"<th>LOS</th>"+
								"<th>Nomor Bed</th>"+
								"<th>Nomor MR</th>"+
								"<th>Nama Pasien</th>"+
								"<th>Nomor SEP</th>"+
								"<th>Koding Hak</th>"+
								"<th>Koding Ditempati</th>"+
								"<th>Real Cost</th>"+
								"<th style='text-align:center;'>Buat PDF</th>"+
								"<th>Tanggal Koding</th>"+
								"<th>Status Koding</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
			
				var nomor_tabel_rawat_inap = 1;
				var tabel_rawat_inap_isi = "";
				$.each(response_parse.rawat_inap, function (index, dataRawatInap) {
						var n_Koding_hak;
						var n_codding_ditempati;
						var tanggal_koding;
						$.ajax({
							url: base_url+"RawatInap/get_last_koding",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_last_koding) {
								var response_last_koding_parse = JSON.parse(response_last_koding);
								if (response_last_koding_parse != null) {
									//KODING HAK
									if (response_last_koding_parse.n_codding_hak != null) {
										n_Koding_hak = numeral(response_last_koding_parse.n_codding_hak).format('0,0');
									} else {
										n_Koding_hak = '-';
									}
									//KODING HAK
									//KODING DITEMPATI
									if (response_last_koding_parse.n_codding_ditempati != null) {
										n_codding_ditempati = numeral(response_last_koding_parse.n_codding_ditempati).format('0,0');
									} else {
										n_codding_ditempati = '-';
									}
									//KODING DITEMPATI
									//TANGGAL KODING
									if (response_last_koding_parse.tanggal_koding != null) {
										tanggal_koding = moment(response_last_koding_parse.tanggal_koding).format('DD/MM/YYYY');
									} else {
										tanggal_koding = '-';
									}
									//TANGGAL KODING
								} else {
									n_Koding_hak = '-';
									n_codding_ditempati = '-';
									tanggal_koding = '-';
								}
							}
						});
					//LOS
					var date1 = moment(dataRawatInap.date);
					var date2 = moment();
					var diff = date2.diff(date1, 'days');
					var mrId=(dataRawatInap.v_mr_code).replace(/\-/g,"");
					var reg_date=moment(dataRawatInap.date).format('YYYY-MM-DD');
					//LOS
					tabel_rawat_inap_isi +=
								"<tr>"+
									"<td>" + nomor_tabel_rawat_inap + "</td>" +
									"<td>" + moment(dataRawatInap.date).format('DD/MM/YYYY') + "</td>" +
									"<td nowrap>" + diff + " hari</td>" +
									"<td>" + dataRawatInap.v_bed_desc + "</td>" +
									"<td nowrap>" + mrId + "</td>" +
									"<td>" + dataRawatInap.v_patient_name + "</td>";
					if (dataRawatInap.v_nosepbpjs != null) {
						var no_sep = '"'+dataRawatInap.v_nosepbpjs+'"';
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						tabel_rawat_inap_isi += "<td><button type='button' class='btn bg-green btn-sm m-l-15 waves-effect' title='Edit SEP' onclick='modal_edit_sep("+dataRawatInap.n_reg_id+","+no_sep+","+nama_pasien+")'>" + dataRawatInap.v_nosepbpjs + "</button></td>";
					} else {
						var sepDefault=getSepPasien(mrId,reg_date);
						var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
						if(sepDefault.length !== 0){
							var no_sep=sepDefault[0];
							var sepStr= '"'+no_sep+'"';
							var icd_code=sepDefault[1].split(' - ')[0];
							var kelasRawat=sepDefault[2];
							var nilai_default='-';
							var nilai_hak_default='-';
							//console.log('Diagnosa: '+icd_code);
							//console.log('kelas rawat: '+kelasRawat.includes('3'));
							var data_sep=get_detail_sep(no_sep);
							var nilai_ina=get_nilai_koding_default(icd_code);
							
							if(nilai_ina){
								//console.log(nilai_ina);
								if(data_sep[1].includes('3')){
									nilai_default=nilai_ina[2];
								}
								else if(data_sep[1].includes('2')){
									nilai_default=nilai_ina[1];
								}else if(data_sep[1].includes('1')){
									nilai_default=nilai_ina[1];
								}else{
									nilai_default='0';
								}

								if(data_sep[2].includes('3')){
									nilai_hak_default=nilai_ina[2];
								}
								else if(data_sep[2].includes('2')){
									nilai_hak_default=nilai_ina[1];
								}else if(data_sep[2].includes('1')){
									nilai_hak_default=nilai_ina[1];
								}else{
									nilai_hak_default='0';
								}
							}
							tabel_rawat_inap_isi += "<td><button type='button' class='btn bg-yellow btn-sm m-l-15 waves-effect' title='Edit SEP' onclick='modal_input_sep_by_default("+dataRawatInap.n_reg_id+","+sepStr+","+nama_pasien+")'><font color='red'>" +no_sep+ "</font></button></td>";
							n_Koding_hak = numeral(nilai_hak_default).format('0,0');

							n_codding_ditempati = numeral(nilai_default).format('0,0');
						}else{
							var nama_pasien = '"'+dataRawatInap.v_patient_name+'"';
							tabel_rawat_inap_isi += "<td align='center'><button type='button' class='btn bg-red  btn-circle waves-effect waves-circle waves-float' title='Input SEP' onclick='modal_input_sep("+dataRawatInap.n_reg_id+","+nama_pasien+")'><i class='material-icons'>add_circle</i></button></td>";
						}
					}
					tabel_rawat_inap_isi +=
									"<td>" + n_Koding_hak + "</td>" +
									"<td>" + n_codding_ditempati + "</td>" +
									"<td nowrap>" + numeral(dataRawatInap.sum_real_cost).format('0,0') + "</td>";

					tabel_rawat_inap_isi += "<td nowrap>"; //BUKA BUAT PDF
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' onclick='modal_buat_pdf("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>group_work</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Grup Ina' disabled><i class='material-icons'>group_work</i></button>";
					}
					if (dataRawatInap.sum_real_cost != null) {
						if (dataRawatInap.v_nosepbpjs != null) {
							/*var waktu_awal = "";
							var waktu_akhir = "";
							$.ajax({
								url: base_url+"RawatInap/get_waktu_awal_akhir",
								type: "post",
								data: {
									n_reg_id: dataRawatInap.n_reg_id
								},
								datatype: 'json',
								async: false,
								success: function(response_waktu_awal_akhir) {
									var response_waktu_awal_akhir_parse = JSON.parse(response_waktu_awal_akhir);
									waktu_awal = '"'+response_waktu_awal_akhir_parse.waktu_awal+'"';
									waktu_akhir = '"'+response_waktu_awal_akhir_parse.waktu_akhir+'"';
								}
							});*/
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' onclick='modal_buat_detail_pdf("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+","+dataRawatInap.sum_real_cost+")'><i class='material-icons'>note</i></button>";
						} else {
							tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Cetak By Reg' disabled><i class='material-icons'>note</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						$.ajax({
							url: base_url+"RawatInap/get_vnoteno_by_nregid",
							type: "post",
							data: {
								n_reg_id: dataRawatInap.n_reg_id
							},
							datatype: 'json',
							async: false,
							success: function(response_vnoteno) {
								var response_vnoteno_parse = JSON.parse(response_vnoteno);
								//console.log(response_vnoteno_parse);
								if (response_vnoteno_parse != null) {
									var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
									var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' onclick='modal_buat_pdf_lab("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>hourglass_empty</i></button>";
								} else {
									tabel_rawat_inap_isi +=
										"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
								}
							}
						});
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Rekap Laborat' disabled><i class='material-icons'>hourglass_empty</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						if (dataRawatInap.n_codding_status == 2) {
							var tglsep_lempar = '"'+dataRawatInap.tglsep+'"';
							var nosep_lempar = '"'+dataRawatInap.v_nosepbpjs+'"';
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' onclick='modal_buat_pdf_iuran("+dataRawatInap.n_reg_id+","+nosep_lempar+","+tglsep_lempar+")'><i class='material-icons'>attach_money</i></button>";
						} else {
							tabel_rawat_inap_isi +=
								"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
						}
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Buat Detail Iuran' disabled><i class='material-icons'>attach_money</i></button>";
					}
					tabel_rawat_inap_isi += "</td>"; //TUTUP BUAT PDF

					tabel_rawat_inap_isi +=
									"<td>" + tanggal_koding + "</td>" +
									"<td>" + dataRawatInap.status + "</td>" +
									"<td nowrap>";

					if (dataRawatInap.v_nosepbpjs != null) {
						var v_nosepbpjs = '"'+dataRawatInap.v_nosepbpjs+'"';
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' onclick='minta_koding("+dataRawatInap.n_reg_id+","+v_nosepbpjs+")'><i class='material-icons'>code</i></button>";
					} else { 
						tabel_rawat_inap_isi +=				
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Minta Koding' disabled><i class='material-icons'>code</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						var tglsep = "";
						$.ajax({
							url: base_url+"RawatInap/get_tanggal_sep",
							type: "post",
							data: {
								v_nosepbpjs: dataRawatInap.v_nosepbpjs
							},
							datatype: 'json',
							async: false,
							success: function(response_tanggal_sep) {
								var response_tanggal_sep_parse = JSON.parse(response_tanggal_sep);
								tglsep = '"'+response_tanggal_sep_parse.tglsep+'"';
							}
						});
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' onclick='modal_upload_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>file_upload</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Upload PDF' disabled><i class='material-icons'>file_upload</i></button>";
					}
					if (dataRawatInap.v_nosepbpjs != null) {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' onclick='modal_lihat_file("+v_nosepbpjs+","+tglsep+")'><i class='material-icons'>pageview</i></button>";
					} else {
						tabel_rawat_inap_isi +=
									"<button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Lihat PDF' disabled><i class='material-icons'>pageview</i></button>";
					}
					tabel_rawat_inap_isi +=	
									"</td>" +
								"</tr>";
					nomor_tabel_rawat_inap++;
				});
				var tabel_rawat_inap_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_rawat_inap = tabel_rawat_inap_awal+tabel_rawat_inap_isi+tabel_rawat_inap_akhir;
				$('#parent_tabel_rawat_inap').html(hasil_tabel_rawat_inap);
				$('#tabel_rawat_inap').DataTable();
			}
		});
	}
	$('#ward_name').val($('#ward_name option:first').val()).trigger('change');
	$('.selectpicker').selectpicker('refresh');
}



function modal_input_sep(n_reg_id, nama_pasien) {
	console.log(n_reg_id);
	$("#regid").val('');
	//$("#nama_pasien").val('');
	$("#nama_pasien_konfirmasi").val('');
	$("#regid").val(n_reg_id);
	//$("#nama_pasien").val(nama_pasien);
	$("#nama_pasien_konfirmasi").val(nama_pasien);

	$("#nomor_rs").val('1124R005');
	$("#nomor_sep").val('');
	$("#konfirmasi_sep").prop("checked", false);
	$('#parent_button_simpan_sep').hide();
    $("#parent_keterangan_no_sep").hide();
	$('#modal_input_sep').modal('show');
}

function modal_input_sep_by_default(n_reg_id, no_sep, nama_pasien){
	console.log(n_reg_id);
	$("#regid").val('');
	//$("#nama_pasien").val('');
	$("#nama_pasien_konfirmasi").val('');
	$("#regid").val(n_reg_id);
	//$("#nama_pasien").val(nama_pasien);
	$("#nama_pasien_konfirmasi").val(nama_pasien);

	$("#nomor_rs").val('1124R005');
	$("#nomor_sep").val(no_sep.substring(8, 19));
	$("#konfirmasi_sep").prop("checked", false);
	$('#parent_button_simpan_sep').hide();
    $("#parent_keterangan_no_sep").hide();
	$('#modal_input_sep').modal('show');
	cari_sep(1);
}

function modal_edit_sep(n_reg_id, no_sep, nama_pasien) {
	console.log(n_reg_id);
	console.log(no_sep);
	$("#nomor_sep_belum_edit").val('');
	$("#regid_edit").val('');
	//$("#nama_pasien").val('');
	$("#nama_pasien_konfirmasi_edit").val('');
	$("#nomor_sep_belum_edit").val(no_sep);
	$("#regid_edit").val(n_reg_id);
	//$("#nama_pasien").val(nama_pasien);
	$("#nama_pasien_konfirmasi_edit").val(nama_pasien);

	$("#nomor_rs_edit").val('1124R005');
	$("#nomor_sep_edit").val('');
	$("#konfirmasi_sep_edit").prop("checked", false);
	$('#parent_button_edit_sep').hide();
    $("#parent_keterangan_edit_no_sep").hide();
	$('#modal_edit_sep').modal('show');
}

$("#parent_keterangan_no_sep").hide();
$("#parent_button_simpan_sep").hide();
$("#parent_keterangan_edit_no_sep").hide();
$("#parent_button_edit_sep").hide();
function cari_sep(cek) {
	if (cek == 1) { //INPUT SEP
		var nomor_sep = $("#nomor_sep").val();
	} else  if (cek == 2) { //EDIT SEP
		var nomor_sep = $("#nomor_sep_edit").val();
	}
	$.ajax({
		url: base_url+"RawatInap/cari_sep",
		type: "post",
		data: {
			nomor_sep: nomor_sep
		},
		datatype: 'json',
		success: function(response) {
			response_parse = JSON.parse(response);
			console.log(response_parse);
			
			/*var nama_pasien = $("#nama_pasien").val();
			console.log('Pembanding : '+nama_pasien);
			console.log('SEP : '+response_parse.response.peserta.nama);
			if (response_parse.response.peserta.nama == nama_pasien) {
				swal({   
					title: "Sukses!",
					text: "Data SEP anda valid",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});*/
				if (cek == 1) {
					//SIMPAN SEP
					$('#nosep').val('');
					$('#tglsep').val('');
					$('#nokartu').val('');
					$('#namapeserta').val('');
					$('#peserta').val('');
					$('#tgllahir').val('');
					$('#jnskelamin').val('');
					$('#politujuan').val('');
					$('#jnsrawat').val('');
					$('#klsrawat').val('');
					$('#diagnosaawal').val('');
					$('#catatan').val('');
					$('#idkelasstandart').val('');
					$('#no_rujukan').val('');
					$('#nosep').val(response_parse.response.noSep);
					$('#tglsep').val(response_parse.response.tglSep);
					$('#nokartu').val(response_parse.response.peserta.noKartu);
					$('#namapeserta').val(response_parse.response.peserta.nama);
					$('#peserta').val(response_parse.response.peserta.jnsPeserta);
					$('#tgllahir').val(response_parse.response.peserta.tglLahir);
					$('#jnskelamin').val(response_parse.response.peserta.kelamin);
					$('#politujuan').val(response_parse.response.poli);
					$('#jnsrawat').val(response_parse.response.jnsPelayanan);
					$('#klsrawat').val(response_parse.response.kelasRawat);
					$('#diagnosaawal').val(response_parse.response.diagnosa);
					$('#catatan').val(response_parse.response.catatan);
					//$('#regid').val($("#regid").val());
					$('#idkelasstandart').val(response_parse.response.peserta.hakKelas);
					$('#no_rujukan').val(response_parse.response.noRujukan);
					//SIMPAN SEP

					//TAMPIL DI MODAL
					$('#nama_peserta').val('');
					$('#kelas_rawat').val('');
					$('#hak_kelas').val('');
					$('#diagnosa').val('');
					$('#catatan').val('');
					if (response_parse.response.catatan != "") {
						var catatan = response_parse.response.catatan;
					} else {
						var catatan = "-";
					}
					//console.log(response_parse);
					$('#nama_peserta').val(response_parse.response.peserta.nama);
					$('#kelas_rawat').val(response_parse.response.kelasRawat);
					$('#hak_kelas').val(response_parse.response.peserta.hakKelas);
					$('#diagnosa').val(response_parse.response.diagnosa);
					$('#catatan').val(catatan);
					//TAMPIL DI MODAL
					$("#parent_keterangan_no_sep").show();
				} else if (cek == 2) {
					//EDIT SEP
					$('#nosep_edit').val('');
					$('#tglsep_edit').val('');
					$('#nokartu_edit').val('');
					$('#namapeserta_edit').val('');
					$('#peserta_edit').val('');
					$('#tgllahir_edit').val('');
					$('#jnskelamin_edit').val('');
					$('#politujuan_edit').val('');
					$('#jnsrawat_edit').val('');
					$('#klsrawat_edit').val('');
					$('#diagnosaawal_edit').val('');
					$('#catatan_edit').val('');
					$('#idkelasstandart_edit').val('');
					$('#no_rujukan_edit').val('');
					$('#nosep_edit').val(response_parse.response.noSep);
					$('#tglsep_edit').val(response_parse.response.tglSep);
					$('#nokartu_edit').val(response_parse.response.peserta.noKartu);
					$('#namapeserta_edit').val(response_parse.response.peserta.nama);
					$('#peserta_edit').val(response_parse.response.peserta.jnsPeserta);
					$('#tgllahir_edit').val(response_parse.response.peserta.tglLahir);
					$('#jnskelamin_edit').val(response_parse.response.peserta.kelamin);
					$('#politujuan_edit').val(response_parse.response.poli);
					$('#jnsrawat_edit').val(response_parse.response.jnsPelayanan);
					$('#klsrawat_edit').val(response_parse.response.kelasRawat);
					$('#diagnosaawal_edit').val(response_parse.response.diagnosa);
					$('#catatan_edit').val(response_parse.response.catatan);
					//$('#regid_edit').val($("#regid_edit").val());
					$('#idkelasstandart_edit').val(response_parse.response.peserta.hakKelas);
					$('#no_rujukan_edit').val(response_parse.response.noRujukan);
					//EDIT SEP

					//TAMPIL DI MODAL
					$('#nama_peserta_edit').val('');
					$('#kelas_rawat_edit').val('');
					$('#hak_kelas_edit').val('');
					$('#diagnosa_edit').val('');
					$('#catatan_edit').val('');
					if (response_parse.response.catatan != "") {
						var catatan = response_parse.response.catatan;
					} else {
						var catatan = "-";
					}
					//console.log(response_parse);
					$('#nama_peserta_edit').val(response_parse.response.peserta.nama);
					$('#kelas_rawat_edit').val(response_parse.response.kelasRawat);
					$('#hak_kelas_edit').val(response_parse.response.peserta.hakKelas);
					$('#diagnosa_edit').val(response_parse.response.diagnosa);
					$('#catatan_edit').val(catatan);
					//TAMPIL DI MODAL
					$("#parent_keterangan_edit_no_sep").show();
				}
			/*} else {
				swal({   
					title: "Gagal!",
					text: "Data SEP anda tidak valid, silahkan ulangi dan sesuaikan dengan data pasien",
					type: "error",
				});
			}*/
		}
	});
}

$('#nomor_sep').keyup(function () {
    if (this.value.length == 11) {
        cari_sep(1);
    } else {
    	$("#parent_keterangan_no_sep").hide();
    }
});

$('#nomor_sep_edit').keyup(function () {
    if (this.value.length == 11) {
        cari_sep(2);
    } else {
    	$("#parent_keterangan_edit_no_sep").hide();
    }
});

$("#konfirmasi_sep").change(function() {
    if(this.checked) {
        $('#parent_button_simpan_sep').show();
    } else {
    	$('#parent_button_simpan_sep').hide();
    }
});

$("#konfirmasi_sep_edit").change(function() {
    if(this.checked) {
        $('#parent_button_edit_sep').show();
    } else {
    	$('#parent_button_edit_sep').hide();
    }
});

$('#button_simpan_sep').click(function(){
	$('#modal_input_sep').modal('hide');
	simpan_sep();
	var ward_name = $("#ward_name_hidden").val();
	setTimeout(function(){ cari_rawat_inap(ward_name); }, 500);
});
function simpan_sep() {
	$.ajax({
		url: base_url+"RawatInap/simpan_sep",
		type: "post",
		data: {
			nosep: $("#nosep").val(),
			tglsep: $("#tglsep").val(),
			nokartu: $("#nokartu").val(),
			namapeserta: $("#namapeserta").val(),
			peserta: $("#peserta").val(),
			tgllahir: $("#tgllahir").val(),
			jnskelamin: $("#jnskelamin").val(),
			//cob: $("#cob").val(),
			politujuan: $("#politujuan").val(),
			jnsrawat: $("#jnsrawat").val(),
			klsrawat: $("#klsrawat").val(),
			diagnosaawal: $("#diagnosaawal").val(),
			catatan: $("#catatan").val(),
			regid: $("#regid").val(),
			//idtable: $("#idtable").val(),
			//tglrujukan: $("#tglrujukan").val(),
			//kodeasalfaskes: $("#kodeasalfaskes").val(),
			//asalfaskestk1: $("#asalfaskestk1").val(),
			//kddiagawal: $("#kddiagawal").val(),
			idkelasstandart: $("#idkelasstandart").val(),
			//status: $("#status").val(),
			//nikpeserta: $("#nikpeserta").val(),
			//biaya_tagihan: $("#biaya_tagihan").val(),
			//tgl_pulang: $("#tgl_pulang").val(),
			//tgl_batal: $("#tgl_batal").val(),
			//user_batal: $("#user_batal").val(),
			//alasanbatal: $("#alasanbatal").val(),
			//iscob: $("#iscob").val(),
			//telppeserta: $("#telppeserta").val(),
			no_rujukan: $("#no_rujukan").val(),
			//v_dpjp_kode: $("#v_dpjp_kode").val(),
			//v_no_skdp: $("#v_no_skdp").val(),
			//v_nama_ppk_perujuk: $("#v_nama_ppk_perujuk").val(),
			//dinsos: $("#dinsos").val(),
			//prolanis_prb: $("#prolanis_prb").val(),
			//no_sktm: $("#no_sktm").val(),
			//n_Koding_status: $("#n_Koding_status").val()
		},
		success: function(response) {
			var response_parse = JSON.parse(response);
			if (response_parse == 'sukses') {
				swal({   
					title: "Sukses!",
					text: "Data SEP anda berhasil disimpan",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			} else {
				swal({   
					title: "Gagal!",
					text: "Input SEP gagal, nomor SEP sudah ada",
					type: "error",
				});
			}
		}
	});
}
$('#button_edit_sep').click(function(){
	$('#modal_edit_sep').modal('hide');
	edit_sep();
	var ward_name = $("#ward_name_hidden").val();
	setTimeout(function(){ cari_rawat_inap(ward_name); }, 500);
});
function edit_sep() {
	$.ajax({
		url: base_url+"RawatInap/edit_sep",
		type: "post",
		data: {
			nosep_lama: $("#nomor_sep_belum_edit").val(),
			nosep: $("#nosep_edit").val(),
			tglsep: $("#tglsep_edit").val(),
			nokartu: $("#nokartu_edit").val(),
			namapeserta: $("#namapeserta_edit").val(),
			peserta: $("#peserta_edit").val(),
			tgllahir: $("#tgllahir_edit").val(),
			jnskelamin: $("#jnskelamin_edit").val(),
			politujuan: $("#politujuan_edit").val(),
			jnsrawat: $("#jnsrawat_edit").val(),
			klsrawat: $("#klsrawat_edit").val(),
			diagnosaawal: $("#diagnosaawal_edit").val(),
			catatan: $("#catatan_edit").val(),
			regid: $("#regid_edit").val(),
			idkelasstandart: $("#idkelasstandart_edit").val(),
			no_rujukan: $("#no_rujukan_edit").val()
		},
		success: function(response) {
			swal({   
				title: "Sukses!",
				text: "Data SEP anda berhasil diedit",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}

//CASEMIX
function minta_koding(n_reg_id, v_nosepbpjs) {
	swal({
		title: "Apakah anda yakin ingin minta koding data ini?",
		text: "Nomor SEP data ini adalah "+v_nosepbpjs,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: 'orange',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Minta Koding!'
	}, function(){
		$.ajax({
			url: base_url+"CasemixRawatInap/update_codding_status",
			type: "post",
			data: {
				n_reg_id: n_reg_id
			},
			datatype: 'json',
			success: function(response) {
				swal({
					title: "Sukses!",
					text: "Data anda berhasil dikirmkan ke Casemix",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			}
		});
	});
}

$.ajax({
	url: base_url+"CasemixRawatInap/cari_casemix",
	type: "post",
	datatype: 'json',
	success: function(response) {
		var response_parse = JSON.parse(response);

		$('#parent_tabel_casemix').empty();
		var tabel_casemix_awal =
				"<table class='table table-bordered' id='tabel_casemix'>"+
					"<thead>"+
						"<th>#</th>"+
						"<th>Nomor MR</th>"+
						"<th>Nomor SEP</th>"+
						"<th>Ruang</th>"+
						"<th>Nomor Bed</th>"+
					"</thead>"+
					"<tbody id='body_casemix'>";
		
		var nomor_tabel_casemix = 1;
		var tabel_casemix_isi = "";
		$.each(response_parse, function (index, dataCasemix) {
			tabel_casemix_isi +=
						"<tr class='row_casemix' casemixPilih='"+dataCasemix.n_reg_id+","+dataCasemix.v_nosepbpjs+"'>"+
							"<td>" + nomor_tabel_casemix + "</td>" +
							"<td>" + dataCasemix.v_mr_code + "</td>" +
							"<td>" + dataCasemix.v_nosepbpjs + "</td>" +
							"<td>" + dataCasemix.v_ward_name + "</td>" +
							"<td>" + dataCasemix.v_bed_desc + "</td>" +
						"</tr>";
			nomor_tabel_casemix++;
		});
		var tabel_casemix_akhir =
					"</tbody>"+
				"</table>";
		var hasil_tabel_casemix = tabel_casemix_awal+tabel_casemix_isi+tabel_casemix_akhir;
		$('#parent_tabel_casemix').html(hasil_tabel_casemix);
		$('#tabel_casemix').DataTable();
	}
});

function refresh_casemix() {
	$.ajax({
		url: base_url+"CasemixRawatInap/cari_casemix",
		type: "post",
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);

			$('#parent_tabel_casemix').empty();
			var tabel_casemix_awal =
					"<table class='table table-bordered' id='tabel_casemix'>"+
						"<thead>"+
							"<th>#</th>"+
							"<th>Nomor MR</th>"+
							"<th>Nomor SEP</th>"+
							"<th>Ruang</th>"+
							"<th>Nomor Bed</th>"+
						"</thead>"+
						"<tbody id='body_casemix'>";
			
			var nomor_tabel_casemix = 1;
			var tabel_casemix_isi = "";
			$.each(response_parse, function (index, dataCasemix) {
				tabel_casemix_isi +=
							"<tr class='row_casemix' casemixPilih='"+dataCasemix.n_reg_id+","+dataCasemix.v_nosepbpjs+"'>"+
								"<td>" + nomor_tabel_casemix + "</td>" +
								"<td>" + dataCasemix.v_mr_code + "</td>" +
								"<td>" + dataCasemix.v_nosepbpjs + "</td>" +
								"<td>" + dataCasemix.v_ward_name + "</td>" +
								"<td>" + dataCasemix.v_bed_desc + "</td>" +
							"</tr>";
				nomor_tabel_casemix++;
			});
			var tabel_casemix_akhir =
						"</tbody>"+
					"</table>";
			var hasil_tabel_casemix = tabel_casemix_awal+tabel_casemix_isi+tabel_casemix_akhir;
			$('#parent_tabel_casemix').html(hasil_tabel_casemix);
			$('#tabel_casemix').DataTable();
		}
	});
}

$(document).on("click", ".row_casemix", function () {
    var regid_dan_nosep = $(this).attr('casemixPilih');
    var regid_dan_nosep_array = regid_dan_nosep.split(',');
    var regid = regid_dan_nosep_array[0];
    var nosep = regid_dan_nosep_array[1];
    console.log(regid);
    console.log(nosep);
    cari_detail_casemix(regid, nosep);

    $(this).addClass('pilih_row').siblings().removeClass('pilih_row');

    $('#koding_hak_kelas').val('');
	$('#koding_ditempati').val('');
	$('#iuran_hidden').val('');
	$('#iuran').val('Rp. 0');
	$('#selisih').val('Rp. 0');
});

$('#parent_detail_tabel').hide();
$('#parent_tabel_history_koding_ina').hide();
$('#parent_input_koding_ina').hide();
$('#parent_button_modal_ubah_kelas').hide();

function cari_detail_casemix(regid, nosep) {
	$.ajax({
		url: base_url+"CasemixRawatInap/cari_sep_by_nosep",
		type: "post",
		data: {
			regid: regid,
			nosep: nosep
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			$('#pemberitahuan_input_keterangan').hide();
			$('#parent_detail_tabel').show();
			$('#parent_input_koding_ina').show();

			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			// menampilkan opsi untuk penyesuian kelas rawat
			if (
				(response_parse.sep.id_hak_kelas == 6 &&
				(response_parse.sep.id_kelas_ditempati == 6 || response_parse.sep.id_kelas_ditempati == 11)) ||
				(response_parse.sep.id_hak_kelas == 7 &&
				(response_parse.sep.id_kelas_ditempati == 7 || response_parse.sep.id_kelas_ditempati == 6)) ||
				(response_parse.sep.id_hak_kelas == 8 &&
				(response_parse.sep.id_kelas_ditempati == 8 || response_parse.sep.id_kelas_ditempati == 7))
			) {
				$('#parent_button_modal_ubah_kelas').hide();
			} else {
				$('#parent_button_modal_ubah_kelas').show();
			}
			
			//JENIS KELAMIN
			if (response_parse.sep.jnskelamin == 'P') {
				var jnskelamin = 'Perempuan';
			} else {
				var jnskelamin = 'Laki-laki';
			}
			
			$('#nosep').val('');
			$('#tglsep').val('');
			$('#nokartu').val('');
			$('#namapeserta').val('');
			$('#peserta').val('');
			$('#tgllahir').val('');
			$('#jnskelamin').val('');
			$('#politujuan').val('');
			$('#jnsrawat').val('');
			$('#klsrawat').val('');
			$('#diagnosaawal').val('');
			$('#catatan').val('');
			$('#idkelasstandart').val('');
			$('#no_rujukan').val('');
			$('#real_cost').val('');
			$('#n_reg_id_hidden').val('');
			//TAMBAHAN
			$('#id_hak_kelas_hidden').val('');
			$('#id_kelas_ditempati_hidden').val('');
			//TAMBAHAN
			$('#nosep').val(response_parse.sep.nosep);
			$('#tglsep').val(moment(response_parse.sep.tglsep).format('MM/DD/YYYY'));
			$('#nokartu').val(response_parse.sep.nokartu);
			$('#namapeserta').val(response_parse.sep.namapeserta);
			$('#peserta').val(response_parse.sep.peserta);
			$('#tgllahir').val(moment(response_parse.sep.tgllahir).format('MM/DD/YYYY'));
			$('#jnskelamin').val(jnskelamin);
			$('#politujuan').val(response_parse.sep.politujuan);
			$('#jnsrawat').val(response_parse.sep.jnsrawat);

			if (response_parse.sep.hasil_ubah_kelas_ditempati != null) {
				$('#klsrawat').val(response_parse.sep.hasil_ubah_kelas_ditempati);
			} else {
				$('#klsrawat').val(response_parse.sep.desc_kelas_ditempati);
			}

			$('#diagnosaawal').val(response_parse.sep.diagnosaawal);
			$('#catatan').val(response_parse.sep.catatan);
			$('#idkelasstandart').val(response_parse.sep.desc_hak_kelas);
			$('#no_rujukan').val(response_parse.sep.no_rujukan);
			$('#real_cost').val('Rp. '+numeral(response_parse.real_cost.sum_real_cost).format('0,0'));
			$('#iuran').val('Rp. 0');
			$('#selisih').val('Rp. 0');
			$('#n_reg_id_hidden').val(regid);
			$('#real_cost_hidden').val(response_parse.real_cost.sum_real_cost);
			$('#hak_kelas_hidden').val(response_parse.sep.id_hak_kelas);

			if (response_parse.sep.hasil_ubah_kelas_ditempati != null) {
				$('#kelas_rawat_hidden').val(response_parse.sep.hasil_ubah_kelas_ditempati);
			} else {
				$('#kelas_rawat_hidden').val(response_parse.sep.desc_kelas_ditempati);
			}
			//TAMBAHAN
			$('#id_hak_kelas_hidden').val(response_parse.sep.id_hak_kelas);
			$('#id_kelas_ditempati_hidden').val(response_parse.sep.id_kelas_ditempati);
			$('#desc_hak_kelas_hidden').val(response_parse.sep.desc_hak_kelas);
			if (response_parse.sep.hasil_ubah_kelas_ditempati != null) {
				$('#desc_kelas_ditempati_hidden').val(response_parse.sep.hasil_ubah_kelas_ditempati);
			} else {
				$('#desc_kelas_ditempati_hidden').val(response_parse.sep.desc_kelas_ditempati);
			}
			//TAMBAHAN
		}
	});
	$.ajax({
		url: base_url+"CasemixRawatInap/cari_history_koding_ina_by_regid",
		type: "post",
		data: {
			regid: regid
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			if (response_parse != null) {
				$('#parent_tabel_history_koding_ina').show();
			} else {
				$('#parent_tabel_history_koding_ina').hide();
			}
			$('#parent_tabel_history_koding_ina').empty();

			var tabel_history_koding_ina_awal =
				"<div class='header' style='margin-top:-30px;'><h2>Tabel History Koding</h2></div>"+
				"<table class='table table-bordered' id='tabel_history_koding_ina'>"+
					"<thead>"+
						"<th>#</th>"+
						"<th>Tanggal Koding</th>"+
						"<th>User Koding</th>"+
						"<th>Koding Hak</th>"+
						"<th>Koding Ditempati</th>"+
						"<th>Iuran</th>"+
						"<th>Real Cost</th>"+
					"</thead>"+
					"<tbody>";
		
			var nomor_tabel_history_koding_ina = 1;
			var tabel_history_koding_ina_isi = "";
			$.each(response_parse, function (index, dataKodingIna) {
				tabel_history_koding_ina_isi +=
							"<tr>"+
								"<td>" + nomor_tabel_history_koding_ina + "</td>" +
								"<td>" + moment(dataKodingIna.d_whn_create).format('DD/MM/YYYY') + "</td>" +
								"<td>" + dataKodingIna.v_staff_name + "</td>" +
								"<td>" + numeral(dataKodingIna.n_codding_hak).format('0,0') + "</td>" +
								"<td>" + numeral(dataKodingIna.n_codding_ditempati).format('0,0') + "</td>" +
								"<td>" + numeral(dataKodingIna.n_iur_max).format('0,0') + "</td>" +
								"<td>" + numeral(dataKodingIna.n_real_cost).format('0,0') + "</td>" +
							"</tr>";
				nomor_tabel_history_koding_ina++;
			});
			var tabel_history_koding_ina_akhir =
						"</tbody>"+
					"</table>";
			var hasil_tabel_history_koding_ina = tabel_history_koding_ina_awal+tabel_history_koding_ina_isi+tabel_history_koding_ina_akhir;
			$('#parent_tabel_history_koding_ina').html(hasil_tabel_history_koding_ina);
			$('#tabel_history_koding_ina').DataTable();
		}
	});
}
$('#button_modal_ubah_kelas').click(function(){
	$('#id_kelas_ditempati').val($('#id_kelas_ditempati option:first').val()).trigger('change');
	$('.selectpicker').selectpicker('refresh');
	$('#ket_ubah_kelas').val('');
	$('#judul_ubah_kelas').html('(Nomor SEP: '+$('#nosep').val()+')');
	$('#hak_kelas').val($('#desc_hak_kelas_hidden').val());
	$('#kelas_ditempati').val($('#desc_kelas_ditempati_hidden').val());
	var n_reg_id = $('#n_reg_id_hidden').val();
	$.ajax({
		url: base_url+"CasemixRawatInap/cari_header_ubah_kelas",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			var tabel_header_ubah_kelas =
				"DETAIL PASIEN RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>";
						"</td>" +
					"</table>" +
			$('#parent_header_ubah_kelas').html(tabel_header_ubah_kelas);
		}
	});
	$('#modal_ubah_kelas').modal('show');
});
$('#button_ubah_kelas').click(function(){
	var n_reg_id = $('#n_reg_id_hidden').val();
	var id_kelas_ditempati_lama = $('#id_kelas_ditempati_hidden').val();
	var id_kelas_ditempati_baru = $('#id_kelas_ditempati').val();
	var ket_ubah_kelas = $('#ket_ubah_kelas').val();
	var nosep = $('#nosep').val();
	ubah_kelas(n_reg_id, id_kelas_ditempati_lama, id_kelas_ditempati_baru, ket_ubah_kelas, nosep);
});
function ubah_kelas(n_reg_id, id_kelas_ditempati_lama, id_kelas_ditempati_baru, ket_ubah_kelas, nosep) {
	if (id_kelas_ditempati_baru != null) {
		$.ajax({
			url: base_url+"CasemixRawatInap/ubah_kelas",
			type: "post",
			data: {
				n_reg_id: n_reg_id,
				id_kelas_ditempati_lama: id_kelas_ditempati_lama,
				id_kelas_ditempati_baru: id_kelas_ditempati_baru,
				ket_ubah_kelas: ket_ubah_kelas
			},
			datatype: 'json',
			async: false,
			success: function(response) {
				cari_detail_casemix(n_reg_id, nosep);
				$('#modal_ubah_kelas').modal('hide');
				swal({   
					title: "Sukses!",
					text: "Kelas berhasil diubah",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			}
		});
	} else {
		swal({   
			title: "Gagal!",
			text: "Data anda gagal diubah, silahkan mengisi data dengan benar",
			timer: 1500,
			showConfirmButton: false,
			type: "error",
		});
	}
}
$('#koding_hak_kelas').keyup(function () {
	hitung_iuran();
});
$('#koding_ditempati').keyup(function () {
	hitung_iuran();
});
function hitung_iuran() {
	var real_cost = parseInt($('#real_cost_hidden').val());



	if ($('#koding_hak_kelas').val() != "") {
		var koding_hak_kelas = parseInt(($('#koding_hak_kelas').val()).split('.').join(""));
	} else {
		var koding_hak_kelas = 0;
	}

	// cek apakah kelas hak sama dengan kelas rawat
	if($('#id_hak_kelas_hidden').val()==$('#id_kelas_ditempati_hidden').val()){
		$('#koding_ditempati').val($('#koding_hak_kelas').val());
		var koding_ditempati = ($('#koding_hak_kelas').val()).split('.').join("");
	}

	if ($('#koding_ditempati').val() != "") {
		var koding_ditempati = parseInt(($('#koding_ditempati').val()).split('.').join(""));
	} else {
		var koding_ditempati = 0;
	}
	$("#koding_hak_kelas").mask('0.000.000.000', {reverse: true});
	$("#koding_ditempati").mask('0.000.000.000', {reverse: true});

	// console.log('kelas rawat hidden'+ $('#kelas_rawat_hidden').val() );

	if (!isNaN(koding_hak_kelas) || !isNaN(koding_ditempati)) {
		if ($('#kelas_rawat_hidden').val().includes (1) ||
			$('#kelas_rawat_hidden').val().includes (2) ||
			$('#kelas_rawat_hidden').val().includes (3) ||
			$('#kelas_rawat_hidden').val().includes ('I') ||
			$('#kelas_rawat_hidden').val().includes ('II') ||
			$('#kelas_rawat_hidden').val().includes ('III')) {
			// console.log('koding ditempati'+koding_ditempati);
			// console.log('koding hak kelas'+koding_hak_kelas);
			var hitung_iuran = (koding_ditempati - koding_hak_kelas);
			var selisih = real_cost - (koding_hak_kelas + hitung_iuran);
			$('#iuran').val('Rp. '+numeral(String(hitung_iuran)).format('0,0'));
			$('#selisih').val('Rp. '+numeral(String(selisih)).format('0,0'));
			$('#iuran_hidden').val(hitung_iuran);
		} else {
			var hitung_iuran = (75/100)*(koding_hak_kelas);
			var selisih = real_cost - (koding_hak_kelas + hitung_iuran);
			$('#iuran').val('Rp. '+numeral(String(hitung_iuran)).format('0,0'));
			$('#selisih').val('Rp. '+numeral(String(selisih)).format('0,0'));
			$('#iuran_hidden').val(hitung_iuran);
		}
	} else {
		$('#iuran_hidden').val('');
		$('#iuran').val('Rp. 0');
		$('#selisih').val('Rp. 0');
	}
}
$('#button_simpan_koding').click(function(){
	var n_reg_id = $('#n_reg_id_hidden').val();
	if ($('#koding_hak_kelas').val() != "") {
		var koding_hak_kelas = ($('#koding_hak_kelas').val()).split('.').join("");
	} else {
		var koding_hak_kelas = 0;
	}
// cek kelas hak dan kelas yang ditempati
	if($('#id_hak_kelas_hidden').val()==$('#id_kelas_ditempati_hidden').val()){
		$('#koding_ditempati').val($('#koding_hak_kelas').val());
		var koding_ditempati = ($('#koding_hak_kelas').val()).split('.').join("");
	}

	if ($('#koding_ditempati').val() != "") {
		var koding_ditempati = ($('#koding_ditempati').val()).split('.').join("");
	} else {
		var koding_ditempati = 0;
	}

	if ($('#real_cost_hidden').val() != "") {
		var real_cost = $('#real_cost_hidden').val();
	} else {
		var real_cost = 0;
	}
	if ($('#iuran_hidden').val() != "") {
		var iur_max = $('#iuran_hidden').val();
	} else {
		var iur_max = 0;
	}
	var id_koding_hak_kelas = $('#hak_kelas_hidden').val();
	if ($('#kelas_rawat_hidden').val() == 1 || $('#kelas_rawat_hidden').val() == 'I') {
		var id_koding_ditempati = 6;
	} else if ($('#kelas_rawat_hidden').val() == 2 || $('#kelas_rawat_hidden').val() == 'II') {
		var id_koding_ditempati = 7;
	} else if ($('#kelas_rawat_hidden').val() == 3 || $('#kelas_rawat_hidden').val() == 'III') {
		var id_koding_ditempati = 8;
	} else {
		var id_koding_ditempati = 11;
	}
	simpan_koding(n_reg_id,koding_hak_kelas,koding_ditempati,real_cost,iur_max,id_koding_hak_kelas,id_koding_ditempati);
});
function simpan_koding(n_reg_id,koding_hak_kelas,koding_ditempati,real_cost,iur_max,id_koding_hak_kelas,id_koding_ditempati) {
	$.ajax({
		url: base_url+"CasemixRawatInap/simpan_koding",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			koding_hak_kelas: koding_hak_kelas,
			koding_ditempati: koding_ditempati,
			real_cost: real_cost,
			iur_max: iur_max,
			id_koding_hak_kelas: id_koding_hak_kelas,
			id_koding_ditempati: id_koding_ditempati
		},
		datatype: 'json',
		success: function(response) {
			$('#koding_hak_kelas').val('');
			$('#koding_ditempati').val('');
			$('#iuran_hidden').val('');
			$('#iuran').val('Rp. 0');
			$('#selisih').val('Rp. 0');
			
			refresh_casemix();
			$('#pemberitahuan_input_keterangan').show();
			$('#parent_detail_tabel').hide();
			$('#parent_input_koding_ina').hide();

			swal({   
				title: "Sukses!",
				text: "Data SEP anda berhasil disimpan",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}
//CASEMIX

//PDF
function modal_upload_file(v_nosepbpjs, tglsep) {
	$('#no_sep_hidden').val('');
	$('#tgl_sep_hidden').val('');
	$('#no_sep_hidden').val(v_nosepbpjs);
	$('#tgl_sep_hidden').val(tglsep);
	$('#judul_upload_no_sep').html('(Nomor SEP: '+v_nosepbpjs+')');
	$('#modal_upload_file').modal('show');
	myDropzone.removeAllFiles();
}
Dropzone.autoDiscover = false;
var myDropzone = new Dropzone("div#mydropzone", {
	url: base_url+"RawatInap/upload_file",
	autoProcessQueue: false,
	paramName: "file_diupload",
	clickable: true,
	maxFilesize: 20, //in mb
	addRemoveLinks: false,
	acceptedFiles: '.pdf',
	init: function() {
		this.on("sending", function(file, xhr, formData) {
		  	//console.log("sending file");
		  	formData.append('no_sep', $('#no_sep_hidden').val());
		  	formData.append('tgl_sep', $('#tgl_sep_hidden').val());
		});
		this.on("success", function(file, responseText) {
		  	//console.log('great success');
		  	//console.log(file);
		  	//console.log(responseText);
		});
		this.on("addedfile", function(file){
			//console.log('file added');
		});
		var submitButton = document.querySelector("#button_upload_file");
		myDropzone = this;
		submitButton.addEventListener("click", function() {
			myDropzone.processQueue();
		});
	}
});
function modal_lihat_file(v_nosepbpjs, tglsep) {
	$('#parent_lihat_file').empty();
	var hasil_lihat_file = "<iframe src='' type='application/pdf' style='margin-left:15px;width:100%;height:420px;''></iframe>";
	$('#parent_lihat_file').html(hasil_lihat_file);
	nama_file(v_nosepbpjs, tglsep);
}
function nama_file(v_nosepbpjs, tglsep) {
	$.ajax({
		url: base_url+"RawatInap/nama_file",
		type: "post",
		data: {
			v_nosepbpjs: v_nosepbpjs
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			if (response_parse != 'gagal') {
				$('#parent_nama_file').empty();

				var tabel_nama_file_awal =
						"<table class='table table-bordered'>"+
							"<thead>"+
								"<th>#</th>"+
								"<th>Nama File</th>"+
								"<th style='text-align:center;'>Aksi</th>"+
							"</thead>"+
							"<tbody>";
				
				var nomor_file = 1;
				var tabel_nama_file_isi = "";
				var nosep_response = response_parse.nosep;
				var tglsep_response = (response_parse.tglsep).replace(/\-/g,'');
				$.each(response_parse.file, function (index, dataNamaFile) {
					var dataNamaFile_lempar = '"'+dataNamaFile+'"';
					var nosep_response_lempar = '"'+nosep_response+'"';
					var tglsep_response_lempar = '"'+tglsep_response+'"';
					tabel_nama_file_isi +=
								"<tr class='row_nama_file' namaFilePilih='"+dataNamaFile+","+nosep_response+","+tglsep_response+"'>"+
									"<td>" + nomor_file + "</td>" +
									"<td>" + dataNamaFile + ".pdf</td>" +
									"<td style='text-align:center;'><button type='button' class='btn bg-orange btn-circle waves-effect waves-circle waves-float' title='Hapus PDF' onclick='modal_hapus_pdf("+dataNamaFile_lempar+","+nosep_response_lempar+","+tglsep_response_lempar+")'><i class='material-icons'>delete</i></button></td>" +
								"</tr>";
					nomor_file++;
				});
				var tabel_nama_file_akhir =
							"</tbody>"+
						"</table>";
				var hasil_tabel_nama_file = tabel_nama_file_awal+tabel_nama_file_isi+tabel_nama_file_akhir;

				$('#parent_nama_file').html(hasil_tabel_nama_file);
				$('#judul_lihat_no_sep').html('(Nomor SEP: '+nosep_response+')');
				$('#modal_lihat_file').modal('show');
			} else {
				swal({
					title: "Gagal!",
					text: "File belum ada, silahkan upload file terlebih dahulu",
					type: "error",
				});
			}
		}
	});
}
$(document).on("click", ".row_nama_file", function () {
    var nama_file = $(this).attr('namaFilePilih');
    var nama_file_array = nama_file.split(',');
    var nama_file_pdf = nama_file_array[0];
    var no_sep = nama_file_array[1];
    var tgl_sep = nama_file_array[2];
    lihat_file(no_sep,tgl_sep,nama_file_pdf);

    $(this).addClass('pilih_row').siblings().removeClass('pilih_row');
});
function lihat_file(no_sep,tgl_sep,nama_file_pdf) {
	/*$.ajax({
		url: base_url+"RawatInap/lihat_file/"+no_sep+"/"+nama_file_pdf,
		type: "post",
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			console.log(response_parse);
		}
	});*/
	$('#parent_lihat_file').empty();
	var url = base_url+"RawatInap/lihat_file/"+no_sep+"/"+tgl_sep+"/"+nama_file_pdf;
	var hasil_lihat_file = "<iframe src='"+url+"' type='application/pdf' style='margin-left:15px;width:100%;height:420px;''></iframe>";
	$('#parent_lihat_file').html(hasil_lihat_file);
}

function modal_hapus_pdf(namafile, nosep, tglsep) {
	swal({
		title: "Apakah anda yakin ingin menghapus file ini?",
		text: "File yang akan anda hapus adalah '"+namafile+".pdf'",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: 'orange',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, hapus!'
	}, function(){
		$.ajax({
			url: base_url+"RawatInap/hapus_pdf",
			type: "post",
			data: {
				namafile: namafile,
				nosep: nosep,
				tglsep: tglsep
			},
			datatype: 'json',
			success: function(data) {
				if (data == "kosong") {
					$('#modal_lihat_file').modal('hide');
				} else {
					nama_file(nosep, tglsep);
				}
				swal({   
					title: "Sukses!",
					text: "PDF berhasil dihapus",
					timer: 1500,
					showConfirmButton: false,
					type: "success",
				});
			}
		});
	});
}
//PDF

//CETAK BY REG
function modal_buat_pdf(n_reg_id,nosep,tglsep,sum_real_cost) {
	console.log(n_reg_id);
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatInap/cari_nilai_paket_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			sum_real_cost: sum_real_cost
		},
		datatype: 'json',
		async: false,
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			$('#nosep_buat_pdf_hidden').val('');
			$('#regid_buat_pdf_hidden').val('');
			$('#tglsep_buat_pdf_hidden').val('');
			$('#nosep_buat_pdf_hidden').val(nosep);
			$('#regid_buat_pdf_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_hidden').val(tglsep);
			$('#parent_buat_pdf').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;border-collapse: collapse;' border='1'>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<th style='padding: 5px;text-align:center;'>No.</th>" +
						"<th style='padding: 5px;text-align:center;'>Kelompok</th>" +
						"<th style='padding: 5px;text-align:center;'>Total</th>" +
					"</tr>";
			var nomor_nota_paket = 1;
			var total_nota_paket = 0;
			$.each(response_parse.nota_paket, function (index, dataNotaPaket) {
				if (dataNotaPaket.total != null) {
					total_nota_paket = dataNotaPaket.total;
				} else {
					total_nota_paket = 0;
				}
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;text-align:center;'>" + nomor_nota_paket + "</td>" +
						"<td style='padding: 5px'>" + dataNotaPaket.namakel + "</td>" +
						"<td style='padding: 5px' align='right'>" + numeral(total_nota_paket).format('0,0') + "</td>" +
					"</tr>";
				nomor_nota_paket++;
			});
			tabel_buat_pdf +=
				"</table>" +
				"<table align='right'>" +
					"<tr>" +
						"<td style='padding: 5px;'><b>Jumlah Biaya : Rp. " + numeral(response_parse.sum_real_cost).format('0,0') +"</b></td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td><br/><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
		}
	});
	$('#parent_buat_pdf').html(tabel_buat_pdf);
	if (nosep != 'null') {
		$('#judul_buat_pdf').html('(Nomor SEP: '+nosep+')');
	} else {
		$('#judul_buat_pdf').html('');
	}
	$('#modal_buat_pdf').modal('show');
}

function modal_buat_detail_pdf(n_reg_id,nosep,tglsep,sum_real_cost) {
	console.log(n_reg_id);
	var tabel_buat_pdf = "";
	$.ajax({
		url: base_url+"RawatInap/cari_detail_nilai_paket_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id,
			sum_real_cost: sum_real_cost
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			//console.log(response_parse);
			$('#nosep_buat_detail_pdf_hidden').val('');
			$('#regid_buat_detail_pdf_hidden').val('');
			$('#tglsep_buat_detail_pdf_hidden').val('');
			$('#nosep_buat_detail_pdf_hidden').val(nosep);
			$('#regid_buat_detail_pdf_hidden').val(n_reg_id);
			$('#tglsep_buat_detail_pdf_hidden').val(tglsep);
			$('#parent_buat_detail_pdf').empty();
			tabel_buat_pdf +=
				"PERINCIAN BIAYA RAWAT INAP<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;'>" +
					"<tr>" +
						"<td style='padding: 5px;' colspan='5'>KAMAR KEPERAWATAN</td>" +
					"</tr>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;'>NOTA RUANGAN</td>" +
						"<td style='padding: 5px;'>TANGGAL MASUK</td>" +
						"<td style='padding: 5px;'>TANGGAL KELUAR</td>" +
						"<td style='padding: 5px;'>JUMLAH JAM</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			
			var total_nota_paket = 0;
			$.each(response_parse.kasur_detail, function (index, dataKasurDetail) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.nota + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.masuk).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + moment(dataKasurDetail.keluar).format('DD/MM/YYYY') + "</td>" +
						"<td style='padding: 5px;'>" + dataKasurDetail.jam + "</td>" +
						"<td style='padding: 5px;text-align:right;'>" + numeral(dataKasurDetail.total).format('0,0') + "</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(response_parse.sum_fungsi_kasur_detail).format('0,0') +"</td>" +
					"</tr>" +
				"</table><br/>";
			tabel_buat_pdf +=
				"<table style='width:100%;'>" +
					"<tr style='border-top: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;'>" +
						"<td style='padding: 5px;' colspan='4'>KELOMPOK BIAYA</td>" +
						"<td style='padding: 5px;text-align:right;'>SUBTOTAL</td>" +
					"</tr>";
			$.each(response_parse.cetak_byreg, function (index, dataCetakByReg) {
				tabel_buat_pdf +=
					"<tr>" +
						"<td style='padding: 5px;font-size: 18px;' colspan='5'>" + dataCetakByReg.kelompok + "</td>" +
					"</tr>";
				$.ajax({
					url: base_url+"RawatInap/get_cetak_byreg_kojek_by_kelompok",
					type: "post",
					data: {
						n_reg_id: n_reg_id,
						kelompok: dataCetakByReg.kelompok
					},
					datatype: 'json',
					async: false,
					success: function(response_by_kelompok) {
						var response_by_kelompok_parse = JSON.parse(response_by_kelompok);
						$.each(response_by_kelompok_parse, function (index, dataByKelompok) {
							tabel_buat_pdf +=
								"<tr>" +
									"<td>&emsp;&emsp;</td>" +
									"<td style='padding: 5px;'>" + moment(dataByKelompok.tgl).format('DD/MM/YYYY') + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.ket + "</td>" +
									"<td style='padding: 5px;'>" + dataByKelompok.qty + "</td>" +
									"<td style='padding: 5px;text-align:right;'>" + numeral(dataByKelompok.subttl).format('0,0') + "</td>" +
								"</tr>";
						});
					}
				});
				tabel_buat_pdf +=
					"<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style='padding: 5px;border-top: 1px solid black;border-collapse: collapse;' align='right'>" + numeral(dataCetakByReg.sum_total).format('0,0') +"</td>" +
					"</tr>";
			});
			tabel_buat_pdf +=
				"</table>" +
				"<table align='right'>" +
					"<tr>" +
						"<td style='padding: 5px;'><b>Jumlah Biaya : Rp. " + numeral(response_parse.sum_real_cost).format('0,0') +"</b></td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td><br/><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			$('#parent_buat_detail_pdf').html(tabel_buat_pdf);
			if (nosep != 'null') {
				$('#judul_buat_detail_pdf').html('(Nomor SEP: '+nosep+')');
			} else {
				$('#judul_buat_detail_pdf').html('');
			}
			$('#modal_buat_detail_pdf').modal('show');
		}
	});
}
//CETAK BY REG


//CETAK BY REG

//PDF
$('#button_buat_pdf').click(function(){
	var nosep_buat_pdf = $('#nosep_buat_pdf_hidden').val();
	var tglsep_buat_pdf = $('#tglsep_buat_pdf_hidden').val();
	var regid_buat_pdf = $('#regid_buat_pdf_hidden').val();
	var cek_buat_pdf = 1; //GRUP INA
	buat_pdf(nosep_buat_pdf, tglsep_buat_pdf, regid_buat_pdf, cek_buat_pdf);
});
$('#button_buat_detail_pdf').click(function(){
	var nosep_buat_detail_pdf = $('#nosep_buat_detail_pdf_hidden').val();
	var tglsep_buat_detail_pdf = $('#tglsep_buat_detail_pdf_hidden').val();
	var regid_buat_detail_pdf = $('#regid_buat_detail_pdf_hidden').val();
	var cek_buat_pdf = 2; //CETAK BY REG
	buat_pdf(nosep_buat_detail_pdf, tglsep_buat_detail_pdf, regid_buat_detail_pdf, cek_buat_pdf);
});
$('#button_buat_pdf_lab').click(function(){
	var nosep_buat_pdf_lab = $('#nosep_buat_pdf_lab_hidden').val();
	var tglsep_buat_pdf_lab = $('#tglsep_buat_pdf_lab_hidden').val();
	var regid_buat_pdf_lab = $('#regid_buat_pdf_lab_hidden').val();
	var cek_buat_pdf_lab = 3; //REKAP HASIL LAB
	buat_pdf(nosep_buat_pdf_lab, tglsep_buat_pdf_lab, regid_buat_pdf_lab, cek_buat_pdf_lab);
});
function buat_pdf(nosep, tglsep, regid, cek) {
	if (cek == 1) {
		var html_buat_pdf = $('#parent_buat_pdf').html();
	} else if (cek == 2) {
		var html_buat_pdf = $('#parent_buat_detail_pdf').html();
	} else if (cek == 3) {
		var html_buat_pdf = $('#parent_buat_pdf_lab').html();
	}
	//console.log(html_buat_pdf);
	$.ajax({
		url: base_url+"RawatInap/buat_pdf",
		type: "post",
		data: {
			nosep: nosep,
			tglsep: tglsep,
			regid: regid,
			cek: cek,
			html_buat_pdf: html_buat_pdf
		},
		datatype: 'json',
		success: function(response) {
			swal({
				title: "Sukses!",
				text: "PDF berhasil dibuat",
				timer: 1500,
				showConfirmButton: false,
				type: "success",
			});
		}
	});
}
//PDF

//LIS
function modal_buat_pdf_lab(n_reg_id,nosep,tglsep) {
	var tabel_buat_pdf_lab = "";
	var tabel_buat_pdf_lab_isi = "";
	var cek_rekap = "";
	$.ajax({
		url: base_url+"RawatInap/cari_lab_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_lab_hidden').val('');
			$('#regid_buat_pdf_lab_hidden').val('');
			$('#tglsep_buat_pdf_lab_hidden').val('');
			$('#nosep_buat_pdf_lab_hidden').val(nosep);
			$('#regid_buat_pdf_lab_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_lab_hidden').val(tglsep);
			$('#parent_buat_pdf_lab').empty();
			//console.log(response_parse);
			tabel_buat_pdf_lab +=
				"REKAP HASIL LABORAT<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
									"<td style='padding-right: 5px;' valign='top'>:</td>" +
									"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
						"<td>" +
							"<table>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
									"<td style='padding-right: 5px;'>:</td>" +
									"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
								"</tr>" +
								"<tr>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
									"<td><br/></td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</table>" +
				"<table style='width:100%;' id='tabel_rekap_hasil_lab'>";
				$.each(response_parse.list_vnoteno, function (index, dataVnoteno) {
					$.ajax({
						url: base_url+"RawatInap/cari_lab_buat_pdf_rekap",
						type: "post",
						data: {
							vnoteno: dataVnoteno.v_note_no
						},
						datatype: 'json',
						async: false,
						success: function(response_rekap) {
							var response_rekap_parse = JSON.parse(response_rekap);
							//console.log(response_rekap_parse);
							if (response_rekap_parse != null) {
								tabel_buat_pdf_lab +=
									"<tr style='border-bottom: 1px solid black;border-top: 1px solid black;border-collapse: collapse;'>" +
										"<td colspan='4' style='padding: 5px;'>" + dataVnoteno.v_note_no + "</td>" +
									"</tr>" +
									"<tr>" +
										"<td>Nama Tes</td>" +
										"<td>Hasil Tes</td>" +
										"<td>Nilai Normal</td>" +
										"<td>Nama Unit Tes</td>" +
									"</tr>";
								if (response_rekap_parse != null) {
									$.each(response_rekap_parse, function (index, dataRekap) {
										if (dataRekap.test_name != "") {
											var nama_tes = dataRekap.test_name;
										} else {
											var nama_tes = '-';
										}
										if (dataRekap.hasil != "") {
											var hasil_tes = dataRekap.hasil;
										} else {
											var hasil_tes = '-';
										}
										if (dataRekap.nilai_normal != "") {
											var nilai_normal = dataRekap.nilai_normal;
										} else {
											var nilai_normal = '-';
										}
										if (dataRekap.test_units_name != "") {
											var nama_unit_tes = dataRekap.test_units_name;
										} else {
											var nama_unit_tes = '-';
										}
										tabel_buat_pdf_lab_isi +=
											"<tr>" +
												"<td>"+nama_tes+"</td>" +
												"<td>"+hasil_tes+"</td>" +
												"<td>"+nilai_normal+"</td>" +
												"<td>"+nama_unit_tes+"</td>" +
											"</tr>";
									});
									cek_rekap += response_rekap_parse;
								}
							}
						}
					});
				tabel_buat_pdf_lab += tabel_buat_pdf_lab_isi;
			});
			tabel_buat_pdf_lab +=
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td colspan='5'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			if (cek_rekap != "") {
				$('#parent_buat_pdf_lab').html(tabel_buat_pdf_lab);
				if (nosep != 'null') {
					$('#judul_buat_pdf_lab').html('(Nomor SEP: '+nosep+')');
				} else {
					$('#judul_buat_pdf_lab').html('');
				}
				$('#modal_buat_pdf_lab').modal('show');
			} else {
				swal({   
					title: "Data hasil rekap laborat belum ada",
					type: "error",
				});
			}
		}
	});
	
}
//LIS


//DETAIL IURAN
function modal_buat_pdf_iuran(n_reg_id,nosep,tglsep) {
	var tabel_buat_pdf_lab = "";
	var tabel_buat_pdf_lab_isi = "";
	var cek_rekap = "";
	$.ajax({
		url: base_url+"RawatInap/cari_iuran_buat_pdf",
		type: "post",
		data: {
			n_reg_id: n_reg_id
		},
		datatype: 'json',
		success: function(response) {
			var response_parse = JSON.parse(response);
			$('#nosep_buat_pdf_iuran_hidden').val('');
			$('#regid_buat_pdf_iuran_hidden').val('');
			$('#tglsep_buat_pdf_iuran_hidden').val('');
			$('#nosep_buat_pdf_iuran_hidden').val(nosep);
			$('#regid_buat_pdf_iuran_hidden').val(n_reg_id);
			$('#tglsep_buat_pdf_iuran_hidden').val(tglsep);
			$('#parent_buat_pdf_iuran').empty();
			//console.log(response_parse);
			tabel_buat_pdf_lab +=
				"PERINCIAN DETAIL IURAN<br/>" +
				"RS Dr. OEN SURAKARTA<br/>" +
				"Jl. Brigjen Katamso No. 55 Surakarta<br/><br/>" +
				"<table>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>No. Register</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_reg_secondary_id + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>No. Rekam Medik</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_mr_code + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Nama Pasien</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' valign='top' nowrap>Alamat</td>" +
						"<td style='padding-right: 5px;' valign='top'>:</td>" +
						"<td>" + response_parse.tb_registration.v_patient_main_addr + "</td>" +
					"</tr>" +
					/*"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Ruang Rawat</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_bed_desc + "</td>" +
					"</tr>" +*/
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Kelas</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.kelas_ditempati + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Penanggung</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.v_insurance_name + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td style='padding-right: 10px;' nowrap>Jatah Kelas</td>" +
						"<td style='padding-right: 5px;'>:</td>" +
						"<td>" + response_parse.tb_registration.hak_kelas + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td><br/></td>" +
						"<td><br/></td>" +
						"<td><br/></td>" +
					"</tr>" +
				"</table>" +
				"<table style='width:40%;' id='tabel_detail_iuran'>" +
					"<tr>" +
						"<td>Nilai Koding Hak</td>" +
						"<td align='right'>" + response_parse.iuran.n_codding_hak + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td>Nilai Koding Ditempati</td>" +
						"<td align='right'>" + response_parse.iuran.n_codding_ditempati + "</td>" +
					"</tr>" +
					"<tr>" +
						"<td>Iuran</td>" +
						"<td style='border-top: 1px solid black;border-collapse: collapse;' align='right'>" + response_parse.iuran.n_iur_max + "</td>" +
					"</tr>" +
				"</table>" +
				"<table>" +
					"<tr>" +
						"<td colspan='5'><br/>" + moment().format('DD/MM/YYYY HH:mm:ss') + "</td>" +
					"</tr>" +
				"</table>";
			$('#parent_buat_pdf_iuran').html(tabel_buat_pdf_lab);
			if (nosep != 'null') {
				$('#judul_buat_pdf_iuran').html('(Nomor SEP: '+nosep+')');
			} else {
				$('#judul_buat_pdf_iuran').html('');
			}
			$('#modal_buat_pdf_iuran').modal('show');
		}
	});
	
}
$('#button_cetak_detail_iuran').click(function(){
	var regid = $('#regid_buat_pdf_iuran_hidden').val();
	var html = $('#parent_buat_pdf_iuran').html();
	print_detail_iuran(regid, html);
});
function print_detail_iuran(regid, html) {
	$.ajax({
        url: 'RawatInap/cetak_detail_iuran',
        method: 'POST',
        dataType: 'JSON',
        async: false,
        data: {
            html: html,
            regid: regid
        },
        success: function (response) {
            printJS(response.response);
        },error: function (error) {
            alert('error; ' + eval(error));
        }
    });
}
//DETAIL IURAN

//mendefinisikan no SEP berdasarkan no kartu dari history registrasi
function getSepPasien(mrId,reg_date) {
	var sep_default = new Array();
	$.ajax({
			url: base_url+"RawatInap/get_noKartu",
			type: "post",
			data: {
				mrId: mrId
			},
			dataType: 'json',
			async: false,
			success: function(response) {
				//var response1 = JSON.parse(response);
				//console.log('no kartu : '+response);
				var nokartu = response;
				//console.log('kartu '+nokartu);
				if(nokartu!= null || nokartu !=''){
					$.ajax({
						url: base_url+"RawatInap/cari_sep_durasi",
						type: "post",
						data: {
							nokartu: nokartu,
							tglAwal:reg_date
						},
						dataType: 'json',
						async: false,
						success: function(responseSep) {
							//console.log(responseSep);
							if(responseSep){
								//var responseSep1 = JSON.parse(responseSep);
								//console.log('NO SEP: '+responseSep1);
								sep_default=[responseSep.SEP,responseSep.icd,responseSep.kelasRawat] ;
							}else{
								sep_default=['','',''];
							}
						}
					});
				}
			},
			error: function (error) {
		      alert("error cari no kartu; " + eval(error));
		    }
		});
	//console.log(sep_default);
	return sep_default;
}

function get_nilai_koding_default(icd_code) {
	var nilai_koding = new Array();
	$.ajax({
        url: base_url+'RawatInap/get_koding_default',
        method: 'POST',
        dataType: 'JSON',
        async: false,
        data: {
            icd_code: icd_code
        },
        success: function (response) {
           if(response){
           	nilai_koding=[response.nilai_koding_i,response.nilai_koding_ii,response.nilai_koding_iii];
           }else{
           	nilai_koding=null;
           }
        },error: function (error) {
            alert('error; ' + eval(error));
        }
    });
    return nilai_koding;
}

function get_detail_sep(no_sep) {
	var data_sep = new Array();
	$.ajax({
        url: base_url+'RawatInap/cari_sep_komplit',
        method: 'POST',
        dataType: 'JSON',
        async: false,
        data: {
            nomor_sep: no_sep
        },
        success: function (response) {
           if(response){
           	data_sep=[response.response.noSep,response.response.kelasRawat,response.response.peserta.hakKelas];
           }else{
           	data_sep=null;
           }
        },error: function (error) {
            alert('error; ' + eval(error));
        }
    });
    return data_sep;
}
// mendefinisikan no SEP berdasarkan no kartu dari history registrasi