$.ajax({
	url: base_url+"RawatInap/select_ward_name_byUser",
	type: "post",
	dataType: 'json',
	async: false,
	success: function(response) {
		$('#ward_name').html('<option selected disabled>-- Pilih Ruang Rawat Inap --</option>');
		$.each(response,function(index,data){
			$('#ward_name').append('<option value="'+data['n_ward_id']+'">'+data['v_ward_name']+'</option>');
		});
	}
});