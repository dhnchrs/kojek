<section class="content">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>Rawat Jalan</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control bootstrapMaterialDatePicker" data-date-format="dd/mm/yyyy" name="tanggal_awal" id="tanggal_awal" value="<?php echo date('d/m/Y'); ?>">
									<label class="form-label">Tanggal SEP Awal</label>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control bootstrapMaterialDatePicker" data-date-format="dd/mm/yyyy" name="tanggal_akhir" id="tanggal_akhir" value="<?php echo date('d/m/Y'); ?>">
									<label class="form-label">Tanggal SEP Akhir</label>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_cari_rawat_jalan"><i class="material-icons">search</i> Cari Data</button>
						</div>
					</div>
					<div class="table-responsive">
						<div class="header" id="pemberitahuan_input">
							<span><b>SILAHKAN PILIH PERIODE TANGGAL TERLEBIH DAHULU</b></span>
						</div>
						<div id="parent_tabel_rawat_jalan"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal_input_sep_gabung">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Tabel Keterangan SEP Pasien</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div id="parent_nosep_lama">
									<div class="row clearfix">
										<div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
											<div class="form-group form-float">
												<div class="form-line focused">
													<input class="form-control" name="nosep_lama" id="nosep_lama" readonly="" value=" " style="font-weight: bold;">
													<label class="form-label">Nomor SEP Sekarang</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<input type="hidden" id="cek_edit_atau_simpan_sep">
									<input type="hidden" id="input_nosep_gabungan_lama">
									<div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
										<div class="form-group form-float">
											<input type="hidden" id="nama_pasien_konfirmasi_input">
											<div class="form-line focused">
												<input class="form-control" name="input_nosep_gabungan" id="input_nosep_gabungan" readonly="" value=" " style="font-weight: bold;">
												<label class="form-label">Masukkan Nomor SEP</label>
											</div>
										</div>
									</div>
									<div id="parent_button_edit_sep_rajal">
										<div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
											<div class="form-group form-float">
												<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' onclick='edit_sep_rajal()' style='margin-top: 5px;margin-left: -2px;'><i class='material-icons'>edit</i> Edit</button>
											</div>
										</div>
									</div>
								</div>
								<div id="parent_cek_gabung_sep">
									<div class="demo-checkbox">
		                                <input type="checkbox" id="cek_gabung_sep"/>
		                                <label for="cek_gabung_sep">Apakah anda ingin menggabungkan SEP?</label>
		                            </div>
	                            </div>
	                            <br/>
								<div id="parent_sep_gabung">
									<div id="parent_tabel_modal_sep_gabung"></div>
									<div id="parent_button_sep_gabung"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_input_sep" style="zIndex:9999;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Input SEP</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
	                            <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_rs" id="nomor_rs" style="color:blue;font-weight:bold;" disabled>
			                                    <label class="form-label">Nomor SEP</label>
			                                </div>
			                            </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_sep" id="nomor_sep" required>
			                                </div>
			                            </div>
                                    </div>
                                </div>
	                            <div id="parent_keterangan_no_sep">
	                            	<input type="hidden" name="nosep" id="nosep">
	                            	<input type="hidden" name="tglsep" id="tglsep">
	                            	<input type="hidden" name="nokartu" id="nokartu">
	                            	<input type="hidden" name="namapeserta" id="namapeserta">
	                            	<input type="hidden" name="peserta" id="peserta">
	                            	<input type="hidden" name="tgllahir" id="tgllahir">
	                            	<input type="hidden" name="jnskelamin" id="jnskelamin">
	                            	<input type="hidden" name="politujuan" id="politujuan">
	                            	<input type="hidden" name="jnsrawat" id="jnsrawat">
	                            	<input type="hidden" name="klsrawat" id="klsrawat">
	                            	<input type="hidden" name="diagnosaawal" id="diagnosaawal">
	                            	<input type="hidden" name="catatan" id="catatan">
	                            	<input type="hidden" name="regid" id="regid">
	                            	<input type="hidden" name="nama_pasien" id="nama_pasien">
	                            	<input type="hidden" name="idkelasstandart" id="idkelasstandart">
	                            	<input type="hidden" name="no_rujukan" id="no_rujukan">
				                    <div class="row">
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_peserta" id="nama_peserta" style="color:blue;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Peserta</label>
				                                </div>
				                            </div>
				                        </div>
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_pasien_konfirmasi" id="nama_pasien_konfirmasi" style="color:red;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Pasien (Konfirmasi SEP)</label>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="kelas_rawat" id="kelas_rawat" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Kelas Rawat</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="hak_kelas" id="hak_kelas" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Hak Kelas</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <textarea rows="3" class="form-control no-resize" name="diagnosa" id="diagnosa" style="color:blue;font-weight:bold;" readonly></textarea>
		                                    <label class="form-label">Diagnosa</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="catatan" id="catatan" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Catatan</label>
		                                </div>
		                            </div>
		                            <div class="demo-checkbox">
		                                <input type="checkbox" id="konfirmasi_input"/>
		                                <label for="konfirmasi_input">Apakah anda yakin data SEP tersebut benar?</label>
		                            </div>
		                            <div class="form-group form-float" id="parent_text_input_sep">
										<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id='text_input_sep' style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">input</i> Input</button>
		                            </div>
		                        </div>
								<!--<button type="button" class="btn bg-orange m-t-15 waves-effect" data-dismiss="modal">Back</button>
								<button type="button" class="btn bg-orange m-t-15 waves-effect" id="input_sep" onclick="input_sep()">Save</button>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_upload_file_rajal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Upload File ke Server <span id="judul_upload_no_sep_rajal"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<input type="hidden" id="no_sep_hidden_rajal"/>
								<input type="hidden" id="tgl_sep_hidden_rajal"/>
						        <div class="dropzone" id="mydropzone_rajal">
						        	<div class="dz-message">
	                                    <div class="drag-icon-cph">
	                                        <i class="material-icons">touch_app</i>
	                                    </div>
	                                    <h3>Drop files here or click to upload.</h3>
	                                </div>
								</div>
								<br/>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_upload_file_rajal" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_upload</i> Upload File</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_lihat_file_rajal">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lihat File <span id="judul_lihat_no_sep_rajal"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<div style="float:left;width:30%;">
										<div style="margin-left:15px;" id="parent_nama_file_rajal">
										</div>
									</div>
									<div style="float:left;width:68%;">
										<div id="parent_lihat_file_rajal">
											<iframe src='' type='application/pdf' style='margin-left:15px;width:100%;'></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_pdf_rajal">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat PDF <span id="judul_buat_pdf_rajal"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_pdf_hidden">
									<input type="hidden" id="norm_buat_pdf_hidden"/>
									<input type="hidden" id="tglsep_buat_pdf_hidden"/>
									<div id="parent_buat_pdf_rajal" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_buat_pdf_rajal" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_download</i> Buat PDF</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_pdf_lab_rajal">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat PDF Laborat <span id="judul_buat_pdf_lab_rajal"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_pdf_lab_hidden">
									<input type="hidden" id="norm_buat_pdf_lab_hidden"/>
									<input type="hidden" id="tglsep_buat_pdf_lab_hidden"/>
									<div id="parent_buat_pdf_lab_rajal" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_buat_pdf_lab_rajal" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_download</i> Buat PDF</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>