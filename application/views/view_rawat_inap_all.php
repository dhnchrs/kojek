<section class="content">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>Pencarian History Pasien Rawat Inap</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control bootstrapMaterialDatePicker" data-date-format="dd/mm/yyyy" name="tanggal_pulang" id="tanggal_pulang" value="<?php echo date('d/m/Y'); ?>">
									<label class="form-label">Tanggal Pulang</label>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_cari_rawat_inap_pulang"><i class="material-icons">search</i> Cari Data</button>
						</div>
					</div>
					<div class="table-responsive">
						<div class="header" id="pemberitahuan_input">
							<span><b>SILAHKAN PILIH TANGGAL PULANG PASIEN TERLEBIH DAHULU</b></span>
						</div>
						<div id="parent_tabel_rawat_inap"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_input_sep">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Input SEP</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
	                            <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_rs" id="nomor_rs" style="color:blue;font-weight:bold;" disabled>
			                                    <label class="form-label">Nomor SEP</label>
			                                </div>
			                            </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_sep" id="nomor_sep" required>
			                                </div>
			                            </div>
                                    </div>
                                </div>
	                            <div id="parent_keterangan_no_sep">
	                            	<input type="hidden" name="nosep" id="nosep">
	                            	<input type="hidden" name="tglsep" id="tglsep">
	                            	<input type="hidden" name="nokartu" id="nokartu">
	                            	<input type="hidden" name="namapeserta" id="namapeserta">
	                            	<input type="hidden" name="peserta" id="peserta">
	                            	<input type="hidden" name="tgllahir" id="tgllahir">
	                            	<input type="hidden" name="jnskelamin" id="jnskelamin">
	                            	<input type="hidden" name="politujuan" id="politujuan">
	                            	<input type="hidden" name="jnsrawat" id="jnsrawat">
	                            	<input type="hidden" name="klsrawat" id="klsrawat">
	                            	<input type="hidden" name="diagnosaawal" id="diagnosaawal">
	                            	<input type="hidden" name="catatan" id="catatan">
	                            	<input type="hidden" name="regid" id="regid">
	                            	<input type="hidden" name="nama_pasien" id="nama_pasien">
	                            	<input type="hidden" name="idkelasstandart" id="idkelasstandart">
	                            	<input type="hidden" name="no_rujukan" id="no_rujukan">
				                    <div class="row">
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_peserta" id="nama_peserta" style="color:blue;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Peserta</label>
				                                </div>
				                            </div>
				                        </div>
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_pasien_konfirmasi" id="nama_pasien_konfirmasi" style="color:red;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Pasien (Konfirmasi SEP)</label>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="kelas_rawat" id="kelas_rawat" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Kelas Rawat</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="hak_kelas" id="hak_kelas" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Hak Kelas</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <textarea rows="3" class="form-control no-resize" name="diagnosa" id="diagnosa" style="color:blue;font-weight:bold;" readonly></textarea>
		                                    <label class="form-label">Diagnosa</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="catatan" id="catatan" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Catatan</label>
		                                </div>
		                            </div>
		                            <div class="demo-checkbox">
		                                <input type="checkbox" id="konfirmasi_sep"/>
		                                <label for="konfirmasi_sep">Apakah anda yakin data SEP tersebut benar?</label>
		                            </div>
		                            <div class="form-group form-float" id="parent_button_simpan_sep">
										<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_simpan_sep" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">save</i> Simpan</button>
		                            </div>
		                        </div>
								<!--<button type="button" class="btn bg-orange m-t-15 waves-effect" data-dismiss="modal">Back</button>
								<button type="button" class="btn bg-orange m-t-15 waves-effect" id="input_sep" onclick="input_sep()">Save</button>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_edit_sep">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Edit SEP</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row clearfix">
                                    <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_sep_belum_edit" id="nomor_sep_belum_edit" style="color:blue;font-weight:bold;" disabled>
			                                    <label class="form-label">Nomor SEP Sekarang</label>
			                                </div>
			                            </div>
                                    </div>
                                </div>
	                            <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_rs_edit" id="nomor_rs_edit" style="color:blue;font-weight:bold;" disabled>
			                                    <label class="form-label">Nomor SEP Baru</label>
			                                </div>
			                            </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nomor_sep_edit" id="nomor_sep_edit" required>
			                                </div>
			                            </div>
                                    </div>
                                </div>
	                            <div id="parent_keterangan_edit_no_sep">
	                            	<input type="hidden" name="nosep_edit" id="nosep_edit">
	                            	<input type="hidden" name="tglsep_edit" id="tglsep_edit">
	                            	<input type="hidden" name="nokartu_edit" id="nokartu_edit">
	                            	<input type="hidden" name="namapeserta_edit" id="namapeserta_edit">
	                            	<input type="hidden" name="peserta_edit" id="peserta_edit">
	                            	<input type="hidden" name="tgllahir_edit" id="tgllahir_edit">
	                            	<input type="hidden" name="jnskelamin_edit" id="jnskelamin_edit">
	                            	<input type="hidden" name="politujuan_edit" id="politujuan_edit">
	                            	<input type="hidden" name="jnsrawat_edit" id="jnsrawat_edit">
	                            	<input type="hidden" name="klsrawat_edit" id="klsrawat_edit">
	                            	<input type="hidden" name="diagnosaawal_edit" id="diagnosaawal_edit">
	                            	<input type="hidden" name="catatan_edit" id="catatan_edit">
	                            	<input type="hidden" name="regid_edit" id="regid_edit">
	                            	<input type="hidden" name="nama_pasien_edit" id="nama_pasien_edit">
	                            	<input type="hidden" name="idkelasstandart_edit" id="idkelasstandart_edit">
	                            	<input type="hidden" name="no_rujukan_edit" id="no_rujukan_edit">
				                    <div class="row">
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_peserta_edit" id="nama_peserta_edit" style="color:blue;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Peserta</label>
				                                </div>
				                            </div>
				                        </div>
				                        <div style="float:left;width: 48%;padding-left: 15px;">
				                            <div class="form-group form-float">
				                                <div class="form-line focused">
				                                    <input type="text" class="form-control" name="nama_pasien_konfirmasi_edit" id="nama_pasien_konfirmasi_edit" style="color:red;font-weight:bold;" disabled>
				                                    <label class="form-label">Nama Pasien (Konfirmasi SEP)</label>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="kelas_rawat_edit" id="kelas_rawat_edit" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Kelas Rawat</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="hak_kelas_edit" id="hak_kelas_edit" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Hak Kelas</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <textarea rows="3" class="form-control no-resize" name="diagnosa_edit" id="diagnosa_edit" style="color:blue;font-weight:bold;" readonly></textarea>
		                                    <label class="form-label">Diagnosa</label>
		                                </div>
		                            </div>
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="catatan_edit" id="catatan_edit" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Catatan</label>
		                                </div>
		                            </div>
		                            <div class="demo-checkbox">
		                                <input type="checkbox" id="konfirmasi_sep_edit"/>
		                                <label for="konfirmasi_sep_edit">Apakah anda yakin data SEP tersebut benar?</label>
		                            </div>
		                            <div class="form-group form-float" id="parent_button_edit_sep">
										<button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_edit_sep" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">edit</i> Edit</button>
		                            </div>
		                        </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_upload_file">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Upload File ke Server <span id="judul_upload_no_sep"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<input type="hidden" id="no_sep_hidden"/>							
								<input type="hidden" id="tgl_sep_hidden"/>							
						        <div class="dropzone" id="mydropzone">
						        	<div class="dz-message">
	                                    <div class="drag-icon-cph">
	                                        <i class="material-icons">touch_app</i>
	                                    </div>
	                                    <h3>Drop files here or click to upload.</h3>
	                                </div>
								</div>
								<br/>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_upload_file" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_upload</i> Upload File</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_lihat_file">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lihat File <span id="judul_lihat_no_sep"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<div style="float:left;width:30%;">
										<div style="margin-left:15px;" id="parent_nama_file">
										</div>
									</div>
									<div style="float:left;width:68%;">
										<div id="parent_lihat_file">
											<iframe src='' type='application/pdf' style='margin-left:15px;width:100%;'></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_pdf_pasien_pulang">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat Grup Ina <span id="judul_buat_pdf"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_pdf_hidden">
									<input type="hidden" id="regid_buat_pdf_hidden"/>
									<input type="hidden" id="tglsep_buat_pdf_hidden"/>
									<div id="parent_buat_pdf" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_buat_pdf" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_download</i> Buat Grup Ina</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_detail_pdf">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat Cetak By Reg <span id="judul_buat_detail_pdf"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_detail_pdf_hidden">
									<input type="hidden" id="regid_buat_detail_pdf_hidden"/>
									<input type="hidden" id="tglsep_buat_detail_pdf_hidden"/>
									<div id="parent_buat_detail_pdf" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_buat_detail_pdf" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_download</i> Buat Cetak By Reg</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_pdf_lab">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat Cetak Laborat <span id="judul_buat_pdf_lab"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_pdf_lab_hidden">
									<input type="hidden" id="regid_buat_pdf_lab_hidden"/>
									<input type="hidden" id="tglsep_buat_pdf_lab_hidden"/>
									<div id="parent_buat_pdf_lab" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_buat_pdf_lab" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">file_download</i> Buat Cetak Laborat</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_buat_pdf_iuran">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Buat Detail Iuran <span id="judul_buat_pdf_iuran"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<input type="hidden" id="nosep_buat_pdf_iuran_hidden">
									<input type="hidden" id="regid_buat_pdf_iuran_hidden"/>
									<input type="hidden" id="tglsep_buat_pdf_iuran_hidden"/>
									<div id="parent_buat_pdf_iuran" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div align="right">
							        <button type='button' class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_cetak_detail_iuran" style='margin-top: 5px;margin-left: -2px;'><i class="material-icons">print</i> Cetak Detail Iuran</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>