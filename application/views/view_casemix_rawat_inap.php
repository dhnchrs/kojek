<section class="content">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="column_bagi" style="width:45%;">
					<div class="card">
						<div class="header">
							<h2>Casemix Rawat Inap</h2>
						</div>
						<div class="body">
							<div class="table-responsive">
								<div id="parent_tabel_casemix"></div>
							</div>
						</div>
		            </div>
		        </div>
		        <div class="column_bagi" style="width:54%;">
		            <div class="card">
						<div class="header">
							<h2>Detail Pasien Rawat Inap</h2>
						</div>
						<div class="body">
							<div class="table-responsive">
								<div class="header" id="pemberitahuan_input_keterangan">
									<span><b>SILAHKAN PILIH BARIS TERLEBIH DAHULU</b></span>
								</div>
								<div class="body" id="parent_detail_tabel">
									<div class="column_bagi" style="width:49%;padding-left: 0px;">
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nosep" id="nosep" disabled>
			                                    <label class="form-label">Nomor SEP</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="tglsep" id="tglsep" disabled>
			                                    <label class="form-label">Tanggal SEP</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="nokartu" id="nokartu" disabled>
			                                    <label class="form-label">Nomor Kartu</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="namapeserta" id="namapeserta" disabled>
			                                    <label class="form-label">Nama Peserta</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="peserta" id="peserta" disabled>
			                                    <label class="form-label">Peserta</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="tgllahir" id="tgllahir" disabled>
			                                    <label class="form-label">Tanggal Lahir</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="jnskelamin" id="jnskelamin" disabled>
			                                    <label class="form-label">Jenis Kelamin</label>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="column_bagi" style="width:51%;">
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="jnsrawat" id="jnsrawat" disabled>
			                                    <label class="form-label">Jenis Rawat</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="klsrawat" id="klsrawat" disabled>
			                                    <label class="form-label">Kelas Rawat</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <textarea rows="4" class="form-control no-resize" name="diagnosaawal" id="diagnosaawal" disabled></textarea>
			                                    <label class="form-label">Diagnosa Awal</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="catatan" id="catatan" disabled>
			                                    <label class="form-label">Catatan</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="idkelasstandart" id="idkelasstandart" disabled>
			                                    <label class="form-label">Hak Kelas</label>
			                                </div>
			                            </div>
										<div class="form-group form-float">
			                                <div class="form-line focused">
			                                    <input type="text" class="form-control" name="no_rujukan" id="no_rujukan" disabled>
			                                    <label class="form-label">No Rujukan</label>
			                                </div>
			                            </div>
			                        </div>
								</div>
							</div>
							<div class="body" id="parent_tabel_history_koding_ina"></div>
							<div class="body" id="parent_input_koding_ina" style='margin-top:-30px;'>
                            	<form>
                            		<input type="hidden" id="n_reg_id_hidden">
                            		<input type="hidden" id="real_cost_hidden">
                            		<input type="hidden" id="iuran_hidden">
                            		<input type="hidden" id="hak_kelas_hidden">
                            		<input type="hidden" id="kelas_rawat_hidden">
                            		<!-- TAMBAHAN -->
                            		<input type="hidden" id="id_hak_kelas_hidden">
                            		<input type="hidden" id="id_kelas_ditempati_hidden">
                            		<input type="hidden" id="desc_hak_kelas_hidden">
                            		<input type="hidden" id="desc_kelas_ditempati_hidden">
                            		<div class="row">
                            			<div style="float:left;width: 49%;padding-left: 15px;">
			                                <label for="koding_hak_kelas">Koding Sesuai Hak Kelas (Rp.)</label>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" id="koding_hak_kelas" class="form-control" placeholder="Masukkan Nilai Koding Hak Kelas">
			                                    </div>
			                                </div>
			                            </div>
			                            <div style="float:left;width: 49%;padding-left: 15px;">
			                                <label for="koding_ditempati">Koding Kelas Ditempati (Rp.)</label>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" id="koding_ditempati" class="form-control" placeholder="Masukkan Nilai Koding Ditempati">
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="row">
		                            	<div style="float:left;width: 49%;padding-left: 15px;">
			                                <label for="real_cost">Total Real Cost</label>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" id="real_cost" class="form-control" disabled>
			                                    </div>
			                                </div>
			                            </div>
			                            <div style="float:left;width: 49%;padding-left: 15px;">
			                                <label for="iuran">Iuran</label>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" id="iuran" class="form-control" disabled>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="row">
			                            <div style="float:left;width: 49%;padding-left: 15px;">
				                            <label for="selisih">Selisih</label>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" id="selisih" class="form-control" disabled style='font-weight: bold;'>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                            <table>
		                            	<tr>
		                            		<td style='padding-right: 5px;'>
		                            			<button style='margin-top:-10px;' type="button" class="btn bg-orange m-t-15 waves-effect" id="button_simpan_koding"><i class="material-icons">save</i> Simpan</button>
		                            		</td>
		                            		<td>
		                            			<div id="parent_button_modal_ubah_kelas">
					                                <button style='margin-top:-10px;' type="button" class="btn bg-orange m-t-15 waves-effect" id="button_modal_ubah_kelas"><i class="material-icons">edit</i> Ubah Kelas</button>
					                            </div>
		                            		</td>
		                            	</tr>
		                            </table>
	                            </form>
                            </div>
						</div>
		            </div>
		        </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_ubah_kelas">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Ubah Kelas <span id="judul_ubah_kelas"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row">
									<div id="parent_header_ubah_kelas" style="padding-left: 20px;padding-right: 20px;">
									</div>
								</div>
								<div style="float:left;width: 50%;">
									<div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="hak_kelas" id="hak_kelas" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Hak Kelas</label>
		                                </div>
		                            </div>
	                            </div>
	                            <div style="float:left;width: 50%;padding-left: 15px;">
		                            <div class="form-group form-float">
		                                <div class="form-line focused">
		                                    <input type="text" class="form-control" name="kelas_ditempati" id="kelas_ditempati" style="color:blue;font-weight:bold;" disabled>
		                                    <label class="form-label">Kelas Ditempati</label>
		                                </div>
		                            </div>
	                            </div>
	                            <div class="row clearfix">
		                            <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control selectpicker show-tick" name="id_kelas_ditempati" id="id_kelas_ditempati" required>
													<option selected disabled>-- Pilih Kelas Ditempati --</option>
													<option value="11">VIP</option>
													<option value="6">Kelas I</option>
													<option value="7">Kelas II</option>
													<option value="8">Kelas III</option>
												</select>
			                                </div>
										</div>
									</div>
								</div>
	                            <div class="row clearfix">
									<div class="col-lg-12 col-md-3 col-sm-3 col-xs-6">
										<div class="form-group form-float">
			                                <div class="form-line">
												<textarea rows="2" class="form-control no-resize" name="ket_ubah_kelas" id="ket_ubah_kelas" required></textarea>
			                                    <label class="form-label">Keterangan Pindah Kelas</label>
			                                </div>
			                            </div>
		                            </div>
								</div>
								<div align="right">
							        <button type='button'  class='btn bg-orange btn-sm m-l-15 waves-effect' id="button_ubah_kelas" style='margin-top: -40px;margin-left: -2px;'><i class="material-icons">save</i> Simpan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>