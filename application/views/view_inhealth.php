<section class="content">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>Inhealth Rawat Inap</h2>
				</div>
				<div class="body">
					<div class="table-responsive">
						<div id="parent_tabel_rawat_inap_inhealth"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal_daftar_nota_valid">
	<div class="modal-dialog" style="width: 1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Tabel Daftar Nota Valid</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="table-responsive">
									<div id="parent_tabel_daftar_nota_valid"></div>
									<div id="button_daftar_nota_valid"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>