<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Kojek Rumah Sakit Dr. OEN Surakarta</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url();?>assets/images/icon.png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url();?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url();?>assets/css/themes/all-themes.css" rel="stylesheet" />
	
	<!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	
	<!-- Sweetalert Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
	
	<!-- Bootstrap Spinner Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Dropzone Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.css" rel="stylesheet">
	
	<script>
		var base_url = '<?php echo base_url(); ?>';
	</script>
	<style>
        .modal {
            overflow-y:auto;
        }
        .pilih_row{
            font-weight: bold;
            background-color: lightblue;
        }
        .column_bagi{
            padding-left: 15px;
            float: left;
        }
		.kolom_stgh{
			float: left;
			width: 50%;
		}
		#loading {
		   width: 100%;
		   height: 100%;
		   top: 0;
		   left: 0;
		   position: fixed;
		   display: block;
		   opacity: 0.7;
		   background-color: #fff;
		   z-index: 99;
		   text-align: center;
		}
		#loading-image {
		  position: absolute;
		  top: 30%;
		  left: 40%;
		  z-index: 100;
		}

        @media screen {
          #printSection {
              display: none;
          }
        }

        @media print {
          body * {
            margin: 0 !important; padding: 0 !important;
            visibility:hidden;
          }
          #printSection, #printSection * {
            visibility:visible;
          }
          #printSection {
            position:absolute;
            left:0;
            top:0;
          }
        }
	</style>
</head>

<body class="theme-orange ls-closed" style="background-color:khaki;">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div id="loading" style="display: none;">
		<img id="loading-image" src="<?php echo base_url(); ?>assets/images/preloader.gif" alt="Loading..." height="300" width="300"/>
	</div>
    <div class="overlay"></div>
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url();?>">
                  <div style="margin-top: -15px !important;">
                        <img src="<?php echo base_url();?>assets/images/logo-kojek.png" class="img-fluid">
                  </div>
                </a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url();?>assets/images/user.png" width="35" height="35" alt="User" />
                </div>
                <div class="info-container">
					<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo '<br/>'; //echo ucwords(strtolower($this->session->userdata('bagian')));?></div>
				
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('nama'); ?></div>
                    <div class="email">
						<?php if ($this->session->userdata('is_admin_it')) { ?>
						Administrator
						<?php } else { ?>
						User
						<?php } ?>
					</div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <?php if ($this->session->userdata('is_admin_it') or
                              $this->session->userdata('is_admin_casemix') or
                              $this->session->userdata('is_admin_ranap')) { ?>
                    <li <?php if(($this->uri->segment(1)=='RawatInap') or ($this->uri->segment(1)=='CasemixRawatInap') or ($this->uri->segment(1)=='rawatInapAll')): echo 'class="active"'; endif;?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">hotel</i>
                            <span>Rawat Inap</span>
                        </a>
                        <ul class="ml-menu">
                            <?php if ($this->session->userdata('is_admin_it') or
                                      $this->session->userdata('is_admin_casemix') or
                                      $this->session->userdata('is_admin_ranap')) { ?>
                            <li <?php if($this->uri->segment(1)=='RawatInap'): echo 'class="active"'; endif;?>>
                                <a href="<?php echo site_url('RawatInap');?>">
                                    <i class="material-icons">format_list_numbered</i>
                                    <span>Ruang Rawat Inap</span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($this->session->userdata('is_admin_it') or
                                      $this->session->userdata('is_admin_casemix')) { ?>
                            <li <?php if($this->uri->segment(1)=='CasemixRawatInap'): echo 'class="active"'; endif;?>>
                                <a href="<?php echo site_url('CasemixRawatInap');?>">
                                    <i class="material-icons">code</i>
                                    <span>Casemix Rawat Inap</span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($this->session->userdata('is_admin_it') or
                                      $this->session->userdata('is_admin_casemix') or
                                      $this->session->userdata('is_admin_ranap')) { ?>
                            <li <?php if($this->uri->segment(1)=='rawatInapAll'): echo 'class="active"'; endif;?>>
                                <a href="<?php echo site_url('rawatInapAll');?>">
                                    <i class="material-icons">keyboard_return</i>
                                    <span>Pasien Pulang</span>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if ($this->session->userdata('is_admin_it') or
                              $this->session->userdata('is_admin_casemix') or
                              $this->session->userdata('is_admin_rajal')) { ?>
					<li <?php if(($this->uri->segment(1)=='RawatJalan') or ($this->uri->segment(1)=='CasemixRawatJalan')): echo 'class="active"'; endif;?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">directions_walk</i>
                            <span>Rawat Jalan</span>
                        </a>
                        <ul class="ml-menu">
                            <?php if ($this->session->userdata('is_admin_it') or
                                      $this->session->userdata('is_admin_casemix') or
                                      $this->session->userdata('is_admin_rajal')) { ?>
                            <li <?php if($this->uri->segment(1)=='RawatJalan'): echo 'class="active"'; endif;?>>
                                <a href="<?php echo site_url('RawatJalan');?>">
                                    <i class="material-icons">format_list_numbered</i>
                                    <span>Poli Rawat Jalan</span>
                                </a>
                            </li>
                            <?php } ?>
                            <!--<li <?php if($this->uri->segment(1)=='CasemixRawatJalan'): echo 'class="active"'; endif;?>>
                                <a href="<?php echo site_url('CasemixRawatJalan');?>">
                                    <i class="material-icons">code</i>
                                    <span>Casemix Rawat Rajal</span>
                                </a>
                            </li>-->
                        </ul>
                    </li>
                    <?php } ?>
                    <!-- <li <?php if(($this->uri->segment(1)=='Inhealth')): echo 'class="active"'; endif;?>>
                        <a href="<?php echo site_url('Inhealth'); ?>">
                            <i class="material-icons">functions</i>
                            <span>Inhealth Rawat Inap</span>
                        </a>
                    </li> -->
					<li>
						<a href="<?php echo site_url('administrator/logout'); ?>">
							<i class="material-icons">input</i>
							<span>Sign Out</span>
						</a>
					</li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <strong>&copy; <?php echo date('Y');?> <a href="javascript:void(0);">IT Rumah Sakit Dr. Oen Surakarta</a>.</strong>
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <?php $this->load->view($content);?>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url();?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
	
	<!-- Chart Plugins Js -->
    <script src="<?php echo base_url();?>assets/plugins/chartjs/Chart.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/chartjs/chartjs-plugins/chartjs-plugin-annotation.js"></script>
	
    <!-- Custom Js -->
    <script src="<?php echo base_url();?>assets/js/admin.js"></script>
    <script src="<?php echo base_url();?>assets/js/pages/tables/jquery-datatable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/pages/forms/basic-form-elements.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/pages/ui/dialogs.js"></script>
	<script src="<?php echo base_url();?>assets/js/print.min.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url();?>assets/js/demo.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/momentjs/moment.js"></script>

	<!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	
	<!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
	
	<!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    
    <!-- Mask -->
    <script src="<?php echo base_url(); ?>assets/plugins/mask/js/jquery.mask.min.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.js"></script>
    
    <!-- numeral -->
    <script src="<?php echo base_url(); ?>assets/plugins/numeral/min/numeral.min.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ward.js?v=1.9"></script>
    <script src="<?php echo base_url(); ?>assets/js/rawatinap.js?v=1.9"></script>
    <script src="<?php echo base_url(); ?>assets/js/rawatjalan.js?v=1.9"></script>
    <script src="<?php echo base_url(); ?>assets/js/rawatInapPasienPulang.js?v=1.9"></script>
    <script src="<?php echo base_url(); ?>assets/js/inhealth.js?v=1.9"></script>
	
	<!-- Jquery Spinner Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script>
</body>

</html>
