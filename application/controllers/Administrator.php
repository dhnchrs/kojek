<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Model_user');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function validasi(){
		$username = $this->input->post('username');
        $password = $this->input->post('password');
        $login_data = $this->Model_user->validasi_user($username, $password);
        if($login_data){
        	$session_data = array(
                    'id_user' => $login_data['n_user_id'],
					'id_staff' => $login_data['n_staff_id'],
					'username' => $login_data['v_user_name'],
					'nama' => $login_data['v_user_full_name'],
                    'is_admin_it'=> $this->Model_user->is_admin_it($login_data['n_staff_id']),
                    'is_admin_casemix'=> $this->Model_user->is_admin_casemix($login_data['n_user_id']),
                    'is_admin_ranap'=> $this->Model_user->is_admin_ranap($login_data['n_user_id']),
					'is_admin_rajal'=> $this->Model_user->is_admin_rajal($login_data['n_user_id']),
					'is_login'=> TRUE
				);
        	$this->session->set_userdata($session_data);
        	$previlege=$this->Model_user->cek_previlege($login_data['n_user_id']);
        	if($previlege['status']==200){
                if ($this->Model_user->is_admin_it($login_data['n_staff_id'])) {
                    redirect('RawatJalan');
                } else if ($this->Model_user->is_admin_casemix($login_data['n_user_id'])) {
    				redirect('CasemixRawatInap');
                } else if ($this->Model_user->is_admin_ranap($login_data['n_user_id'])) {
                    redirect('RawatInap');
                } else if ($this->Model_user->is_admin_rajal($login_data['n_user_id'])) {
                    redirect('RawatJalan');
                } else {
                    redirect('Administrator');
                }
        	}else{
        		$this->session->set_flashdata('message', 'Login Gagal, Username tidak berhak mengakases Kojek.');
            	redirect('Administrator');
        	}
        }else{
        	$this->session->set_flashdata('message', 'Login Gagal, Username atau Password salah.');
            redirect('Administrator');
        }
    }

    public function logout(){
        $session_data = array(
            'id_user' => 0,
            'id_staff' => 0,
			'username' => 0,
			'nama' => 0,
            'is_admin_it'=> 0,
            'is_admin_casemix'=> 0,
            'is_admin_ranap'=> 0,
			'is_admin_rajal'=> 0,
			'is_login'=> FALSE
        );

        $this->session->sess_destroy();
        $this->session->unset_userdata($session_data);

        redirect('Administrator');
    }

    public function error(){
        $data['content'] = '404';
        $this->load->view('template',$data);
    }

}
