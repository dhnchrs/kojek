<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use GuzzleHttp\Client;
class RawatJalan extends CI_Controller
{
    var $urlVclaim = 'https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest';
    var $vclaim_conf = [
        'cons_id' => '4270',
        'secret_key' => '4fVFD7C518',
        'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
        'service_name' => 'new-vclaim-rest'
    ];
    // $nomor_sep = '1124R0051218V006750';
    // $nomor_sep = '1218V006750';

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('is_login')== FALSE){
            redirect('administrator');
        }
        $this->load->model(array('Model_rawat_jalan'));
		$this->load->helper(array('date', 'tgl_indonesia'));
        date_default_timezone_set("Asia/Jakarta");
    }
	
	public function index(){
		$data['content'] = 'view_rawat_jalan';
		$this->load->view('template',$data);
    }

    public function DMY_ke_YMD($tanggal) {
        $tanggal_array = explode("/",$tanggal);
        $hasil_tanggal = $tanggal_array[2]."-".$tanggal_array[1]."-".$tanggal_array[0];
        $hasil_tanggal_YMD = date('Y-m-d',strtotime($hasil_tanggal));
        return $hasil_tanggal_YMD;
    }

    public function cari_rawat_jalan(){
        $tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));
        $rawat_jalan_by_tanggal = $this->Model_rawat_jalan->get_rawat_jalan_by_tanggal($tanggal_awal, $tanggal_akhir);
        echo json_encode($rawat_jalan_by_tanggal);
    }

    public function cari_gabungan_poli_unit() {
        $mr_id = $this->input->post('mr_id');
        // $tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        // $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));
        $nosep=$this->input->post('no_sep');
        $gabungan_poli_unit = $this->Model_rawat_jalan->cari_gabungan_poli_unit2($mr_id,$nosep);
        echo json_encode($gabungan_poli_unit);
    }

    public function hilangkan_titik_koma($string) {
        $hasil = rtrim(str_replace('__','_',str_replace('(','',str_replace(')','',str_replace('.','',str_replace(',','',str_replace('.PDF','',str_replace('.pdf','',$string))))))),'_');
        return $hasil;
    }

    public function upload_file() {
        if ($_FILES['file_diupload']['error'] != 4) {
            $no_sep = $this->input->post('no_sep');
            $tgl_sep = str_replace('-','',$this->input->post('tgl_sep'));
            if (!file_exists('upload/'.$tgl_sep)) {
                mkdir('upload/'.$tgl_sep, 0777, true);
            }
            if (!file_exists('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ')) {
                mkdir('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ', 0777, true);
            }
            if (!file_exists('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14))) {
                mkdir('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14), 0777, true);
            }

            $config['upload_path'] = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14);
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '20480';
            //$config['file_name'] = '';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload("file_diupload")) {
                $file = $this->upload->data();
                //HILANGKAN TITIK KOMA HASIL UPLOAD
                $nama_file_baru_upload = $file['file_name'];
                $file_baru_upload = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14).'/'.$nama_file_baru_upload;
                $nama_file_hilangkan_titik_koma = $this->hilangkan_titik_koma($nama_file_baru_upload);
                $file_hilangkan_titik_koma = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14).'/'.$nama_file_hilangkan_titik_koma.'.pdf';
                if (file_exists($file_baru_upload)) {
                    rename($file_baru_upload,$file_hilangkan_titik_koma);
                }
                //HILANGKAN TITIK KOMA HASIL UPLOAD
                $cek_file = $this->Model_rawat_jalan->get_nama_file($no_sep);
                $cek_file_array = explode(';',$cek_file['file']);
                if ($cek_file['file'] != null) {
                    if (in_array($nama_file_hilangkan_titik_koma, $cek_file_array)) {
                        $data['file'] = $cek_file['file'];
                    } else {
                        $data['file'] = $cek_file['file'].';'.$this->hilangkan_titik_koma($file['file_name']);
                    }
                } else {
                    $data['file'] = $this->hilangkan_titik_koma($file['file_name']);
                }
                $this->Model_rawat_jalan->update_file($no_sep, $data);

            } else {
                echo $this->upload->display_errors();
            }
        } else {
            echo $this->upload->display_errors();
        }
    }

    public function nama_file(){
        $nosep = $this->input->post('nosep');
        $file = $this->Model_rawat_jalan->get_nama_file($nosep);
        if ($file['file'] != null) {
            $file_array = explode(';', $file['file']);
            $data = array(
                'nosep' => $file['nosep'],
                'tglsep' => $file['tglsep'],
                'file' => $file_array
            );
            echo json_encode($data);
        } else {
            echo json_encode('gagal');
        }
    }

    public function lihat_file($no_sep, $tgl_sep, $nama_file_pdf) {
        http_response_code(200);
        header('Content-Type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header('Content-Length:'.filesize('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14).'/'.$nama_file_pdf.'.pdf'));
        @readfile('upload/'.$tgl_sep.'/'.$tgl_sep.'RJ/'.substr($no_sep,14).'/'.$nama_file_pdf.'.pdf', NULL);
        exit;
    }

    public function cari_nilai_paket_buat_pdf(){
        $n_mr_id = $this->input->post('n_mr_id');
        $tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));
        $sum_real_cost = $this->input->post('sum_real_cost');
        $nosep=$this->input->post('nosep');
        $buat_pdf_tb_registration = $this->Model_rawat_jalan->get_buat_pdf_tb_registration($n_mr_id);
        $buat_pdf_tb_examination = $this->Model_rawat_jalan->get_buat_pdf_tb_examination($n_mr_id,$tanggal_awal,$tanggal_akhir,$nosep);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'tb_examination' => $buat_pdf_tb_examination,
            'total_amount' => $sum_real_cost
        );
        echo json_encode($data);
    }

    public function cari_buat_pdf_treatment_item(){
        $n_note_id = $this->input->post('n_note_id');
        $detail_nota_paket = $this->Model_rawat_jalan->get_detail_nota_paket($n_note_id);
        echo json_encode($detail_nota_paket);
        /*$buat_pdf_tb_treatment = $this->Model_rawat_jalan->get_buat_pdf_tb_treatment($n_note_id);
        $buat_pdf_tb_item = $this->Model_rawat_jalan->get_buat_pdf_tb_item($n_note_id);
        $buat_pdf_tb_drug_ingredients = $this->Model_rawat_jalan->get_buat_pdf_tb_drug_ingredients($n_note_id);
        $data = array(
            'tb_treatment' => $buat_pdf_tb_treatment,
            'tb_item' => $buat_pdf_tb_item,
            'tb_drug_ingredients' => $buat_pdf_tb_drug_ingredients
        );
        echo json_encode($data);*/
    }

    /*public function get_nama_dokter_treatment() {
        $n_doctor_id = $this->input->post('n_doctor_id');
        $nama_dokter_treatment = $this->Model_rawat_jalan->get_nama_dokter_treatment($n_doctor_id);
        $data = array(
            'nama_dokter_treatment' => $nama_dokter_treatment['v_staff_name']
        );
        echo json_encode($data);
    }*/

    public function buat_pdf() {
        $no_sep = $this->input->post('nosep');
        $tglsep = str_replace('-','',$this->input->post('tglsep'));
        $norm = $this->input->post('norm');
        $cek = $this->input->post('cek');
        if ($cek == 1) {
            $nama_file = 'CETAK_BY_REG_'.$norm;
        } else {
            $nama_file = 'REKAP_HASIL_LABORAT_'.$norm;
        }
        if (!file_exists('upload/'.$tglsep)) {
            mkdir('upload/'.$tglsep, 0777, true);
        }
        if (!file_exists('upload/'.$tglsep.'/'.$tglsep.'RJ')) {
            mkdir('upload/'.$tglsep.'/'.$tglsep.'RJ', 0777, true);
        }
        if (!file_exists('upload/'.$tglsep.'/'.$tglsep.'RJ/'.substr($no_sep,14))) {
            mkdir('upload/'.$tglsep.'/'.$tglsep.'RJ/'.substr($no_sep,14), 0777, true);
        }
        $html_buat_pdf = $this->input->post('html_buat_pdf');

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html_buat_pdf);
        $mpdf->Output('upload/'.$tglsep.'/'.$tglsep.'RJ/'.substr($no_sep,14).'/'.$nama_file.'.pdf', \Mpdf\Output\Destination::FILE);

        $cek_file = $this->Model_rawat_jalan->get_nama_file($no_sep);
        $cek_file_array = explode(';',$cek_file['file']);
        if ($cek_file['file'] != null) {
            if (in_array($nama_file, $cek_file_array)) {
                $data['file'] = $cek_file['file'];
            } else {
                $data['file'] = $cek_file['file'].';'.$nama_file;
            }
        } else {
            $data['file'] = $nama_file;
        }
        $this->Model_rawat_jalan->update_file($no_sep, $data);
    }

    //LIS
    public function get_vnoteno_by_nmrid() {
        // $n_mr_id = $this->input->post('n_mr_id');
        // $tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        // $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));
        $nosep=$this->input->post('nosep');
        $list_vnoteno = $this->Model_rawat_jalan->get_vnoteno_by_nmrid2($nosep);
        echo json_encode($list_vnoteno);
    }

    public function cari_lab_buat_pdf(){
        $n_mr_id = $this->input->post('n_mr_id');
        $nosep=$this->input->post('nosep');
        $tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));
        $buat_pdf_tb_registration = $this->Model_rawat_jalan->get_buat_pdf_tb_registration($n_mr_id);
        $list_vnoteno = $this->Model_rawat_jalan->get_vnoteno_by_nmrid2($nosep);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'list_vnoteno' => $list_vnoteno
        );
        echo json_encode($data);
    }

    public function cari_lab_buat_pdf_rekap(){
        $vnoteno = $this->input->post('vnoteno');
        $rekap_laborat_by_vnoteno = $this->Model_rawat_jalan->get_rekap_laborat_by_vnoteno($vnoteno);
        echo json_encode($rekap_laborat_by_vnoteno);
    }
    //LIS

    public function hapus_pdf() {
        $namafile = $this->input->post('namafile');
        $nosep = $this->input->post('nosep');
        $tglsep = $this->input->post('tglsep');
        if (file_exists('upload/'.$tglsep.'/'.$tglsep.'RJ/'.substr($nosep,14).'/'.$namafile.'.pdf')) {
            unlink('upload/'.$tglsep.'/'.$tglsep.'RJ/'.substr($nosep,14).'/'.$namafile.'.pdf');
        } else {
            // File not found.
        }
        $cek_file = $this->Model_rawat_jalan->get_nama_file($nosep);
        $cek_file_array = explode(';',$cek_file['file']);
        if (strpos($cek_file['file'], ';') !== false) {
            $key = array_search($namafile, $cek_file_array);
            unset($cek_file_array[$key]);
            foreach ($cek_file_array as $file_array) {
                $hasil_file_array[] = $file_array.";";
            }
            $data['file'] = rtrim(implode($hasil_file_array),";");
        } else {            
            $data['file'] = null;
            echo 'kosong';
        }
        $this->Model_rawat_jalan->update_file($nosep, $data);
    }

    //SEP GABUNG
    function get_modal_sep_gabung() {
        $n_mr_id = $this->input->post('n_mr_id');
        /*$tanggal_awal = $this->DMY_ke_YMD($this->input->post('tanggal_awal'));
        $tanggal_akhir = $this->DMY_ke_YMD($this->input->post('tanggal_akhir'));*/
        $modal_sep_gabung = $this->Model_rawat_jalan->get_modal_sep_gabung($n_mr_id);
        echo json_encode($modal_sep_gabung);
    }
    public function simpan_sep_rajal(){
        $regid_checkbox = $this->input->post('regid');
        foreach ($regid_checkbox as $regid_checkbox_value) {
            $cek_nosep = $this->Model_rawat_jalan->cek_nosep($this->input->post('nosep'));
            if (!$cek_nosep) {
                $v_tclass_code = str_replace('Kelas ', '', $this->input->post('idkelasstandart'));
                $idkelasstandart = $this->Model_rawat_jalan->get_idtclass_by_tclasscode($v_tclass_code);
                $data = array(
                    'nosep' => $this->input->post('nosep'),
                    'tglsep' => $this->input->post('tglsep'),
                    'nokartu' => $this->input->post('nokartu'),
                    'namapeserta' => $this->input->post('namapeserta'),
                    'peserta' => $this->input->post('peserta'),
                    'tgllahir' => $this->input->post('tgllahir'),
                    'jnskelamin' => $this->input->post('jnskelamin'),
                    'politujuan' => $this->input->post('politujuan'),
                    'jnsrawat' => $this->input->post('jnsrawat'),
                    'klsrawat' => $this->input->post('klsrawat'),
                    'diagnosaawal' => $this->input->post('diagnosaawal'),
                    'catatan' => $this->input->post('catatan'),
                    'regid' => $regid_checkbox_value,
                    'idkelasstandart' => $idkelasstandart['n_tclass_id'],
                    'no_rujukan' => $this->input->post('no_rujukan'),
                    'v_who_create' => $this->session->userdata('username'),
                    'd_whn_create' => date('Y-m-d H:i:s')
                );
                $this->Model_rawat_jalan->insert_sep($data);
            }

            $cek_tanggal_nosep = $this->Model_rawat_jalan->cek_tanggal_nosep($this->input->post('nosep'));
            if ($cek_tanggal_nosep['tglsep'] == $this->input->post('tglsep')) {
                $data_registration = array(
                    'v_nosepbpjs' => $this->input->post('nosep')
                );
                $this->Model_rawat_jalan->update_sep_registration($regid_checkbox_value,$data_registration);
            }
        }
        echo json_encode('sukses');
    }
    public function edit_sep_rajal(){
        $nosep_lama = $this->input->post('nosep_lama');
        
        $regid_array = $this->Model_rawat_jalan->get_reg_id_by_nosep($nosep_lama);
        $regid = $regid_array['regid'];
        $nosep_baru = $this->input->post('nosep');
        $cek_nosep = $this->Model_rawat_jalan->cek_nosep($nosep_baru);
        if (!$cek_nosep) {
            //INSERT SEP BARU
            $v_tclass_code = str_replace('Kelas ', '', $this->input->post('idkelasstandart'));
            $idkelasstandart = $this->Model_rawat_jalan->get_idtclass_by_tclasscode($v_tclass_code);
            $data_nosep_baru = array(
                'regid' => $regid,
                'nosep' => $nosep_baru,
                'tglsep' => $this->input->post('tglsep'),
                'nokartu' => $this->input->post('nokartu'),
                'namapeserta' => $this->input->post('namapeserta'),
                'peserta' => $this->input->post('peserta'),
                'tgllahir' => $this->input->post('tgllahir'),
                'jnskelamin' => $this->input->post('jnskelamin'),
                'politujuan' => $this->input->post('politujuan'),
                'jnsrawat' => $this->input->post('jnsrawat'),
                'klsrawat' => $this->input->post('klsrawat'),
                'diagnosaawal' => $this->input->post('diagnosaawal'),
                'catatan' => $this->input->post('catatan'),
                'idkelasstandart' => $idkelasstandart['n_tclass_id'],
                'no_rujukan' => $this->input->post('no_rujukan'),
                'v_who_create' => $this->session->userdata('username'),
                'd_whn_create' => date('Y-m-d H:i:s')
            );
            $this->Model_rawat_jalan->insert_sep($data_nosep_baru);
            //INSERT SEP BARU
        } else {
            //SET REGID BARU
            $data_nosep_baru = array(
                'regid' => $regid
            );
            $this->Model_rawat_jalan->update_sep_by_nosep($nosep_baru, $data_nosep_baru);
            //SET REGID BARU
        }
        $data_registration = array(
            'v_nosepbpjs' => $nosep_baru
        );
        $this->Model_rawat_jalan->update_sep_registration($regid, $data_registration);

        //SET REGID = NULL NOSEP LAMA
        $data_nosep_lama = array(
            'regid' => null
        );
        $this->Model_rawat_jalan->update_sep_by_nosep($nosep_lama, $data_nosep_lama);
        //SET REGID = NULL NOSEP LAMA
    }
    public function gabung_sep_rajal(){
        $regid_checkbox = $this->input->post('regid');
        foreach ($regid_checkbox as $regid_checkbox_value) {
            $nosep_baru = $this->input->post('nosep');
            $cek_nosep = $this->Model_rawat_jalan->cek_nosep_by_regid($regid_checkbox_value);
            $cek_nosep_by_nosep = $this->Model_rawat_jalan->cek_nosep($nosep_baru);
            if (!$cek_nosep and !$cek_nosep_by_nosep) {
                //INSERT SEP BARU
                $v_tclass_code = str_replace('Kelas ', '', $this->input->post('idkelasstandart'));
                $idkelasstandart = $this->Model_rawat_jalan->get_idtclass_by_tclasscode($v_tclass_code);
                $data_nosep_baru = array(
                    'regid' => $regid_checkbox_value,
                    'nosep' => $nosep_baru,
                    'tglsep' => $this->input->post('tglsep'),
                    'nokartu' => $this->input->post('nokartu'),
                    'namapeserta' => $this->input->post('namapeserta'),
                    'peserta' => $this->input->post('peserta'),
                    'tgllahir' => $this->input->post('tgllahir'),
                    'jnskelamin' => $this->input->post('jnskelamin'),
                    'politujuan' => $this->input->post('politujuan'),
                    'jnsrawat' => $this->input->post('jnsrawat'),
                    'klsrawat' => $this->input->post('klsrawat'),
                    'diagnosaawal' => $this->input->post('diagnosaawal'),
                    'catatan' => $this->input->post('catatan'),
                    'idkelasstandart' => $idkelasstandart['n_tclass_id'],
                    'no_rujukan' => $this->input->post('no_rujukan'),
                    'v_who_create' => $this->session->userdata('username'),
                    'd_whn_create' => date('Y-m-d H:i:s')
                );
                $this->Model_rawat_jalan->insert_sep($data_nosep_baru);
                //INSERT SEP BARU
            } else {
                //SET REGID BARU
                $data_nosep_baru = array(
                    'regid' => $regid_checkbox_value
                );
                $this->Model_rawat_jalan->update_sep_by_nosep($nosep_baru, $data_nosep_baru);
                //SET REGID BARU
            }
            $data_registration = array(
                'v_nosepbpjs' => $nosep_baru
            );
            $this->Model_rawat_jalan->update_sep_registration($regid_checkbox_value, $data_registration);
        }
        echo json_encode('sukses');
    }
    //SEP GABUNG

    //MENCEGAH ERROR KETIKA UPLOAD KARENA TGL SEP (FOLDER) = NULL
    public function cari_tglsep_by_nosep() {
        $nosep = $this->input->post('nosep');
        $tgl_sep = $this->Model_rawat_jalan->get_tglsep_by_nosep($nosep);
        echo json_encode($tgl_sep);
    }
    //MENCEGAH ERROR KETIKA UPLOAD KARENA TGL SEP (FOLDER) = NULL
}