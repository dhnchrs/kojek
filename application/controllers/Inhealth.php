<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use GuzzleHttp\Client;
class Inhealth extends CI_Controller
{

    /*var $url_inhealth = 'http://app.inhealth.co.id/pelkesws/InHealthService.asmx';
    var $token_inhealth = 'inh_0d42c80fda4d452af4b2dfcd42cc7b5f';*/
    var $url_inhealth = 'http://development.inhealth.co.id/pelkesws/InHealthService.asmx';
    var $token_inhealth = 'inh_034c5b44f58a4e8098d7fdaf23b3ba61';
    
    var $kode_provider_inhealth = '1124R004';
    var $username_inhealth = 'rs.dr.oen.surakarta';

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('is_login')== FALSE){
            redirect('administrator');
        }
        $this->load->model(array('Model_ward','Model_inhealth'));
		$this->load->helper(array('date', 'tgl_indonesia'));
        date_default_timezone_set("Asia/Jakarta");

        $this->client = new Client([
            'verify' => false
        ]);
    }
	
	public function index(){
		$data['content'] = 'view_inhealth';
		$this->load->view('template',$data);
    }

    public function cari_rawat_inap_inhealth(){
        $rawat_inap_inhealth = $this->Model_inhealth->get_rawat_inap_inhealth();
        echo json_encode($rawat_inap_inhealth);
    }

    public function cari_daftar_nota_valid(){
        $n_reg_id = $this->input->post('n_reg_id');
        $daftar_nota_valid = $this->Model_inhealth->get_daftar_nota_valid($n_reg_id);
        echo json_encode($daftar_nota_valid);
    }

    public function cari_detail_daftar_nota_valid(){
        $n_exam_id = $this->input->post('n_exam_id');
        $detail_item_trx = $this->Model_inhealth->get_detail_item_trx($n_exam_id);
        $detail_treatment_trx = $this->Model_inhealth->get_detail_treatment_trx($n_exam_id);
        $data = array(
            'detail_item_trx' => $detail_item_trx,
            'detail_treatment_trx' => $detail_treatment_trx
        );
        echo json_encode($data);
    }

    public function simpan_tindakan_inhealth(){
        $checkbox_examination = $this->input->post('checkbox_examination');
        $jenis_pelayanan = '4'; //3 itu RJTL kalau 4 itu RITL
        $nosjp = $this->input->post('nosjp');
        $idakomodasi = $this->input->post('idakomodasi');
        $tglmasukrawat = $this->input->post('tglmasukrawat');
        $poli = 'IGD';

        $url = $this->url_inhealth.'/SimpanSJP';

        foreach ($checkbox_examination as $n_exam_id) {
                $detail_item_trx = $this->Model_inhealth->get_detail_item_trx($n_exam_id);
                echo json_encode($detail_item_trx);
                if ($detail_item_trx) {
                    foreach ($detail_item_trx as $detail_item_trx_value) {                        
                        try {
                            $response = $this->client->request('GET', $url,
                            [   
                                'query' => [
                                    'token' => $this->token_inhealth,
                                    'kodeprovider' => $this->kode_provider_inhealth,
                                    'jenispelayanan' => $jenis_pelayanan,
                                    'nosjp' => $nosjp,
                                    'idakomodasi' => $idakomodasi,
                                    'tglmasukrawat' => $tglmasukrawat,
                                    'tanggalpelayanan' => $detail_item_trx_value['tanggal_pelayanan'],
                                    'kodetindakan' => $detail_item_trx_value['v_item_code'],
                                    'poli' => $poli,
                                    'kodedokter' => null,
                                    'biayaaju' => $detail_item_trx_value['n_amount_trx']
                                ]
                            ]
                            );
                            $simpan_tindakan_ritl = json_encode(simplexml_load_string($response->getBody()->getContents()));
                            $hasil_tindakan_ritl = (json_decode($simpan_tindakan_ritl,true));

                            if ($hasil_tindakan_ritl['ERRORCODE'] == '00') {
                                $data = array(
                                    'nosjp' => $hasil_tindakan_ritl['NOSJP'],
                                    'tkp' => $hasil_tindakan_ritl['TKP'],
                                    'idakomodasi' => $hasil_tindakan_ritl['IDAKOMODASI'],
                                    'tglmskrwt' => $hasil_tindakan_ritl['TGLMSKRWT'],
                                    'tglpel' => $hasil_tindakan_ritl['TGLPEL'],
                                    'jenpel' => $hasil_tindakan_ritl['JENPEL'],
                                    'namajenpel' => $hasil_tindakan_ritl['NAMAJENPEL'],
                                    'poliupf' => $hasil_tindakan_ritl['POLIUPF'],
                                    'kddokter' => $hasil_tindakan_ritl['KDDOKTER'],
                                    'bytag' => $hasil_tindakan_ritl['BYTAG'],
                                    'notes' => $hasil_tindakan_ritl['NOTES']
                                );
                                $this->Model_inhealth->simpan_tindakan_ritl($data);
                                
                                echo json_encode(array(
                                    'ERRORCODE' => $hasil_tindakan_ritl['ERRORCODE'],
                                    'ERRORDESC' => $hasil_tindakan_ritl['ERRORDESC'],
                                    'NOSJP' => $hasil_tindakan_ritl['NOSJP']
                                ));
                            } else {
                                echo json_encode(array(
                                'ERRORCODE' => $hasil_tindakan_ritl['ERRORCODE'],
                                'ERRORDESC' => $hasil_tindakan_ritl['ERRORDESC']
                                ));
                            }
                        } catch (GuzzleHttp\Exception\BadResponseException $e) {
                            $response = $e->getResponse();
                            $responseBodyAsString = $response->getBody()->getContents();
                            print_r($responseBodyAsString);
                        }
                    }
                }
                $detail_treatment_trx = $this->Model_inhealth->get_detail_treatment_trx($n_exam_id);
                if ($detail_treatment_trx) {
                    foreach ($detail_treatment_trx as $detail_treatment_trx_value) {
                        try {
                            $response = $this->client->request('GET', $url,
                            [   
                                'query' => [
                                    'token' => $this->token_inhealth,
                                    'kodeprovider' => $this->kode_provider_inhealth,
                                    'jenispelayanan' => $jenis_pelayanan,
                                    'nosjp' => $nosjp,
                                    'idakomodasi' => $idakomodasi,
                                    'tglmasukrawat' => $tglmasukrawat,
                                    'tanggalpelayanan' => $detail_treatment_trx_value['tanggal_pelayanan'],
                                    'kodetindakan' => $detail_treatment_trx_value['v_treatment_code'],
                                    'poli' => $poli,
                                    'kodedokter' => $detail_treatment_trx_value['n_doctor_id'],
                                    'biayaaju' => $detail_treatment_trx_value['n_amount_trx']
                                ]
                            ]
                            );
                            $simpan_tindakan_ritl = json_encode(simplexml_load_string($response->getBody()->getContents()));
                            $hasil_tindakan_ritl = (json_decode($simpan_tindakan_ritl,true));

                            if ($hasil_tindakan_ritl['ERRORCODE'] == '00') {
                                $data = array(
                                    'nosjp' => $hasil_tindakan_ritl['NOSJP'],
                                    'tkp' => $hasil_tindakan_ritl['TKP'],
                                    'idakomodasi' => $hasil_tindakan_ritl['IDAKOMODASI'],
                                    'tglmskrwt' => $hasil_tindakan_ritl['TGLMSKRWT'],
                                    'tglpel' => $hasil_tindakan_ritl['TGLPEL'],
                                    'jenpel' => $hasil_tindakan_ritl['JENPEL'],
                                    'namajenpel' => $hasil_tindakan_ritl['NAMAJENPEL'],
                                    'poliupf' => $hasil_tindakan_ritl['POLIUPF'],
                                    'kddokter' => $hasil_tindakan_ritl['KDDOKTER'],
                                    'bytag' => $hasil_tindakan_ritl['BYTAG'],
                                    'notes' => $hasil_tindakan_ritl['NOTES']
                                );
                                $this->Model_inhealth->simpan_tindakan_ritl($data);
                                
                                echo json_encode(array(
                                    'ERRORCODE' => $hasil_tindakan_ritl['ERRORCODE'],
                                    'ERRORDESC' => $hasil_tindakan_ritl['ERRORDESC'],
                                    'NOSJP' => $hasil_tindakan_ritl['NOSJP']
                                ));
                            } else {
                                echo json_encode(array(
                                'ERRORCODE' => $hasil_tindakan_ritl['ERRORCODE'],
                                'ERRORDESC' => $hasil_tindakan_ritl['ERRORDESC']
                                ));
                            }
                        } catch (GuzzleHttp\Exception\BadResponseException $e) {
                            $response = $e->getResponse();
                            $responseBodyAsString = $response->getBody()->getContents();
                            print_r($responseBodyAsString);
                        }
                    }
                }
        }
    }
	
}