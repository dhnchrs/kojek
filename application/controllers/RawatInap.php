<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use GuzzleHttp\Client;
class RawatInap extends CI_Controller
{
    var $urlVclaim = 'https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest';
    var $vclaim_conf = [
        'cons_id' => '4270',
        'secret_key' => '4fVFD7C518',
        'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
        'service_name' => 'new-vclaim-rest'
    ];

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('is_login')== FALSE){
            redirect('administrator');
        }
        $this->load->model(array('Model_ward','Model_rawat_inap'));
		$this->load->helper(array('date', 'tgl_indonesia'));
        date_default_timezone_set("Asia/Jakarta");
    }
	
	public function index(){
		$data['content'] = 'view_rawat_inap';
		$this->load->view('template',$data);
    }

    public function select_ward_name(){
    	$select_ward_name = $this->Model_ward->get_list_all();
        echo json_encode($select_ward_name);
    }

    public function select_ward_name_byUser(){
        $userid=$this->session->userdata('id_user');
        $select_ward_name = $this->Model_ward->get_list_all_by_user($userid);
        echo json_encode($select_ward_name);
    }

    public function cari_rawat_inap(){
        $ward_name = $this->input->post('ward_name');
        $nama_ward = $this->Model_ward->get_wardname_by_id($ward_name);
        $rawat_inap_by_ward_name = $this->Model_rawat_inap->get_rawat_inap_by_ward_name($ward_name);
        $data = array(
            'nama_ward' => $nama_ward['v_ward_name'],
            'rawat_inap' => $rawat_inap_by_ward_name
        );
        echo json_encode($data);
    }

    public function get_last_koding(){
        $n_reg_id = $this->input->post('n_reg_id');
        $last_koding = $this->Model_rawat_inap->get_last_koding($n_reg_id);
        echo json_encode($last_koding);
    }

    public function cari_sep(){
        $nomor_sep = $this->input->POST('nomor_sep');
        // $nomor_sep = '1124R0051218V006751';
        // $nomor_sep = '1124R0051218V006753';
        // $nomor_sep = '1218V006751'; SUGENG
        // $nomor_sep = '1218V006753'; WINARSIH
        $sep = new Nsulistiyawan\Bpjs\VClaim\Sep($this->vclaim_conf);
        echo json_encode($sep->cariSEP('1124R005'.$nomor_sep));
    }

    public function cari_sep_komplit(){
        $nomor_sep = $this->input->POST('nomor_sep');
        // $nomor_sep = '1124R0051218V006751';
        // $nomor_sep = '1124R0051218V006753';
        // $nomor_sep = '1218V006751'; SUGENG
        // $nomor_sep = '1218V006753'; WINARSIH
        $sep = new Nsulistiyawan\Bpjs\VClaim\Sep($this->vclaim_conf);
        echo json_encode($sep->cariSEP($nomor_sep));
    }

    public function cari_sep_durasi(){
        $nokartu = $this->input->POST('nokartu');
        // $nokartu='0001524853168';
        $tglAwal=$this->input->POST('tglAwal');
        // $tglAwal='2019-04-01';
        $tglAkhir=date('Y-m-d');
        $sep = new Nsulistiyawan\Bpjs\VClaim\Monitoring($this->vclaim_conf);
        $sepInap=$sep->historyPelayanan($nokartu, $tglAwal, $tglAkhir);
        if($sepInap['metaData']['code']==200){
            foreach ($sepInap['response']['histori'] as $data) {
                
                if($data['jnsPelayanan']==1 && $data['tglPlgSep'] == null){
                    $response=array('SEP'=>$data['noSep'],
                        'icd'=>$data['diagnosa'],'kelasRawat'=>$data['kelasRawat']);
                    echo json_encode($response);
                    // var_dump($data);
                }else{
                    $response=array('SEP'=>NULL);
                    echo json_encode($response);
                }
                # code...
            }

        }
        

    }
     public function get_noKartu() {
        $mrId = $this->input->post('mrId');
        $data= $this->Model_rawat_inap->get_noKartu($mrId);
        $nokartu =$data['v_nopesertabpjs'];
        echo json_encode($nokartu);
    }

     public function get_koding_default() {
        $icd_code = $this->input->post('icd_code');
        $data= $this->Model_rawat_inap->get_default_koding($icd_code);
        $nilai_koding =$data;
        echo json_encode($nilai_koding);
    }

    public function get_tanggal_sep() {
        $v_nosepbpjs = $this->input->post('v_nosepbpjs');
        $tanggal_sep = $this->Model_rawat_inap->get_tanggal_sep($v_nosepbpjs);
        $data = array(
            'tglsep' => $tanggal_sep['tglsep']
        );
        echo json_encode($data);
    }

    public function simpan_sep(){
        $cek_nosep = $this->Model_rawat_inap->cek_nosep($this->input->post('nosep'));
        if (!$cek_nosep) {
            $v_tclass_code = str_replace('Kelas ', '', $this->input->post('idkelasstandart'));
            $idkelasstandart = $this->Model_rawat_inap->get_idtclass_by_tclasscode($v_tclass_code);
            $data = array(
                'nosep' => $this->input->post('nosep'),
                'tglsep' => $this->input->post('tglsep'),
                'nokartu' => $this->input->post('nokartu'),
                'namapeserta' => $this->input->post('namapeserta'),
                'peserta' => $this->input->post('peserta'),
                'tgllahir' => $this->input->post('tgllahir'),
                'jnskelamin' => $this->input->post('jnskelamin'),
                //'cob' => $this->input->post('cob'),
                'politujuan' => $this->input->post('politujuan'),
                'jnsrawat' => $this->input->post('jnsrawat'),
                'klsrawat' => $this->input->post('klsrawat'),
                'diagnosaawal' => $this->input->post('diagnosaawal'),
                'catatan' => $this->input->post('catatan'),
                'regid' => $this->input->post('regid'),
                //'idtable' => $this->input->post('idtable'),
                //'tglrujukan' => $this->input->post('tglrujukan'),
                //'kodeasalfaskes' => $this->input->post('kodeasalfaskes'),
                //'asalfaskestk1' => $this->input->post('asalfaskestk1'),
                //'kddiagawal' => $this->input->post('kddiagawal'),
                'idkelasstandart' => $idkelasstandart['n_tclass_id'],
                //'status' => $this->input->post('status'),
                //'nikpeserta' => $this->input->post('nikpeserta'),
                //'biaya_tagihan' => $this->input->post('biaya_tagihan'),
                //'tgl_pulang' => $this->input->post('tgl_pulang'),
                //'tgl_batal' => $this->input->post('tgl_batal'),
                //'user_batal' => $this->input->post('user_batal'),
                //'alasanbatal' => $this->input->post('alasanbatal'),
                //'iscob' => $this->input->post('iscob'),
                //'telppeserta' => $this->input->post('telppeserta'),
                'no_rujukan' => $this->input->post('no_rujukan'),
                //'v_dpjp_kode' => $this->input->post('v_dpjp_kode'),
                //'v_no_skdp' => $this->input->post('v_no_skdp'),
                //'v_nama_ppk_perujuk' => $this->input->post('v_nama_ppk_perujuk'),
                //'dinsos' => $this->input->post('dinsos'),
                //'prolanis_prb' => $this->input->post('prolanis_prb'),
                //'no_sktm' => $this->input->post('no_sktm'),
                //'n_codding_status' => $this->input->post('n_codding_status')
                'v_who_create' => $this->session->userdata('username'),
                'd_whn_create' => date('Y-m-d H:i:s')
            );
            $this->Model_rawat_inap->insert_sep($data);

            $data_registration = array(
                'v_nosepbpjs' => $this->input->post('nosep')
            );
            $this->Model_rawat_inap->update_sep_registration($this->input->post('regid'),$data_registration);
            echo json_encode('sukses');
        } else {
            echo json_encode('gagal');
        }
    }

    public function edit_sep(){
        //0319V003308 2610227
        //0319V004083 2610136

        //SET REGID = NULL NOSEP LAMA
        $nosep_lama = $this->input->post('nosep_lama');
        $data_nosep_lama = array(
            'regid' => null
        );
        $this->Model_rawat_inap->update_sep_by_nosep($nosep_lama, $data_nosep_lama);
        //SET REGID = NULL NOSEP LAMA

        $regid = $this->input->post('regid');
        $nosep_baru = $this->input->post('nosep');
        $cek_nosep = $this->Model_rawat_inap->cek_nosep($nosep_baru);
        if (!$cek_nosep) {
            //INSERT SEP BARU
            $v_tclass_code = str_replace('Kelas ', '', $this->input->post('idkelasstandart'));
            $idkelasstandart = $this->Model_rawat_inap->get_idtclass_by_tclasscode($v_tclass_code);
            $data_nosep_baru = array(
                'regid' => $regid,
                'nosep' => $nosep_baru,
                'tglsep' => $this->input->post('tglsep'),
                'nokartu' => $this->input->post('nokartu'),
                'namapeserta' => $this->input->post('namapeserta'),
                'peserta' => $this->input->post('peserta'),
                'tgllahir' => $this->input->post('tgllahir'),
                'jnskelamin' => $this->input->post('jnskelamin'),
                'politujuan' => $this->input->post('politujuan'),
                'jnsrawat' => $this->input->post('jnsrawat'),
                'klsrawat' => $this->input->post('klsrawat'),
                'diagnosaawal' => $this->input->post('diagnosaawal'),
                'catatan' => $this->input->post('catatan'),
                'idkelasstandart' => $idkelasstandart['n_tclass_id'],
                'no_rujukan' => $this->input->post('no_rujukan'),
                'v_who_create' => $this->session->userdata('username'),
                'd_whn_create' => date('Y-m-d H:i:s')
            );
            $this->Model_rawat_inap->insert_sep($data_nosep_baru);
            //INSERT SEP BARU
        } else {
            //SET REGID BARU
            $data_nosep_baru = array(
                'regid' => $regid
            );
            $this->Model_rawat_inap->update_sep_by_nosep($nosep_baru, $data_nosep_baru);
            //SET REGID BARU
        }
        $data_registration = array(
            'v_nosepbpjs' => $nosep_baru
        );
        $this->Model_rawat_inap->update_sep_registration($regid, $data_registration);
    }

    public function hilangkan_titik_koma($string) {
        $hasil = rtrim(str_replace('__','_',str_replace('(','',str_replace(')','',str_replace('.','',str_replace(',','',str_replace('.PDF','',str_replace('.pdf','',$string))))))),'_');
        return $hasil;
    }

    public function upload_file() {
        if ($_FILES['file_diupload']['error'] != 4) {
            $no_sep = $this->input->post('no_sep');
            $tgl_sep = str_replace('-','',$this->input->post('tgl_sep'));
            if (!file_exists('upload/'.$tgl_sep)) {
                mkdir('upload/'.$tgl_sep, 0777, true);
            }
            if (!file_exists('upload/'.$tgl_sep.'/'.$tgl_sep.'RI')) {
                mkdir('upload/'.$tgl_sep.'/'.$tgl_sep.'RI', 0777, true);
            }
            if (!file_exists('upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14))) {
                mkdir('upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14), 0777, true);
            }

            $config['upload_path'] = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14);
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '20480';
            //$config['file_name'] = $no_sep;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload("file_diupload")) {
                $file = $this->upload->data();
                //HILANGKAN TITIK KOMA HASIL UPLOAD
                $nama_file_baru_upload = $file['file_name'];
                $file_baru_upload = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14).'/'.$nama_file_baru_upload;
                $nama_file_hilangkan_titik_koma = $this->hilangkan_titik_koma($nama_file_baru_upload);
                $file_hilangkan_titik_koma = 'upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14).'/'.$nama_file_hilangkan_titik_koma.'.pdf';
                if (file_exists($file_baru_upload)) {
                    rename($file_baru_upload,$file_hilangkan_titik_koma);
                }
                //HILANGKAN TITIK KOMA HASIL UPLOAD
                $cek_file = $this->Model_rawat_inap->get_nama_file($no_sep);
                $cek_file_array = explode(';',$cek_file['file']);
                if ($cek_file['file'] != null) {
                    if (in_array($nama_file_hilangkan_titik_koma, $cek_file_array)) {
                        $data['file'] = $cek_file['file'];
                    } else {
                        $data['file'] = $cek_file['file'].';'.$this->hilangkan_titik_koma($file['file_name']);
                    }
                } else {
                    $data['file'] = $this->hilangkan_titik_koma($file['file_name']);
                }
                $this->Model_rawat_inap->update_file($no_sep, $data);

            } else {
                echo $this->upload->display_errors();
            }
        } else {
            echo $this->upload->display_errors();
        }
    }

    public function nama_file(){
        $v_nosepbpjs = $this->input->post('v_nosepbpjs');
        $file = $this->Model_rawat_inap->get_nama_file($v_nosepbpjs);
        if ($file['file'] != null) {
            $file_array = explode(';', $file['file']);
            $data = array(
                'nosep' => $file['nosep'],
                'tglsep' => $file['tglsep'],
                'file' => $file_array
            );
            echo json_encode($data);
        } else {
            echo json_encode('gagal');
        }
    }

    public function lihat_file($no_sep, $tgl_sep, $nama_file_pdf) {
        //$no_sep = $this->input->post('no_sep');
        //$nama_file_pdf = $this->input->post('nama_file_pdf');
        http_response_code(200);
        header('Content-Type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header('Content-Length:'.filesize('upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14).'/'.$nama_file_pdf.'.pdf'));
        @readfile('upload/'.$tgl_sep.'/'.$tgl_sep.'RI/'.substr($no_sep,14).'/'.$nama_file_pdf.'.pdf', NULL);
        exit;
        /*$data = array(
            'nosep' => $no_sep,
            'file' => $nama_file_pdf
        );
        echo json_encode($data);*/
    }

    public function hapus_pdf() {
        $namafile = $this->input->post('namafile');
        $nosep = $this->input->post('nosep');
        $tglsep = $this->input->post('tglsep');
        if (file_exists('upload/'.$tglsep.'/'.$tglsep.'RI/'.substr($nosep,14).'/'.$namafile.'.pdf')) {
            unlink('upload/'.$tglsep.'/'.$tglsep.'RI/'.substr($nosep,14).'/'.$namafile.'.pdf');
        } else {
            // File not found.
        }
        $cek_file = $this->Model_rawat_inap->get_nama_file($nosep);
        $cek_file_array = explode(';',$cek_file['file']);
        if (strpos($cek_file['file'], ';') !== false) {
            $key = array_search($namafile, $cek_file_array);
            unset($cek_file_array[$key]);
            foreach ($cek_file_array as $file_array) {
                $hasil_file_array[] = $file_array.";";
            }
            $data['file'] = rtrim(implode($hasil_file_array),";");
        } else {            
            $data['file'] = null;
            echo 'kosong';
        }
        $this->Model_rawat_inap->update_file($nosep, $data);
    }

    //CETAK BY REG
    public function cari_nilai_paket_buat_pdf(){
        $n_reg_id = $this->input->post('n_reg_id');
        $sum_real_cost = $this->input->post('sum_real_cost');
        $buat_pdf_tb_registration = $this->Model_rawat_inap->get_buat_pdf_tb_registration($n_reg_id);
        $nota_paket = $this->Model_rawat_inap->get_nota_paket($n_reg_id);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'nota_paket' => $nota_paket,
            'sum_real_cost' => $sum_real_cost
        );
        echo json_encode($data);
    }
    //CETAK BY REG

    //PDF
    public function buat_pdf() {
        $no_sep = $this->input->post('nosep');
        $tglsep = str_replace('-','',$this->input->post('tglsep'));
        $regid = $this->input->post('regid');
        $cek = $this->input->post('cek');
        if ($cek == 1) {
            $nama_file = 'CETAK_GRUP_INA_'.$regid;
        } else if ($cek == 2) {
            $nama_file = 'CETAK_BY_REG_'.$regid;
        } else if ($cek == 3) {
            $nama_file = 'REKAP_HASIL_LABORAT_'.$regid;
        }
        if (!file_exists('upload/'.$tglsep)) {
            mkdir('upload/'.$tglsep, 0777, true);
        }
        if (!file_exists('upload/'.$tglsep.'/'.$tglsep.'RI')) {
            mkdir('upload/'.$tglsep.'/'.$tglsep.'RI', 0777, true);
        }
        if (!file_exists('upload/'.$tglsep.'/'.$tglsep.'RI/'.substr($no_sep,14))) {
            mkdir('upload/'.$tglsep.'/'.$tglsep.'RI/'.substr($no_sep,14), 0777, true);
        }
        $html_buat_pdf = $this->input->post('html_buat_pdf');

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html_buat_pdf);
        $mpdf->Output('upload/'.$tglsep.'/'.$tglsep.'RI/'.substr($no_sep,14).'/'.$nama_file.'.pdf', \Mpdf\Output\Destination::FILE);

        $cek_file = $this->Model_rawat_inap->get_nama_file($no_sep);
        $cek_file_array = explode(';',$cek_file['file']);
        if ($cek_file['file'] != null) {
            if (in_array($nama_file, $cek_file_array)) {
                $data['file'] = $cek_file['file'];
            } else {
                $data['file'] = $cek_file['file'].';'.$nama_file;
            }
        } else {
            $data['file'] = $nama_file;
        }
        $this->Model_rawat_inap->update_file($no_sep, $data);
    }
    //PDF

    //LIS
    public function get_vnoteno_by_nregid() {
        $n_reg_id = $this->input->post('n_reg_id');
        $list_vnoteno = $this->Model_rawat_inap->get_vnoteno_by_nregid($n_reg_id);
        echo json_encode($list_vnoteno);
    }

    public function cari_lab_buat_pdf(){
        $n_reg_id = $this->input->post('n_reg_id');
        $buat_pdf_tb_registration = $this->Model_rawat_inap->get_buat_pdf_tb_registration($n_reg_id);
        $list_vnoteno = $this->Model_rawat_inap->get_vnoteno_by_nregid($n_reg_id);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'list_vnoteno' => $list_vnoteno
        );
        echo json_encode($data);
    }

    public function cari_lab_buat_pdf_rekap(){
        $vnoteno = $this->input->post('vnoteno');
        $rekap_laborat_by_vnoteno = $this->Model_rawat_inap->get_rekap_laborat_by_vnoteno($vnoteno);
        echo json_encode($rekap_laborat_by_vnoteno);
    }
    //LIS

    //CETAK BY REG DETAIL
    public function get_waktu_awal_akhir(){
        $n_reg_id = $this->input->post('n_reg_id');
        $waktu_awal_akhir = $this->Model_rawat_inap->get_waktu_awal_akhir($n_reg_id);
        echo json_encode($waktu_awal_akhir);
    }
    public function cari_detail_nilai_paket_buat_pdf(){
        $n_reg_id = $this->input->post('n_reg_id');
        $sum_real_cost = $this->input->post('sum_real_cost');
        $buat_pdf_tb_registration = $this->Model_rawat_inap->get_buat_pdf_tb_registration($n_reg_id);
        $fungsi_kasur_detail = $this->Model_rawat_inap->get_fungsi_kasur_detail($n_reg_id);
        $cetak_byreg_kojek = $this->Model_rawat_inap->get_cetak_byreg_kojek($n_reg_id);
        $sum_fungsi_kasur_detail = $this->Model_rawat_inap->sum_fungsi_kasur_detail($n_reg_id);
        $sum_cetak_byreg_kojek = $this->Model_rawat_inap->sum_cetak_byreg_kojek($n_reg_id);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'kasur_detail' => $fungsi_kasur_detail,
            'cetak_byreg' => $cetak_byreg_kojek,
            'sum_fungsi_kasur_detail' => $sum_fungsi_kasur_detail['sum_total'],
            'sum_cetak_byreg_kojek' => $sum_cetak_byreg_kojek['sum_total'],
            'sum_real_cost' => $sum_real_cost
        );
        echo json_encode($data);
    }
    public function get_cetak_byreg_kojek_by_kelompok(){
        $n_reg_id = $this->input->post('n_reg_id');
        $kelompok = $this->input->post('kelompok');
        $cetak_byreg_kojek_by_kelompok = $this->Model_rawat_inap->get_cetak_byreg_kojek_by_kelompok($n_reg_id,$kelompok);
        echo json_encode($cetak_byreg_kojek_by_kelompok);
    }
    //CETAK BY REG DETAIL

    //IURAN
    public function cari_iuran_buat_pdf(){
        $n_reg_id = $this->input->post('n_reg_id');
        $buat_pdf_tb_registration = $this->Model_rawat_inap->get_buat_pdf_tb_registration($n_reg_id);
        $iuran = $this->Model_rawat_inap->get_iuran($n_reg_id);
        $data = array(
            'tb_registration' => $buat_pdf_tb_registration,
            'iuran' => $iuran
        );
        echo json_encode($data);
    }
    public function cetak_detail_iuran() {
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4',
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => 5,
            'margin_bottom' => 5,
        ]);

        $regid = $this->input->post('regid');
        $html = $this->input->post('html');
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);

        $path = 'cetak/DETAIL_IURAN_'.$regid.'.pdf';
        $mpdf->Output($path, 'F');
        
        echo json_encode(array("code" => 200, "response" => base_url().$path));
    }
    //IURAN
	
}