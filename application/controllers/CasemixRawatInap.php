<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CasemixRawatInap extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('is_login')== FALSE){
            redirect('administrator');
        }
        $this->load->model(array('Model_ward','Model_casemix_rawat_inap','Model_rawat_inap'));
		$this->load->helper(array('date', 'tgl_indonesia'));
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index(){
        $data['content'] = 'view_casemix_rawat_inap';
        $this->load->view('template',$data);
    }

    public function cari_casemix(){
        $casemix_by_ward_name = $this->Model_casemix_rawat_inap->get_casemix_by_ward_name();
        echo json_encode($casemix_by_ward_name);
    }

    public function update_codding_status(){
        $n_reg_id = $this->input->post('n_reg_id');
        $data = array(
            'n_codding_status' => 1
        );
        $this->Model_casemix_rawat_inap->update_codding_status($n_reg_id, $data);
    }

    public function cari_sep_by_nosep(){
        $regid = $this->input->post('regid');
        $nosep = $this->input->post('nosep');
        $sep_by_nosep = $this->Model_casemix_rawat_inap->get_sep_by_nosep($regid, $nosep);
        $real_cost = $this->Model_casemix_rawat_inap->get_real_cost($regid);
        $data = array(
            'sep' => $sep_by_nosep,
            'real_cost' => $real_cost
        );
        echo json_encode($data);
    }

    public function cari_history_koding_ina_by_regid(){
        $n_reg_id = $this->input->post('n_reg_id');
        $history_koding_ina_by_regid = $this->Model_casemix_rawat_inap->get_history_koding_ina_by_regid($n_reg_id);
        echo json_encode($history_koding_ina_by_regid);
    }

    public function simpan_koding(){
        $n_reg_id = $this->input->post('n_reg_id');
        $koding_hak_kelas = $this->input->post('koding_hak_kelas');
        $koding_ditempati = $this->input->post('koding_ditempati');
        $real_cost = $this->input->post('real_cost');
        $iur_max = $this->input->post('iur_max');
        $id_koding_hak_kelas = $this->input->post('id_koding_hak_kelas');
        $id_koding_ditempati = $this->input->post('id_koding_ditempati');
        $data = array(
            'n_reg_id' => $n_reg_id,
            'n_codding_hak' => $koding_hak_kelas,
            'n_codding_ditempati' => $koding_ditempati,
            'n_real_cost' => $real_cost,
            'n_iur_max' => $iur_max,
            'd_whn_create' => date('Y-m-d H:i:s'),
            'v_who_create' => $this->session->userdata('id_staff'),
            'id_kelas_hak' => $id_koding_hak_kelas,
            'id_kelas_ditempati' => $id_koding_ditempati
        );
        $this->Model_casemix_rawat_inap->insert_koding($data);
        $data_sep = array(
            'n_codding_status' => 2
        );
        $this->Model_rawat_inap->update_sep($n_reg_id,$data_sep);
    }

    //HEADER UBAH KELAS
    public function cari_header_ubah_kelas(){
        $n_reg_id = $this->input->post('n_reg_id');
        $header_ubah_kelas = $this->Model_rawat_inap->get_buat_pdf_tb_registration($n_reg_id);
        $data = array(
            'tb_registration' => $header_ubah_kelas
        );
        echo json_encode($data);
    }
    public function ubah_kelas(){
        $n_reg_id = $this->input->post('n_reg_id');
        $id_kelas_ditempati_lama = $this->input->post('id_kelas_ditempati_lama');
        $id_kelas_ditempati_baru = $this->input->post('id_kelas_ditempati_baru');
        $ket_ubah_kelas = $this->input->post('ket_ubah_kelas');
        $data_tb_class_adjustment = array(
            'n_reg_id' => $n_reg_id,
            'd_whn_create' => date('Y-m-d H:i:s'),
            'v_who_create' => $this->session->userdata('id_staff'),
            'n_tclass_id_lama' => $id_kelas_ditempati_lama,
            'n_tclass_id_baru' => $id_kelas_ditempati_baru,
            'v_notes' => $ket_ubah_kelas
        );
        $this->Model_casemix_rawat_inap->insert_tb_class_adjustment($data_tb_class_adjustment);

        $data_sep = array(
            'n_tclass_ditempati' => $id_kelas_ditempati_baru,
            'is_ubah_kelas' => 1
        );
        $this->Model_rawat_inap->update_sep($n_reg_id, $data_sep);
    }
    //HEADER UBAH KELAS
}