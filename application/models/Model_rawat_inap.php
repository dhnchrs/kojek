<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_rawat_inap extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function get_rawat_inap_by_ward_name($n_ward_id){
		$query = $this->db->query("
                SELECT distinct tb_registration.n_reg_id,
                 DATE (
                 tb_registration.d_registration_date
                ),
                 tb_sepbpjs.tglsep,
                 v_bed_desc,
                 v_bed_code,
                 tb_medical_record.v_mr_code,
                 ms_patient.v_patient_name,
                 tb_registration.v_nosepbpjs,
                 SUM (
                 tb_examination.n_total_amount
                ) AS sum_real_cost,
                 n_codding_status,
                 case
                 WHEN n_codding_status=1 THEN 'Minta Koding'
                 WHEN n_codding_status=2 THEN 'Sudah Koding'
                 ELSE 'Belum Meminta'
                 END as status
                FROM
                 ms_bed
                JOIN ms_room ON ms_room.n_room_id = ms_bed.n_room_id
                JOIN ms_ward ON ms_ward.n_ward_id = ms_room.n_ward_id
                AND ms_ward.n_ward_id = ".$n_ward_id."
                JOIN tb_registration ON tb_registration.n_reg_id = ms_bed.n_reg_id and (tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519)
                JOIN tb_medical_record ON tb_medical_record.n_mr_id = tb_registration.n_mr_id
                JOIN ms_patient ON ms_patient.n_patient_id = tb_medical_record.n_patient_id
                JOIN tb_examination ON tb_examination.n_reg_id = tb_registration.n_reg_id
                AND n_exam_status = 2
                LEFT JOIN tb_codding_ina ON tb_codding_ina.n_reg_id = tb_registration.n_reg_id
                LEFT JOIN tb_sepbpjs on tb_sepbpjs.regid=tb_registration.n_reg_id and tb_sepbpjs.status = 1
                GROUP BY
                 tb_registration.n_reg_id,
                 DATE (
                  tb_registration.d_registration_date
                 ),
                 tb_sepbpjs.tglsep,
                 v_bed_desc,
                 v_bed_code,
                 tb_medical_record.v_mr_code,
                 ms_patient.v_patient_name,
                 tb_registration.v_nosepbpjs,
                 tb_codding_ina.n_codding_hak,
                 tb_codding_ina.n_codding_ditempati,
                 tb_codding_ina.d_whn_create,n_codding_status
                ORDER BY
                 v_bed_code");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_rawat_inap_by_tgl_pulang($tgl_pulang){
        $query = $this->db->query("
                SELECT distinct tb_registration.n_reg_id,
                 DATE (
                 tb_registration.d_registration_date
                )as reg_date,date_part('day',d_check_out_time-d_registration_date)+1as los,
                 tb_sepbpjs.tglsep,
                 v_bed_desc,
                 v_bed_code,
                 tb_medical_record.v_mr_code,
                 ms_patient.v_patient_name,
                 tb_registration.v_nosepbpjs,
                 SUM (
                 tb_examination.n_total_amount
                ) AS sum_real_cost,
                 n_codding_status,
                 case
                 WHEN n_codding_status=1 THEN 'Minta Koding'
                 WHEN n_codding_status=2 THEN 'Sudah Koding'
                 ELSE 'Belum Meminta'
                 END as status
                FROM
                 tb_registration
                JOIN tb_medical_record ON tb_medical_record.n_mr_id = tb_registration.n_mr_id
                JOIN ms_patient ON ms_patient.n_patient_id = tb_medical_record.n_patient_id
                JOIN tb_examination ON tb_examination.n_reg_id = tb_registration.n_reg_id AND n_exam_status = 2
                join tb_bed_occupancy on tb_bed_occupancy.n_reg_primary_id=tb_registration.n_reg_id and tb_bed_occupancy.v_out_note='K' 
                    and date(tb_bed_occupancy.d_check_out_time) = '$tgl_pulang'
                join ms_bed on ms_bed.n_bed_id=tb_bed_occupancy.n_bed_primary_id
                JOIN ms_room ON ms_room.n_room_id = ms_bed.n_room_id
                JOIN ms_ward ON ms_ward.n_ward_id = ms_room.n_ward_id
                LEFT JOIN tb_codding_ina ON tb_codding_ina.n_reg_id = tb_registration.n_reg_id
                LEFT JOIN tb_sepbpjs on tb_sepbpjs.regid=tb_registration.n_reg_id and tb_sepbpjs.status = 1
                where tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519 
                GROUP by tb_registration.n_reg_id,
                 reg_date,los,
                 tb_sepbpjs.tglsep,
                 v_bed_desc,
                 v_bed_code,
                 tb_medical_record.v_mr_code,
                 ms_patient.v_patient_name,
                 tb_registration.v_nosepbpjs,
                 tb_codding_ina.n_codding_hak,
                 tb_codding_ina.n_codding_ditempati,
                 tb_codding_ina.d_whn_create,n_codding_status
                ORDER BY
                 v_bed_code");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function insert_sep($data){
        $this->db->insert('tb_sepbpjs', $data);
    }

    function cek_nosep($nosep) {
        $query = $this->db->query("SELECT nosep from tb_sepbpjs where nosep = '".$nosep."'");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function update_sep($regid, $data){
        $this->db->where('regid', $regid);
        $this->db->update('tb_sepbpjs', $data);
    }

    function update_sep_by_nosep($nosep, $data){
        $this->db->where('nosep', $nosep);
        $this->db->update('tb_sepbpjs', $data);
    }

    function update_sep_registration($n_reg_id, $data){
        $this->db->where('n_reg_id', $n_reg_id);
        $this->db->update('tb_registration', $data);
    }

    function get_idtclass_by_tclasscode($v_tclass_code){
        $query = $this->db->query("SELECT n_tclass_id from ms_treatment_class where v_tclass_code=".$v_tclass_code);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function update_file($v_nosepbpjs, $data){
        $this->db->where('nosep', $v_nosepbpjs);
        $this->db->update('tb_sepbpjs', $data);
    }

    function get_nama_file($v_nosepbpjs) {
        $this->db->select('nosep,tglsep,file');
        $this->db->where('nosep', $v_nosepbpjs);
        $query = $this->db->get('tb_sepbpjs');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_last_koding($n_reg_id) {
        $query = $this->db->query("SELECT id_codding,date(d_whn_create) as tanggal_koding,n_codding_ditempati,n_codding_hak
            from tb_codding_ina
            where n_reg_id=".$n_reg_id."
            order by id_codding desc limit 1");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_tanggal_sep($v_nosepbpjs) {
        $this->db->select('tglsep');
        $this->db->where('nosep', $v_nosepbpjs);
        $query = $this->db->get('tb_sepbpjs');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    //CETAK BY REG
    function get_buat_pdf_tb_registration($n_reg_id) {
        $cek = $this->db->query("SELECT is_ubah_kelas from tb_sepbpjs where regid = ".$n_reg_id);
        $cek_row_array = $cek->row_array();
        if ($cek_row_array['is_ubah_kelas'] == 1) {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join ms_bed on ms_bed.n_reg_id = tb_registration.n_reg_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = tb_sepbpjs.n_tclass_ditempati
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id);
        } else {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join ms_bed on ms_bed.n_reg_id = tb_registration.n_reg_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = ms_bed.n_tclass_id
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id); //v_insurance_name,v_reg_secondary_id
        }
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    //CETAK BY REG
    function get_buat_pdf_tb_registration_pasien_pulang($n_reg_id) {
        $cek = $this->db->query("SELECT is_ubah_kelas from tb_sepbpjs where regid = ".$n_reg_id);
        $cek_row_array = $cek->row_array();
        if ($cek_row_array['is_ubah_kelas'] == 1) {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join tb_bed_occupancy on tb_bed_occupancy.n_reg_primary_id=tb_registration.n_reg_id
                join ms_bed on ms_bed.n_bed_id = tb_bed_occupancy.n_bed_primary_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = tb_sepbpjs.n_tclass_ditempati
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id);
        } else {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join tb_bed_occupancy on tb_bed_occupancy.n_reg_primary_id=tb_registration.n_reg_id
                join ms_bed on ms_bed.n_bed_id = tb_bed_occupancy.n_bed_primary_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = ms_bed.n_tclass_id
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id); //v_insurance_name,v_reg_secondary_id
        }
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }


    function get_nota_paket($n_reg_id) {
        $query = $this->db->query("SELECT * from report.rekap_inacbg_fungsi($n_reg_id)");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    //CETAK BY REG

 //CETAK BY REG
    function get_buat_pdf_tb_reg_pasien_pulang($n_reg_id) {
        $cek = $this->db->query("SELECT is_ubah_kelas from tb_sepbpjs where regid = ".$n_reg_id);
        $cek_row_array = $cek->row_array();
        if ($cek_row_array['is_ubah_kelas'] == 1) {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join tb_bed_occupancy on tb_bed_occupancy.n_reg_primary_id=tb_registration.n_reg_id and v_out_note='K'
                join ms_bed on ms_bed.n_bed_id=tb_bed_occupancy.n_bed_primary_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = tb_sepbpjs.n_tclass_ditempati
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id);
        } else {
            $query = $this->db->query("SELECT distinct v_insurance_name,v_reg_secondary_id,v_mr_code,v_patient_name,v_patient_main_addr,v_bed_desc,
                     tb_kelas_ditempati.v_tclass_desc as kelas_ditempati,
                     tb_hak_kelas.v_tclass_desc as hak_kelas
                from tb_registration
                join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
                join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
                join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
                join tb_bed_occupancy on tb_bed_occupancy.n_reg_primary_id=tb_registration.n_reg_id and v_out_note='K'
                join ms_bed on ms_bed.n_bed_id=tb_bed_occupancy.n_bed_primary_id
                join tb_sepbpjs on tb_sepbpjs.regid = tb_registration.n_reg_id
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = ms_bed.n_tclass_id
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where tb_registration.n_reg_id = ".$n_reg_id); //v_insurance_name,v_reg_secondary_id
        }
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    
    //LIS
    function get_vnoteno_by_nregid($n_reg_id) {
        $query = $this->db->query("SELECT
            v_note_no
            from tb_examination
            join tb_registration on tb_registration.n_reg_id=tb_examination.n_reg_id
            where tb_registration.n_reg_id=".$n_reg_id."
            and n_exam_status=2
            and n_payment_status<>1
            and (tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519)
            and reg_status<>3
            and ((tb_examination.n_unit_id in (56,160,55) and n_jenisbayar=0) or tb_examination.n_unit_id not in (56,160,55))
            and v_note_no like 'I-LAB%'"); //JANGAN UBAH
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function get_rekap_laborat_by_vnoteno($v_note_no) {
        $lis = $this->load->database('lis', TRUE);
        $query = $lis->query("SELECT DISTINCT his_reg_no,test_name,result||test_flag_sign as hasil, 
                reference_value as nilai_normal,test_units_name,seq_group,seq
                from result_data 
                where his_reg_no = '".$v_note_no."' order by his_reg_no,seq_group,seq");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    //LIS

    //CETAK BY REG DETAIL
    function get_waktu_awal_akhir($n_reg_id) {
        $query = $this->db->query("
            SELECT
            date(d_registration_date) as waktu_awal,
            case
            when reg_status = 1 then CURRENT_DATE
            when reg_status <> 1 then (
                SELECT date(d_check_out_time)
                from tb_bed_occupancy
                where n_reg_primary_id=".$n_reg_id."
                and v_out_note = 'K'
            )
            end as waktu_akhir
            from tb_registration
            where n_reg_id=".$n_reg_id);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    function get_fungsi_kasur_detail($n_reg_id) {
        $query = $this->db->query("SELECT * from report.fungsi_kasur_detail($n_reg_id)");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_cetak_byreg_kojek($n_reg_id) {
        $query = $this->db->query("
                SELECT * from (
                    SELECT distinct on (kelompok) kelompok,nomor,sum(subttl) as sum_total
                    from kasir.rinci($n_reg_id)
                    group by kelompok,nomor
                ) t
                order by t.nomor");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function get_cetak_byreg_kojek_by_kelompok($n_reg_id,$kelompok) {
        $query = $this->db->query("SELECT * from kasir.rinci($n_reg_id) where kelompok = '$kelompok' order by nomor");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function sum_fungsi_kasur_detail($n_reg_id) {
        $query = $this->db->query("SELECT sum(total) as sum_total from report.fungsi_kasur_detail($n_reg_id)");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    function sum_cetak_byreg_kojek($n_reg_id) {
        $query = $this->db->query("SELECT sum(subttl) as sum_total from kasir.rinci($n_reg_id)");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    //CETAK BY REG DETAIL

    //IURAN
    function get_iuran($n_reg_id) {
        $query = $this->db->query("SELECT n_codding_hak,n_codding_ditempati,n_real_cost,n_iur_max
            from tb_codding_ina
            where n_reg_id=".$n_reg_id."
            order by id_codding desc limit 1");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    //IURAN

    function get_noKartu($mr_id) {
        $query = $this->db->query("SELECT d_registration_date,v_nopesertabpjs from tb_registration where n_mr_id=$mr_id and v_nopesertabpjs is not null order by d_registration_date desc limit 1");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_default_koding($icd){
        $query = $this->db->query("select nilai_koding_i,nilai_koding_ii,nilai_koding_iii from tb_default_nilai_ina where v_icdx='$icd'");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

}