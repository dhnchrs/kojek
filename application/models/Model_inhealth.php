<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_inhealth extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function get_rawat_inap_inhealth(){
		$query = $this->db->query("
                SELECT distinct 
                 tb_registration.n_reg_id,
                 tb_medical_record.v_mr_code,
                 ms_patient.v_patient_name,
                 ms_ward.v_ward_name,
                 v_bed_desc,
                 tb_sjp_inhealth.no_sjp,
                 tb_sjp_inhealth.kelas,
                 tb_sjp_inhealth.kelas_desc,
                 v_tclass_desc,
                 v_bed_code,
                 n_bed_id,
                 date(d_registration_date) as tglmasukrawat
                FROM
                 ms_bed
                JOIN ms_room ON ms_room.n_room_id = ms_bed.n_room_id
                JOIN ms_ward ON ms_ward.n_ward_id = ms_room.n_ward_id
                JOIN tb_registration ON tb_registration.n_reg_id = ms_bed.n_reg_id AND (tb_registration.n_insurance_id=485)
                JOIN tb_medical_record ON tb_medical_record.n_mr_id = tb_registration.n_mr_id
                JOIN ms_patient ON ms_patient.n_patient_id = tb_medical_record.n_patient_id
                JOIN tb_examination ON tb_examination.n_reg_id = tb_registration.n_reg_id AND n_exam_status = 2
                JOIN ms_treatment_class ON ms_treatment_class.n_tclass_id = ms_bed.n_tclass_id
                LEFT JOIN tb_codding_ina ON tb_codding_ina.n_reg_id = tb_registration.n_reg_id
                LEFT JOIN tb_sjp_inhealth ON tb_sjp_inhealth.n_reg_id = tb_registration.n_reg_id
                ORDER BY
                 v_bed_code");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_daftar_nota_valid($n_reg_id){
        $query = $this->db->query("SELECT n_status_inhealth,n_exam_id,v_note_no,n_total_amount from tb_examination where n_reg_id = $n_reg_id order by d_whn_create desc");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_detail_item_trx($n_note_id){
        $query = $this->db->query("SELECT v_item_name,date(d_whn_create) as tanggal_pelayanan,v_item_code,n_amount_trx
            from tb_item_trx
            where n_note_id = $n_note_id");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_detail_treatment_trx($n_note_id){
        $query = $this->db->query("SELECT v_treatment_name,date(d_whn_create) as tanggal_pelayanan,v_treatment_code,n_doctor_id,n_amount_trx
            from tb_treatment_trx
            where n_note_id = $n_note_id");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function simpan_tindakan_ritl($data) {
        $this->db->insert('tb_tindakan_ritl_inhealth', $data);
    }

}