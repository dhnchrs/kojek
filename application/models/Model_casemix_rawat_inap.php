<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_casemix_rawat_inap extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function get_casemix_by_ward_name(){
		$query = $this->db->query("SELECT tb_registration.n_reg_id,tb_medical_record.v_mr_code,tb_registration.v_nosepbpjs,v_ward_name,v_bed_desc
            from ms_bed 
            join ms_room on ms_room.n_room_id=ms_bed.n_room_id
            join ms_ward on ms_ward.n_ward_id=ms_room.n_ward_id
            join tb_registration on tb_registration.n_reg_id=ms_bed.n_reg_id
            join tb_sepbpjs on tb_sepbpjs.regid=tb_registration.n_reg_id and tb_sepbpjs.n_codding_status= 1
            join tb_medical_record on tb_medical_record.n_mr_id=tb_registration.n_mr_id
            order by v_bed_code");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_sep_by_nosep($regid,$no_sep){
        $cek = $this->db->query("SELECT is_ubah_kelas from tb_sepbpjs where regid = ".$regid);
        $cek_row_array = $cek->row_array();
        if ($cek_row_array['is_ubah_kelas'] == 1) {
            $query = $this->db->query("SELECT nosep,tglsep,nokartu,namapeserta,peserta,tgllahir,jnskelamin,politujuan,jnsrawat,klsrawat,diagnosaawal,catatan,regid,idkelasstandart,no_rujukan,n_tclass_ditempati as hasil_ubah_kelas_ditempati,
                    tb_kelas_ditempati.n_tclass_id as id_kelas_ditempati,
                    tb_hak_kelas.n_tclass_id as id_hak_kelas,
                    tb_kelas_ditempati.v_tclass_desc as desc_kelas_ditempati,
                    tb_hak_kelas.v_tclass_desc as desc_hak_kelas,
                    tb_hasil_ubah.v_tclass_desc as hasil_ubah_kelas_ditempati
                from tb_sepbpjs
                join ms_treatment_class on ms_treatment_class.n_tclass_id=tb_sepbpjs.idkelasstandart
                join ms_bed on ms_bed.n_reg_id = tb_sepbpjs.regid
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = ms_bed.n_tclass_id
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                join ms_treatment_class as tb_hasil_ubah on tb_hasil_ubah.n_tclass_id = tb_sepbpjs.n_tclass_ditempati
                where nosep = '".$no_sep."'");
        } else {
            $query = $this->db->query("SELECT nosep,tglsep,nokartu,namapeserta,peserta,tgllahir,jnskelamin,politujuan,jnsrawat,klsrawat,diagnosaawal,catatan,regid,idkelasstandart,no_rujukan,n_tclass_ditempati as hasil_ubah_kelas_ditempati,
                    tb_kelas_ditempati.n_tclass_id as id_kelas_ditempati,
                    tb_hak_kelas.n_tclass_id as id_hak_kelas,
                    tb_kelas_ditempati.v_tclass_desc as desc_kelas_ditempati,
                    tb_hak_kelas.v_tclass_desc as desc_hak_kelas
                from tb_sepbpjs
                join ms_treatment_class on ms_treatment_class.n_tclass_id=tb_sepbpjs.idkelasstandart
                join ms_bed on ms_bed.n_reg_id = tb_sepbpjs.regid
                join ms_treatment_class as tb_kelas_ditempati on tb_kelas_ditempati.n_tclass_id = ms_bed.n_tclass_id
                join ms_treatment_class as tb_hak_kelas on tb_hak_kelas.n_tclass_id = tb_sepbpjs.idkelasstandart
                where nosep = '".$no_sep."'");
        }
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_history_koding_ina_by_regid($n_reg_id){
        $this->db->select('v_staff_name,tb_codding_ina.d_whn_create,tb_codding_ina.v_who_create,n_codding_hak,n_codding_ditempati,n_real_cost,n_iur_max');
        $this->db->join('ms_staff', 'ms_staff.n_staff_id=tb_codding_ina.v_who_create');
        $this->db->where('n_reg_id', $n_reg_id);
        $this->db->order_by('tb_codding_ina.d_whn_create', 'desc');
        $query = $this->db->get('tb_codding_ina');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function update_codding_status($regid, $data){
        $this->db->where('regid', $regid);
        $this->db->update('tb_sepbpjs', $data);
    }

    function get_real_cost($n_reg_id) {
        $query = $this->db->query("SELECT sum(tb_examination.n_total_amount) as sum_real_cost from tb_registration
            join tb_examination on tb_examination.n_reg_id=tb_registration.n_reg_id and n_exam_status=2
            where tb_registration.n_reg_id = ".$n_reg_id);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function insert_koding($data){
        $this->db->insert('tb_codding_ina', $data);
    }

    //UBAH KELAS
    function insert_tb_class_adjustment($data){
        $this->db->insert('tb_class_adjustment', $data);
    }
    //UBAH KELAS

}