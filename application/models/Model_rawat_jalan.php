<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_rawat_jalan extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function get_rawat_jalan_by_tanggal($tanggal_awal, $tanggal_akhir){
		$query = $this->db->query("SELECT tb_sepbpjs.tglsep,tb_registration.n_mr_id,ms_patient.v_patient_name,tb_sepbpjs.diagnosaawal,v_nosepbpjs,sum(tb_examination.n_total_amount) as sum_real_cost
            from tb_sepbpjs
            join tb_registration on tb_registration.v_nosepbpjs=tb_sepbpjs.nosep and SUBSTRING ( v_reg_secondary_id ,1 , 1 )='J'
            left join tb_examination on tb_examination.n_reg_id=tb_registration.n_reg_id and tb_examination.n_exam_status=2 and n_payment_status<>1 and ((tb_examination.n_unit_id in (56,160,55) and n_jenisbayar=0) or tb_examination.n_unit_id not in (56,160,55))
            join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
            join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
            where reg_status<>3
            and tb_sepbpjs.tglsep = '$tanggal_awal'and tb_sepbpjs.status = 1
            group by tb_sepbpjs.tglsep,tb_registration.n_mr_id,ms_patient.v_patient_name,diagnosaawal,v_nosepbpjs
            ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function cari_gabungan_poli_unit($mr_id,$tanggal_awal,$tanggal_akhir,$nosep){
        $query = $this->db->query("
            SELECT v_reg_secondary_id, v_unit_code, v_unit_name, v_staff_name, n_reg_id
            from tb_registration 
            join ms_unit on ms_unit.n_unit_id=tb_registration.n_unit_id
            left join ms_staff on ms_staff.n_staff_id=tb_registration.n_staff_id
            join tb_sepbpjs on tb_registration.v_nosepbpjs=tb_sepbpjs.nosep and v_nosepbpjs='$nosep'
            where n_mr_id=$mr_id and reg_status<>3");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function cari_gabungan_poli_unit2($mr_id,$nosep){
        $query = $this->db->query("
            SELECT v_reg_secondary_id, v_unit_code, v_unit_name, v_staff_name, n_reg_id
            from tb_registration 
            join ms_unit on ms_unit.n_unit_id=tb_registration.n_unit_id
            left join ms_staff on ms_staff.n_staff_id=tb_registration.n_staff_id
            join tb_sepbpjs on tb_registration.v_nosepbpjs=tb_sepbpjs.nosep and v_nosepbpjs='$nosep'
            where n_mr_id=$mr_id and reg_status<>3");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function update_file($v_nosepbpjs, $data){
        $this->db->where('nosep', $v_nosepbpjs);
        $this->db->update('tb_sepbpjs', $data);
    }

    function get_nama_file($v_nosepbpjs) {
        $this->db->select('nosep,tglsep,file');
        $this->db->where('nosep', $v_nosepbpjs);
        $query = $this->db->get('tb_sepbpjs');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_buat_pdf_tb_registration($n_mr_id) {
        $query = $this->db->query("SELECT distinct v_mr_code,v_patient_name,v_patient_main_addr from tb_registration
            join ms_insurance on ms_insurance.n_insurance_id = tb_registration.n_insurance_id
            join tb_medical_record on tb_medical_record.n_mr_id = tb_registration.n_mr_id
            join ms_patient on ms_patient.n_patient_id = tb_medical_record.n_patient_id
            where tb_registration.n_mr_id = ".$n_mr_id); //v_insurance_name,v_reg_secondary_id
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function get_buat_pdf_tb_examination($n_mr_id,$tanggal_awal,$tanggal_akhir,$nosep) {
        $query = $this->db->query("SELECT v_note_no,n_exam_id,n_total_amount
            from tb_examination
            join tb_registration on tb_registration.n_reg_id=tb_examination.n_reg_id
            join tb_sepbpjs on tb_sepbpjs.nosep = tb_registration.v_nosepbpjs and status=1 and v_nosepbpjs='$nosep'
            where n_mr_id=".$n_mr_id."
            and n_exam_status=2
            and n_payment_status<>1
            and reg_status<>3
            and ((tb_examination.n_unit_id in (56,160,55) and n_jenisbayar=0) or tb_examination.n_unit_id not in (56,160,55))"); //and n_jenisbayar=0
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    /*function get_total_amount($n_reg_id) {
        $query = $this->db->query("SELECT sum(n_total_amount) as total_amount
            from tb_examination where n_reg_id=".$n_reg_id." and n_exam_status=2 
            and ((n_unit_id in (56,160,55) and n_jenisbayar=0) or n_unit_id not in (56,160,55))"); //and n_jenisbayar=0
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }*/

    /*function get_buat_pdf_tb_treatment($n_note_id) {
        $query = $this->db->query("SELECT distinct on (v_treatment_name) date(d_whn_create),v_treatment_name,n_doctor_id,n_qty,n_amount_trx
            from tb_treatment_trx
            where n_note_id = ".$n_note_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_buat_pdf_tb_item($n_note_id) {
        $query = $this->db->query("SELECT distinct on (v_item_name) date(tb_item_trx.d_whn_create),v_item_name,n_qty,tb_item_trx.n_amount_trx
            from tb_item_trx
            left join tb_drug_ingredients on tb_drug_ingredients.n_note_id = tb_item_trx.n_note_id
            join tb_examination on tb_examination.n_exam_id=tb_item_trx.n_note_id
            where tb_item_trx.n_note_id = ".$n_note_id." and n_jenisbayar=0 and n_payment_status <> 1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_buat_pdf_tb_drug_ingredients($n_note_id) {
        $query = $this->db->query("SELECT distinct on (v_item_composition) date(tb_drug_ingredients.d_whn_create),v_item_composition,'1.00' as n_qty,n_amount_trx
            from tb_drug_ingredients
            join tb_examination on tb_examination.n_exam_id=tb_drug_ingredients.n_note_id
            where tb_drug_ingredients.n_note_id = ".$n_note_id." and n_jenisbayar=0 and n_payment_status <> 1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }*/

    function get_detail_nota_paket($n_exam_id) {
        $query = $this->db->query("SELECT * from kasir.notakojekexam($n_exam_id)");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_nama_dokter_treatment($n_doctor_id) {
        $query = $this->db->query("SELECT v_staff_name
            from ms_staff
            where n_staff_id = ".$n_doctor_id);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    //LIS
    function get_vnoteno_by_nmrid($n_mr_id,$tanggal_awal,$tanggal_akhir) {
        $query = $this->db->query("SELECT
            v_note_no
            from tb_examination
            join tb_registration on tb_registration.n_reg_id=tb_examination.n_reg_id
            where n_mr_id=".$n_mr_id."
            and n_exam_status=2
            and n_payment_status<>1
            and (tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519)
            and reg_status<>3
            and d_registration_date between '$tanggal_awal'||' 00:00:00' and '$tanggal_akhir'||' 23:59:59'
            and ((tb_examination.n_unit_id in (56,160,55) and n_jenisbayar=0) or tb_examination.n_unit_id not in (56,160,55))
            and v_note_no like 'I-LAB%'"); //JANGAN UBAH
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_vnoteno_by_nmrid2($nosep) {
        $query = $this->db->query("SELECT
            v_note_no
            from tb_examination
            join tb_registration on tb_registration.n_reg_id=tb_examination.n_reg_id and tb_registration.v_nosepbpjs='$nosep'
            where n_exam_status=2
            and n_payment_status<>1
            -- and (tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519)
            and reg_status<>3
            -- and d_registration_date between '$tanggal_awal'||' 00:00:00' and '$tanggal_akhir'||' 23:59:59'
            and ((tb_examination.n_unit_id in (56,160,55) and n_jenisbayar=0) or tb_examination.n_unit_id not in (56,160,55))
            and v_note_no like 'I-LAB%'"); //JANGAN UBAH
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function get_rekap_laborat_by_vnoteno($v_note_no) {
        $lis = $this->load->database('lis', TRUE);
        $query = $lis->query("SELECT DISTINCT his_reg_no,test_name,result||test_flag_sign as hasil, 
                reference_value as nilai_normal,test_units_name,seq_group,seq
                from result_data 
                where his_reg_no = '".$v_note_no."' order by his_reg_no,seq_group,seq");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    //LIS

    //SEP GABUNG
    function get_modal_sep_gabung($n_mr_id) {
        $query = $this->db->query("SELECT n_reg_id,date(d_registration_date) as tgl_reg,v_reg_secondary_id,v_unit_name,v_nosepbpjs
            from tb_registration
            join ms_unit on ms_unit.n_unit_id=tb_registration.n_unit_id
            where (tb_registration.n_insurance_id=475 or tb_registration.n_insurance_id=519) and reg_status<>3
            and v_nosepbpjs is null
            and n_mr_id = ".$n_mr_id."
            order by d_registration_date desc limit 5");
            //and d_registration_date between '$tanggal_awal'||' 00:00:00' and '$tanggal_akhir'||' 23:59:59'
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }
    function get_idtclass_by_tclasscode($v_tclass_code){
        $query = $this->db->query("SELECT n_tclass_id from ms_treatment_class where v_tclass_code=".$v_tclass_code);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    function insert_sep($data){
        $this->db->insert('tb_sepbpjs', $data);
    }
    function update_sep($regid, $data){
        $this->db->where('regid', $regid);
        $this->db->update('tb_sepbpjs', $data);
    }
    function update_sep_by_nosep($nosep, $data){
        $this->db->where('nosep', $nosep);
        $this->db->update('tb_sepbpjs', $data);
    }
    function update_sep_registration($n_reg_id, $data){
        $this->db->where('n_reg_id', $n_reg_id);
        $this->db->update('tb_registration', $data);
    }
    function cek_nosep($nosep) {
        $query = $this->db->query("SELECT nosep from tb_sepbpjs where nosep = '".$nosep."'");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function cek_nosep_by_regid($regid) {
        $query = $this->db->query("SELECT regid from tb_sepbpjs where regid = '".$regid."'");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function cek_tanggal_nosep($nosep) {
        $query = $this->db->query("SELECT tglsep from tb_sepbpjs where nosep = '".$nosep."'");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    function get_reg_id_by_nosep($nosep) {
        $query = $this->db->query("SELECT regid from tb_sepbpjs where nosep = '".$nosep."'");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    //SEP GABUNG

    //MENCEGAH ERROR KETIKA UPLOAD KARENA TGL SEP (FOLDER) = NULL
    function get_tglsep_by_nosep($nosep) {
        $query = $this->db->query("SELECT tglsep from tb_sepbpjs where nosep = '".$nosep."'");
        if ($query->num_rows() > 0) {
            $hasil = $query->row_array();
            return $hasil['tglsep'];
        }
    }
    //MENCEGAH ERROR KETIKA UPLOAD KARENA TGL SEP (FOLDER) = NULL
}