<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ward extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function insert($data){
        $this->db->insert('ms_ward', $data);
    }

    function update($id_indikator_unit, $data){
        $this->db->where('n_ward_id', $id_indikator_unit);
        $this->db->update('ms_ward', $data);
    }

    function delete($id_indikator_unit) {
        $this->db->where('n_ward_id', $id_indikator_unit);
        $this->db->delete('ms_ward');
    }

    /*function get_list_all($n_staff_id){
		$this->db->select('n_ward_id, v_ward_name');
        $this->db->join('ms_unit', 'ms_unit.n_unit_id = ms_ward.n_unit_id');
        $this->db->join('ms_staff_in_unit', 'ms_staff_in_unit.n_unit_id = ms_unit.n_unit_id');
        $this->db->join('ms_staff', 'ms_staff.n_staff_id = ms_staff_in_unit.n_staff_id');
        $this->db->where('ms_staff.n_staff_id', $n_staff_id);
        $query = $this->db->get('ms_ward');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }*/

    function get_list_all(){
        $query = $this->db->query("SELECT DISTINCT ms_ward.n_ward_id, v_ward_name from ms_ward
                join ms_room on ms_room.n_ward_id=ms_ward.n_ward_id
                join ms_bed on ms_bed.n_room_id=ms_room.n_room_id and n_reg_id is not NULL");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_list_all_by_user($user_id){
        $query = $this->db->query("SELECT DISTINCT ms_ward.n_ward_id, v_ward_name 
                        from ms_ward
                        join ms_room on ms_room.n_ward_id=ms_ward.n_ward_id
                        join ms_bed on ms_bed.n_room_id=ms_room.n_room_id and n_reg_id is not null
                        join ms_unit on ms_unit.n_unit_id=ms_ward.n_unit_id
                        join ms_staff_in_unit on ms_staff_in_unit.n_unit_id=ms_unit.n_unit_id 
                        join ms_staff on ms_staff.n_staff_id=ms_staff_in_unit.n_staff_id  
                        join ms_user on ms_user.n_staff_id=ms_staff.n_staff_id and ms_user.n_user_id=$user_id");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    function get_wardname_by_id($n_ward_id) {
        $this->db->select('v_ward_name');
        $this->db->where('n_ward_id', $n_ward_id);
        $query = $this->db->get('ms_ward');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
}