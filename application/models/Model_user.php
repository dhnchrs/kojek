<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }
	
	function validasi_user($username, $password){
		$query=$this->db->query("select n_staff_id,n_user_id,v_user_full_name,v_user_name from ms_user where v_user_name='$username' AND v_password = md5('$password')");
		return $query->row_array();
	}

	function is_admin_it($n_staff_id){
		$query=$this->db->query("SELECT distinct n_unit_id from ms_staff_in_unit where n_unit_id=44 and n_staff_id='$n_staff_id'");
		if($query->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}

	function is_admin_casemix($n_user_id){
		$query=$this->db->query("SELECT distinct n_user_id from tb_group_privilege 
				join ms_user on ms_user.n_group_id=tb_group_privilege.n_group_id and n_user_id=$n_user_id
				where tb_group_privilege.n_group_id = 70");
		if($query->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}

	function is_admin_ranap($n_user_id){
		$query=$this->db->query("SELECT distinct n_user_id from tb_group_privilege 
				join ms_user on ms_user.n_group_id=tb_group_privilege.n_group_id and n_user_id=$n_user_id
				where tb_group_privilege.n_group_id in (53,70,7,63,66)");
		if($query->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}

	function is_admin_rajal($n_user_id){
		$query=$this->db->query("SELECT distinct n_user_id from tb_group_privilege 
				join ms_user on ms_user.n_group_id=tb_group_privilege.n_group_id and n_user_id=$n_user_id
				where tb_group_privilege.n_group_id in (53,70,5)");
		if($query->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	function cek_previlege($n_user_id){
		$query_cek_group=$this->db->query("select n_user_id from tb_group_privilege 
				join ms_user on ms_user.n_group_id=tb_group_privilege.n_group_id and n_user_id=$n_user_id
				where n_screen_id = 224");
		if($query_cek_group->num_rows()>=1){
			$user_prev = array('status' =>200 ,
			'description'=>'User Punya Hak Akes' ); 
		}else{
			$query_cek_user = $this->db->query("select n_user_id from tb_user_privilege where n_screen_id = 224 and n_user_id = $n_user_id");
			if($query_cek_user->num_rows()>=1){
				$user_prev = array('status' =>200 ,
				'description'=>'User Punya Hak Akes' ); 
			}else{
				$user_prev = array('status' =>0 ,
				'description'=>'User Tidak Punya Hak Akes' ); 
			}
		}
		return $user_prev;
	}
	
	function get_nik_nama_by_username($nik) {
		$query = $this->db->query("SELECT DISTINCT ON (v_staff_code) stf.v_staff_code,stf.v_staff_name
        	FROM ms_user usr
			JOIN ms_staff stf on stf.n_staff_id = usr.n_staff_id
			WHERE usr.v_user_name = '$nik'");
		if ($query->num_rows() >= 1) {
            return $query->row_array();
        }
	}
}